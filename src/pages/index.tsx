﻿import { useContext } from "react";
import Link from "next/link";
import Head from "next/head";
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { ReviewsUI } from "@/components/home/ReviewsUI";
import { CertificatesUI } from "@/components/home/CertificatesUI";
import { AuthContext } from "@/contexts/AuthContext";
import { ListCourses } from "@/components/ListCourses";
import { ListCombos } from "@/components/ListCombos";

export default function Home() {
  const { isLoggedIn } = useContext(AuthContext);

  return (
    <>
      <Head>
        <title>Home | CESCO</title>
      </Head>

      <Header />

      <main className="w-full" id="main-content" role="main">
        <section className="bg-center bg-no-repeat bg-cover bg-[url('https://img.freepik.com/foto-gratis/equipo-medicos-especialistas-jovenes-pie-pasillo-hospital_1303-21199.jpg?w=1380&t=st=1688407919~exp=1688408519~hmac=5d1d6c45a482592294c8f277984a5facc275c5f175693cd542ae1afef1887af4')] bg-gray-700 bg-blend-multiply">
          <div className="px-4 mx-auto max-w-screen-xl text-center py-12 md:py-24 lg:py-56">
            <h1 className="mb-4 text-3xl font-extrabold tracking-tight leading-none text-white md:text-5xl lg:text-6xl">
              Tu vocación es servir, la nuestra también, hagamoslo juntos
            </h1>
            <p className="mb-8 text-base font-normal text-gray-300 lg:text-xl sm:px-16 lg:px-48">
              Empresa líder en innovación para la educación y formación continua
              en el sector de la salud en Colombia, Bajo la resolución 3100 del
              2019.
            </p>
            <div className="flex flex-col space-y-4 sm:flex-row sm:justify-center sm:space-y-0 sm:space-x-4">
              <Link href="/courses" className="btn btn-primary-green">
                Ver los cursos
                <svg
                  aria-hidden="true"
                  className="ml-2 -mr-1 w-4 h-4"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
              </Link>
              {!isLoggedIn && (
                <>
                  <Link href="/auth/sign-up" className="btn btn-outline-white">
                    Crear cuenta gratis
                  </Link>
                </>
              )}
            </div>
          </div>
        </section>

        <section className="bg-primary-green/5 border-b border-primary-green w-full h-full py-10 md:py-20">
          <div className="container">
            <div className="row">
              <div className="col-12 col-lg-10">
                <h1 className="font-extrabold text-xl md:text-2xl text-primary-green mb-4">
                  Cursos y Diplomados CESCO
                </h1>
                <p className="font-normal text-sm md:text-lg text-zinc-700 mb-8">
                  Accede a los mejores cursos online para creativo. Interactua
                  con los mejores profesonales y descubre todos los secretos del
                  sector.
                </p>
              </div>
            </div>
            <ListCourses />
          </div>
        </section>

        <section className="bg-white border-b border-primary-green w-full h-full py-10 md:py-20">
          <div className="container">
            <div className="row">
              <div className="col-12 col-lg-10">
                <h1 className="font-extrabold text-xl md:text-2xl text-primary-green mb-4">
                  Combos CESCO
                </h1>
                <p className="font-normal text-sm md:text-lg text-zinc-700 mb-8">
                  Accede a los mejores cursos online para creativo. Interactua
                  con los mejores profesonales y descubre todos los secretos del
                  sector.
                </p>
              </div>
              <div className="col-12">
                <ListCombos />
              </div>
            </div>
          </div>
        </section>

        <ReviewsUI />
        <CertificatesUI />
      </main>

      <Footer />
    </>
  );
}
