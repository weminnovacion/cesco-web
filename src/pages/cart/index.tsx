//React
import { useContext, useEffect, useState } from "react";

//Next
import Head from "next/head";
import Link from "next/link";

//Components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";

//Context
import { CartContext } from "@/contexts/CartContext";

//Hooks
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faShoppingCart,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import { CartItem } from "@/model/cart.model";
import { CourseProps } from "@/model/courseProps.model";
import { RequestStatus } from "@/model/request-status";
import axios from "axios";
import { ComboProps } from "@/model/comboProps.model";
import { EmptyCart } from "@/components/cart/EmptyCart";
import { LoadingCart } from "@/components/cart/LoadingCart";
import { API_URL } from "../../../constants";

interface CoursesByCombo {
  combo: ComboProps;
  courses: CourseProps[];
}

export default function Cart() {
  const { courses, removeFromCart, totalCourses, totalCombos, combos } =
    useContext(CartContext);
  const [status, setStatus] = useState<RequestStatus>("init");
  const [coursesByCombo, setCoursesByCombo] = useState<CoursesByCombo[]>([]);
  const [totalCart, setTotalCart] = useState<number>(0);
  const [totalItems, setTotalItems] = useState<number>(0);
  const { currencyWithSuffixFormatter } = useCurrencyFormatter();

  const handleRemoveCourseFromCart = (course: CourseProps) => {
    const item: CartItem = { ...course, type: "course" };
    removeFromCart(item);
  };

  const handleRemoveComboFromCart = (combo: ComboProps) => {
    const item: CartItem = { ...combo, type: "combo" };
    removeFromCart(item);
  };

  const getCoursesByCombo = (idCombo: number): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await axios.get(
          API_URL + "coursePackage/getComboById?id=" + idCombo
        );
        let coursesData = response.data.Data;
        resolve(coursesData);
      } catch (error) {
        reject(error);
      }
    });
  };

  // ya vuelvo voy a cenar

  const fetchData = async () => {
    setStatus("loading");
    try {
      const promises = combos.map((combo) => getCoursesByCombo(combo.Id));
      const coursesDataArray = await Promise.all(promises);
      const coursesByCombo = coursesDataArray.map((coursesData) => coursesData);
      setCoursesByCombo(coursesByCombo);
      const totalComboCourses = coursesByCombo.reduce((acc, coursesData) => {
        const coursesInCombo = coursesData.courses.length;
        return acc + coursesInCombo;
      }, 0);
      
      setStatus("success"); 
    } catch (error) {
      setStatus("error");
    }
  };

  useEffect(()=>{
    fetchData();
  }, []);

  useEffect(()=>{
    setTotalCart(totalCourses + totalCombos)
  },[totalCourses, totalCombos])
  
  useEffect(() => {
    const totalItems = courses.length + combos.length;
    setTotalItems(totalItems);
  }, [courses, combos]);

  return (
    <>
      <Head>
        <title>CESCO - Cart</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />

        <main
          className="w-full flex-grow bg-zinc-100"
          id="main-content"
          role="main"
        >
          <div className="container pt-10">
            <div className="row">
              {status === "success" && totalItems ? (
                <div className="col-12 ">
                  <div className="row">
                    <div className="col-12">
                      <div className="bg-white w-full rounded p-5 mb-5">
                        <div className="flex justify-between items-center">
                          <h3 className="text-lg font-semibold">
                            Carrito ({totalItems}{" "}
                            {totalItems === 1 ? "Curso" : "Cursos"})
                          </h3>
                          <Link
                            href={"/courses"}
                            className="btn btn-outline-solid-white"
                          >
                            <span className="mr-2">Continuar comprando</span>
                            <FontAwesomeIcon icon={faArrowRight} />
                          </Link>
                        </div>
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="bg-white w-full rounded p-5 mb-5">
                        <div className="flex justify-end items-center border-b mb-5 pb-5">
                          <Link
                            href={"/cart/checkout"}
                            className="btn btn-outline-primary-solid-green "
                          >
                            <FontAwesomeIcon icon={faShoppingCart} />
                            <span className="mx-2">Tramitar pedido</span>
                            <FontAwesomeIcon icon={faArrowRight} />
                          </Link>
                        </div>

                        <div className="row ">
                          {courses.map((course) => (
                            <div className="col-12" key={course.Id}>
                              <div className="border-b mb-5 pb-5">
                                <div className="row">
                                  <div className="col-auto">
                                    <div className="w-28 h-28 bg-zinc-300 border border-zinc-300 rounded overflow-hidden">
                                      <img
                                        src={course.ImgProfile}
                                        alt={course.Name}
                                        className="bg-cover object-cover w-full h-full"
                                      />
                                    </div>
                                  </div>
                                  <div className="col">
                                    <h3 className="text-base font-semibold">
                                      {course.Name}
                                    </h3>
                                    <p className="text-sm mb-2">
                                      <small>
                                        Un curso de {course.Professor.Name}
                                      </small>
                                    </p>
                                    <span className="text-secondary text-sm mr-2 pb-3">
                                      {currencyWithSuffixFormatter(
                                        course.PriceWithDiscount
                                      )}
                                      Dto.
                                    </span>
                                    <span className="text-secondary text-sm line-through font-bold">
                                      <strong className="line-through">
                                        {currencyWithSuffixFormatter(
                                          course.Price
                                        )}
                                      </strong>
                                    </span>
                                  </div>
                                  <div className="col-auto">
                                    <div className="row">
                                      <div className="col">
                                        <p className="text-sm font-semibold">
                                          {currencyWithSuffixFormatter(
                                            course.PriceWithDiscount
                                          )}
                                        </p>
                                      </div>
                                      <div className="col-auto">
                                        <button
                                          type="button"
                                          onClick={() =>
                                            handleRemoveCourseFromCart(course)
                                          }
                                          className="w-6 h-6 flex justify-center items-center text-sm text-zinc-500 hover:text-red-500"
                                        >
                                          <FontAwesomeIcon icon={faTrashAlt} />
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          ))}
                        </div>

                        <div className="row">
                          {coursesByCombo.map(({ combo, courses }) => (
                            <>
                              {courses.map((course) => (
                                <div className="col-12" key={course.Id}>
                                  <div className="border-b mb-5 pb-5">
                                    <div className="row">
                                      <div className="col-auto">
                                        <div className="w-28 h-28 bg-zinc-300 border border-zinc-300 rounded overflow-hidden">
                                          <img
                                            src={course.ImgProfile}
                                            alt={course.Name}
                                            className="bg-cover object-cover w-full h-full"
                                          />
                                        </div>
                                      </div>
                                      <div className="col">
                                        <h3 className="text-base font-semibold">
                                          {course.Name}
                                        </h3>
                                        <p className="text-sm mb-2">
                                          <small>
                                            Un curso de {course.Professor.Name}
                                          </small>
                                        </p>
                                        <span className="text-secondary text-sm mr-2 pb-3">
                                          {currencyWithSuffixFormatter(
                                            course.PriceWithDiscount
                                          )}
                                          Dto.
                                        </span>
                                        <span className="text-secondary text-sm line-through font-bold">
                                          <strong className="line-through">
                                            {currencyWithSuffixFormatter(
                                              course.Price
                                            )}
                                          </strong>
                                        </span>
                                      </div>
                                      <div className="col-auto">
                                        <div className="row">
                                          <div className="col">
                                            <p className="text-sm font-semibold">
                                              {currencyWithSuffixFormatter(
                                                course.PriceWithDiscount
                                              )}
                                            </p>
                                          </div>
                                          <div className="col-auto">
                                            <button
                                              type="button"
                                              onClick={() =>
                                                handleRemoveComboFromCart(combo)
                                              }
                                              className="w-6 h-6 flex justify-center items-center text-sm text-zinc-500 hover:text-red-500"
                                            >
                                              <FontAwesomeIcon
                                                icon={faTrashAlt}
                                              />
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              ))}
                              <div className="col-12">
                                <div className="border-b mb-5 pb-5">
                                  <div className="row">
                                    <div className="col-12">
                                      <div className="row text-primary-green">
                                        <div className="col">
                                          <div className="flex">
                                            <h3 className="text-base font-semibold mr-1">
                                              {combo.Name}
                                            </h3>
                                            {"-"}
                                            <p className="mx-1">
                                              Paquete de {combo.NumCourses}{" "}
                                              cursos
                                            </p>
                                          </div>
                                        </div>
                                        <div className="col-auto">
                                          <p className="font-semibold">
                                            {" "}
                                            -{" "}
                                            {currencyWithSuffixFormatter(
                                              combo.DiscountValueForTheCombo
                                            )}
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </>
                          ))}
                        </div>

                        <div className="row">
                          <div className="col-12">
                            <div className="bg-zinc-100 flex justify-between items-center p-5">
                              <h3 className="text-lg text-zinc-800 font-semibold">
                                Total
                              </h3>
                              <h3 className="text-lg text-zinc-800 font-semibold">
                                {currencyWithSuffixFormatter(totalCart)}
                              </h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
            {(status === "error" && totalItems === 0) ||
            (status === "success" && totalItems === 0) ? (
              <>
                <EmptyCart />
              </>
            ) : null}
            {status === "loading" ? <LoadingCart /> : null}
          </div>
        </main>

        <Footer />
      </div>
    </>
  );
}
