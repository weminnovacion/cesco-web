//React
import { useContext, useEffect, useState } from "react";

//Next
import Head from "next/head";

//components
import { Header } from "@/components/Header";
import { SignInForm } from "@/components/sign-in-form";
import { SignUpForm } from "@/components/sign-up-form";
import { RecordPaymentForm } from "@/components/record-payment-form";

//Context
import { AuthContext } from "@/contexts/AuthContext";
import { CartContext } from "@/contexts/CartContext";
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";

//externals
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartShopping,
  faCircleCheck,
  faExclamationCircle,
} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import axios from "axios";
import { API_URL } from "../../../../constants";
import { parseCookies } from "nookies";
import { RequestStatus } from "@/model/request-status";
import { EmptyCart } from "@/components/cart/EmptyCart";
import { CourseProps } from "@/model/courseProps.model";
import { ComboProps } from "@/model/comboProps.model";
import { LoadingCart } from "@/components/cart/LoadingCart";
import { Modal } from "@/components/Modal";
import { PSEPaymentForm } from "@/components/pse-payment-form";

type SignInFormProps = {
  handleStateForm: (
    status: "init" | "success" | "error" | "loading",
    message?: string
  ) => void;
};

interface CoursesByCombo {
  combo: ComboProps;
  courses: CourseProps[];
}

export default function Checkout() {
  const [toggleForm, setToggleForm] = useState(true);

  const { courses, totalCourses, totalCombos, combos, cart, cleanCart } =
    useContext(CartContext);
  const [status, setStatus] = useState<RequestStatus>("init");
  const [coursesByCombo, setCoursesByCombo] = useState<CoursesByCombo[]>([]);
  const [totalCart, setTotalCart] = useState<number>(0);
  const [totalItems, setTotalItems] = useState<number>(0);
  const { currencyWithSuffixFormatter } = useCurrencyFormatter();
  const [statusPayment, setStatusPayment] = useState();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const { isLoggedIn, userInfo } = useContext(AuthContext);

  const handleToggleForm = () => {
    setToggleForm(!toggleForm);
  };

  const handleToggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const handleSubmit: SignInFormProps["handleStateForm"] = (status) => {
    if (status === "success") {
    } else if (status === "error") {
    }
  };

  const handleRecordFormSubmit = async (formData: RecordPayment) => {
    const items = parseArrayOfCoursesAndCombos();

    let json = {
      TypePayment: 1,
      CardNumber: formData.CardNumber,
      ExpYear: formData.ExpYear,
      ExpMonth: formData.ExpMonth,
      Cvc: formData.Cvc,
      items,
    };

    const response = await completePayment(json);
    setIsModalOpen(true);
    cleanCart();
    setStatusPayment(response.data.Status);
  };
  
  const handlePSEFormSubmit = async (formData: PSEPayment) => {
    const items = parseArrayOfCoursesAndCombos();
    
    let json = {
      TypePayment: 2,
      BankId: formData.BankId,
      TypePersonId: formData.TypePersonId,
      items,
    };

    const response = await completePayment(json);
    if (response.Status == 200) {
      window.location.href = response.Data;
    }
  };

  const completePayment = async (json: any): Promise<any> => {
    const cookies = parseCookies();
    const token = cookies.token;
    const config = {
      headers: {
        Authorization: "Bearer " + token,
      },
    };

    try {
      setStatus("loading");
      const response = await axios.post(
        API_URL + "checkout/completePayment",
        json,
        config
      );
      if (response.status == 200) {
        return response.data;
      }
    } catch (error) {
      return error;
    }
  };

  const parseArrayOfCoursesAndCombos = (): any => {
    return [...courses, ...combos].map((item) => {
      if ("TypeCourse" in item) {
        // Es un curso
        return {
          Id: item.Id,
          Type: item.TypeCourse.Id,
        };
      } else {
        // Es un combo
        return {
          Id: item.Id,
          Type: 2,
        };
      }
    });
  };

  const getCoursesByCombo = (idCombo: number): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await axios.get(
          API_URL + "coursePackage/getComboById?id=" + idCombo
        );
        let coursesData = response.data.Data;
        resolve(coursesData);
      } catch (error) {
        reject(error);
      }
    });
  };

  const fetchData = async () => {
    setStatus("loading");
    try {
      const promises = combos.map((combo) => getCoursesByCombo(combo.Id));
      const coursesDataArray = await Promise.all(promises);

      // Calcular el total de cursos por combo
      const coursesByCombo = coursesDataArray.map((coursesData) => coursesData);

      // Actualizar el estado con los cursos por combo
      setCoursesByCombo(coursesByCombo);

      // Calcular el total de cursos en todos los combos
      const totalComboCourses = coursesByCombo.reduce((acc, coursesData) => {
        const coursesInCombo = coursesData.courses.length;
        return acc + coursesInCombo;
      }, 0);

      // Actualizar el estado con el total de cursos en todos los combos
      setTotalCart(totalCourses + totalCombos);

      setStatus("success");
    } catch (error) {
      setStatus("error");
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
    const totalItems = courses.length + combos.length;
    setTotalItems(totalItems);
  }, [courses, combos]);

  return (
    <>
      <Head>
        <title>Completa tu compra | CESCO</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />

        <div className="w-full flex-grow bg-zinc-100 py-10">
          <div className="container">
            {status === "success" && totalItems ? (
              <div className="row">
                <div className="col-12 col-lg-6 mb-5">
                  {isLoggedIn ? (
                    <>
                      <div
                        className={classNames(
                          "bg-white p-8 mb-4 transition-all duration-300 w-full"
                        )}
                      >
                        <h1 className="text-2xl font-bold">
                          Que medios de pagos desea utilizar?
                        </h1>
                        <p className="text-sm mb-5">
                          Ya puedes realizar tu compra,
                          josearmandoacevedoangarita
                        </p>
                        <div className="flex flex-wrap -mx-3">
                          <div className="w-full px-3">
                            <div
                              id="accordion-flush"
                              data-accordion="collapse"
                              data-active-classes="bg-white dark:bg-gray-900 text-gray-900 dark:text-white"
                              data-inactive-classes="text-gray-500 dark:text-gray-400"
                            >
                              <h2
                                id="accordion-flush-heading-1"
                                className="mb-4"
                              >
                                <input
                                  type="radio"
                                  id="tarjeta-de-credito"
                                  name="medio-de-pago"
                                  value="tarjeta-de-credito"
                                  className="hidden peer input-metodo-de-pago"
                                  required
                                />
                                <label
                                  htmlFor="tarjeta-de-credito"
                                  className="inline-flex items-center w-full p-5 text-gray-500 bg-white border border-gray-200 cursor-pointer  peer-checked:border-primary-green peer-checked:text-primary-green hover:text-gray-600 hover:bg-gray-100 label-metodo-de-pago"
                                  data-accordion-target="#accordion-flush-body-0"
                                  aria-expanded="true"
                                  aria-controls="accordion-flush-body-1"
                                >
                                  <div className="flex flex-col w-full">
                                    <div className="flex flex-wrap -mx-3 w-full">
                                      <div className="flex-grow flex-shrink-0 basis-0 w-full max-w-full text-base text-zinc-900 font-semibold px-3">
                                        Pagar con tarjeta debito y/o credito
                                      </div>
                                      <div className="flex-grow-1 flex-shrink-0 basis-auto w-auto max-w-full ">
                                        <div className="border">
                                          <img
                                            data-srcset="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-visa-8a18fbf2359d9aa9ae996beea6fb859de3be9d8d5256e4fb7c0f926f22f51c45.png, https://cdn.domestika.org/raw/upload/assets/payment-methods/card-visa@2x-d683b41bd4bdde323cb893fed75b982577c2352b3d22dca76d141ef9ada44113.png 2x"
                                            alt="Visa"
                                            title="Visa"
                                            className=" lazyloaded"
                                            src="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-lazyload-6b2937151d7662d2a0b37af79c23a1dfaa812544aa1bd701b42077157c08dd93.svg"
                                            srcSet="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-visa-8a18fbf2359d9aa9ae996beea6fb859de3be9d8d5256e4fb7c0f926f22f51c45.png, https://cdn.domestika.org/raw/upload/assets/payment-methods/card-visa@2x-d683b41bd4bdde323cb893fed75b982577c2352b3d22dca76d141ef9ada44113.png 2x"
                                          ></img>
                                        </div>
                                      </div>
                                      <div className="flex-grow-1 flex-shrink-0 basis-auto w-auto max-w-full px-3">
                                        <div className="border">
                                          <img
                                            data-srcset="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-mastercard-ea3facb35831c19f7d184954248b0e458ed8f1efb6495f75bcc797a014252d96.png, https://cdn.domestika.org/raw/upload/assets/payment-methods/card-mastercard@2x-df0939e975e0b2549a921a8d4754c7c960a55dcdfee81a2a755d0dc6f1610a78.png 2x"
                                            alt="MasterCard"
                                            title="MasterCard"
                                            className=" lazyloaded"
                                            src="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-lazyload-6b2937151d7662d2a0b37af79c23a1dfaa812544aa1bd701b42077157c08dd93.svg"
                                            srcSet="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-mastercard-ea3facb35831c19f7d184954248b0e458ed8f1efb6495f75bcc797a014252d96.png, https://cdn.domestika.org/raw/upload/assets/payment-methods/card-mastercard@2x-df0939e975e0b2549a921a8d4754c7c960a55dcdfee81a2a755d0dc6f1610a78.png 2x"
                                          ></img>
                                        </div>
                                      </div>
                                    </div>
                                    <div
                                      id="accordion-flush-body-1"
                                      className="content-metodo-de-pago"
                                      aria-labelledby="accordion-flush-heading-1"
                                    >
                                      <div className="pt-5 ">
                                        <RecordPaymentForm
                                          handleStateForm={
                                            handleRecordFormSubmit
                                          }
                                          status={status}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </label>
                              </h2>
                              <h2 id="accordion-flush-heading-3">
                                <input
                                  type="radio"
                                  id="efectivo"
                                  name="medio-de-pago"
                                  value="efectivo"
                                  className="hidden peer input-metodo-de-pago"
                                  required
                                />
                                <label
                                  htmlFor="efectivo"
                                  className="inline-flex items-center w-full p-5 text-gray-500 bg-white border border-gray-200 cursor-pointer  peer-checked:border-primary-green peer-checked:text-primary-green hover:text-gray-600 hover:bg-gray-100 label-metodo-de-pago"
                                  data-accordion-target="#accordion-flush-body-3"
                                  aria-expanded="true"
                                  aria-controls="accordion-flush-body-3"
                                >
                                  <div className="flex flex-col w-full">
                                    <div className="flex flex-wrap -mx-3 w-full">
                                      <div className="flex-grow flex-shrink-0 basis-0 w-full max-w-full text-base text-zinc-900 font-semibold px-3">
                                        Pagar con PSE
                                      </div>
                                    </div>
                                    <div
                                      id="accordion-flush-body-3"
                                      className="content-metodo-de-pago"
                                      aria-labelledby="accordion-flush-heading-3"
                                    >
                                      <div className="py-5 ">
                                        <PSEPaymentForm
                                          handleStateForm={handlePSEFormSubmit}
                                          status={status}
                                          setStatus={setStatus}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </label>
                              </h2>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      <div
                        hidden={!toggleForm}
                        className={classNames(
                          "bg-white p-8 mb-4 transition-all duration-300 w-full"
                        )}
                      >
                        <div className="flex justify-between">
                          <h1 className="text-3xl font-bold">Entra</h1>
                          <span className="flex items-center">
                            ¿No tienes cuenta?
                            <a
                              onClick={handleToggleForm}
                              role="button"
                              className="font-bold hover:text-primary-green hover:underline focus:text-primary-green focus:underline transition-all duration-300 ml-3"
                            >
                              Formulario de inscripcion
                            </a>
                          </span>
                        </div>
                        <div className="my-4 relative p-2">
                          <div className="absolute top-1/2 -translate-y-1/2 w-full h-px bg-zinc-300 z-10"></div>
                          <span className="bg-white px-4 py-2 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2  inline-block mx-auto text-sm text-zinc-500 z-20">
                            entra con tu email
                          </span>
                        </div>
                        <SignInForm handleStateForm={handleSubmit} />
                      </div>
                      <div
                        hidden={toggleForm}
                        className={classNames(
                          "bg-white p-8 mb-4 transition-all duration-300 w-full"
                        )}
                      >
                        <div className="flex justify-between">
                          <h1 className="text-3xl font-bold">Regístrate</h1>
                          <span className="flex items-center">
                            ¿Ya tienes cuenta?
                            <a
                              onClick={handleToggleForm}
                              role="button"
                              className="font-bold hover:text-primary-green hover:underline focus:text-primary-green focus:underline transition-all duration-300 ml-3"
                            >
                              Entrar
                            </a>
                          </span>
                        </div>
                        <div className="my-4 relative p-2">
                          <div className="absolute top-1/2 -translate-y-1/2 w-full h-px bg-zinc-300 z-10"></div>
                          <span className="bg-white px-4 py-2 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2  inline-block mx-auto text-sm text-zinc-500 z-20">
                            Regístrate con tu email
                          </span>
                        </div>
                        <SignUpForm handleStateForm={handleSubmit} />
                      </div>
                    </>
                  )}
                  <p className="text-lg font-bold mb-4">Pago 100% seguro</p>
                  <ul className="flex gap-x-2 mb-4">
                    <li className="list-inline-item">
                      <div className="card-wrapper">
                        <img
                          data-srcset="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-visa-8a18fbf2359d9aa9ae996beea6fb859de3be9d8d5256e4fb7c0f926f22f51c45.png, https://cdn.domestika.org/raw/upload/assets/payment-methods/card-visa@2x-d683b41bd4bdde323cb893fed75b982577c2352b3d22dca76d141ef9ada44113.png 2x"
                          alt="Visa"
                          title="Visa"
                          className=" lazyloaded"
                          src="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-lazyload-6b2937151d7662d2a0b37af79c23a1dfaa812544aa1bd701b42077157c08dd93.svg"
                          srcSet="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-visa-8a18fbf2359d9aa9ae996beea6fb859de3be9d8d5256e4fb7c0f926f22f51c45.png, https://cdn.domestika.org/raw/upload/assets/payment-methods/card-visa@2x-d683b41bd4bdde323cb893fed75b982577c2352b3d22dca76d141ef9ada44113.png 2x"
                        />
                      </div>
                    </li>
                    <li className="list-inline-item">
                      <div className="card-wrapper">
                        <img
                          data-srcset="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-mastercard-ea3facb35831c19f7d184954248b0e458ed8f1efb6495f75bcc797a014252d96.png, https://cdn.domestika.org/raw/upload/assets/payment-methods/card-mastercard@2x-df0939e975e0b2549a921a8d4754c7c960a55dcdfee81a2a755d0dc6f1610a78.png 2x"
                          alt="MasterCard"
                          title="MasterCard"
                          className=" lazyloaded"
                          src="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-lazyload-6b2937151d7662d2a0b37af79c23a1dfaa812544aa1bd701b42077157c08dd93.svg"
                          srcSet="https://cdn.domestika.org/raw/upload/assets/payment-methods/card-mastercard-ea3facb35831c19f7d184954248b0e458ed8f1efb6495f75bcc797a014252d96.png, https://cdn.domestika.org/raw/upload/assets/payment-methods/card-mastercard@2x-df0939e975e0b2549a921a8d4754c7c960a55dcdfee81a2a755d0dc6f1610a78.png 2x"
                        />
                      </div>
                    </li>
                  </ul>
                  <p className="text-xs text-zinc-500">
                    Tus tarjetas se guardan de forma segura para que puedas
                    reutilizar el método de pago.
                  </p>
                </div>
                <div className="col-12 col-lg-6">
                  <div className="bg-white p-8 mb-4">
                    <div className="flex justify-between">
                      <span className="text-sm">
                        Carrito ({totalItems}{" "}
                        {totalItems === 1 ? "Curso" : "Cursos"})
                      </span>
                      <Link
                        href={"/cart"}
                        role="button"
                        className="text-zinc-500 text-sm hover:text-secondary transition-all duration-300"
                      >
                        Editar
                      </Link>
                    </div>
                    <hr className="mt-4 mb-6" />
                    <div className="row">
                      {courses.map((course) => (
                        <>
                          <div className="col-12" key={course.Id}>
                            <p className="font-bold text-base">{course.Name}</p>
                            <p className="text-sm">
                              Un curso de {course.Professor.Name}
                            </p>
                            <span className="text-secondary text-sm mr-2 pb-3">
                              {currencyWithSuffixFormatter(
                                course.PriceWithDiscount
                              )}{" "}
                              Dto.
                            </span>
                            <span className="text-secondary text-sm line-through font-bold">
                              {" "}
                              {currencyWithSuffixFormatter(course.Price)}
                            </span>
                            <hr className="mt-4 mb-6" />
                          </div>
                        </>
                      ))}
                      {coursesByCombo.map(({ combo, courses }) => (
                        <>
                          {courses.map((course) => (
                            <>
                              <div className="col-12" key={course.Id}>
                                <p className="font-bold text-base">
                                  {course.Name}
                                </p>
                                <p className="text-sm">
                                  Un curso de {course.Professor.Name}
                                </p>
                                <span className="text-secondary text-sm mr-2 pb-3">
                                  {currencyWithSuffixFormatter(
                                    course.PriceWithDiscount
                                  )}{" "}
                                  Dto.
                                </span>
                                <span className="text-secondary text-sm line-through font-bold">
                                  {" "}
                                  {currencyWithSuffixFormatter(course.Price)}
                                </span>
                                <hr className="mt-4 mb-6" />
                              </div>
                            </>
                          ))}
                          <div className="col-12">
                            <div className="border-b mb-5 pb-5">
                              <div className="row">
                                <div className="col-12">
                                  <div className="row text-primary-green">
                                    <div className="col">
                                      <div className="flex">
                                        <h3 className="text-base font-semibold mr-1">
                                          {combo.Name}
                                        </h3>
                                        {"-"}
                                        <p className="mx-1">
                                          Paquete de {combo.NumCourses} cursos
                                        </p>
                                      </div>
                                    </div>
                                    <div className="col-auto">
                                      <p className="font-semibold">
                                        -{" "}
                                        {currencyWithSuffixFormatter(
                                          combo.DiscountValueForTheCombo
                                        )}
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </>
                      ))}
                      <div className="col-12">
                        <div className="flex justify-between bg-zinc-100 p-4">
                          <p className="font-bold text-lg">Total</p>
                          <p className="font-bold text-lg">
                            {currencyWithSuffixFormatter(totalCart)}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 text-xs text-zinc-400">
                      <p className="text-xl mb-3 font-bold">
                        ¿Qué estás comprando?
                      </p>

                      <ul className="pl-4">
                        <li className="mb-4">
                          Acceso ilimitado a los cursos que compres que nunca
                          expira para verlos todas las veces que desees.
                        </li>
                        <li className="mb-4">
                          Acceso por tiempo indefinido a las diferentes unidades
                          del curso, sus lecciones, textos explicativos y
                          recursos adicionales.
                        </li>
                        {/* <li className="mb-4">
                            Acceso a la comunidad exclusiva del curso donde podrás intercambiar experiencias con el profesor y otros estudiantes.
                          </li> */}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            ) : null}
            {(status === "error" && totalItems === 0) ||
            (status === "success" && totalItems === 0) ? (
              <>
                <EmptyCart />
              </>
            ) : null}
            {status === "loading" ? <LoadingCart /> : null}
          </div>
        </div>
      </div>
      <Modal isOpen={isModalOpen} onClose={handleToggleModal} size="max-w-3xl">
        <div className="lg:p-8 p-5">
          <div className="row">
            {statusPayment == 200 && (
              <>
                <div className="col-auto mx-auto">
                  <FontAwesomeIcon
                    className="text-7xl text-primary-green mb-5"
                    icon={faCircleCheck}
                  />
                </div>
                <div className="col-12">
                  <h2 className="text-zinc-800 text-3xl mb-5 text-center font-semibold">
                    Pago recibido
                  </h2>
                  <p className="text-base text-zinc-500 text-center">
                    Recibimos tu pago de{" "}
                    <strong>{currencyWithSuffixFormatter(totalCart)}</strong>,{" "}
                    {`${userInfo?.FirstName} ${userInfo?.Surname}`}
                  </p>
                </div>
                <div className="col-12">
                  <hr className="my-5" />
                </div>
                <div className="col-12">
                  <Link
                    href={"/my-courses"}
                    className="btn btn-primary-green w-full"
                  >
                    Ir a mis cursos
                  </Link>
                </div>
              </>
            )}
            {statusPayment == -1 && (
              <>
                <div className="col-auto mx-auto">
                  <FontAwesomeIcon
                    className="text-7xl text-red-500 mb-5"
                    icon={faExclamationCircle}
                  />
                </div>
                <div className="col-12">
                  <h2 className="text-zinc-800 text-3xl mb-5 text-center font-semibold">
                    Pago fallido
                  </h2>
                  <p className="text-base text-zinc-500 text-center">
                    Hola, intentamos cargar a su tarjeta{" "}
                    <strong>{currencyWithSuffixFormatter(totalCart)}</strong>{" "}
                    pero algo salió mal. <strong>Actualiza</strong> su método de
                    pago a continuación para continuar.
                  </p>
                </div>
                <div className="col-12">
                  <hr className="my-5" />
                </div>
                <div className="col-12">
                  <button
                    onClick={handleToggleModal}
                    className="btn btn-outline-solid-white w-full"
                  >
                    Actualizar metodo de pago
                  </button>
                </div>
              </>
            )}
          </div>
        </div>
      </Modal>
    </>
  );
}
