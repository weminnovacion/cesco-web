import Link from "next/link";

const Page404 = () => {
  return (
    <>
      <section className="bg-white">
        <div className="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
          <div className="mx-auto max-w-screen-sm text-center">
            <h1 className="mb-4 text-7xl tracking-tight font-extrabold lg:text-9xl text-primary-green ">
              404
            </h1>
            <p className="mb-4 text-3xl tracking-tight font-bold text-zinc-700 md:text-4xl ">
              Algo falta, algo está faltando.
            </p>
            <p className="mb-4 text-lg font-light text-zinc-500 ">
              Lo sentimos, no podemos encontrar esa página. Encontrará mucho
              para explorar en la página de inicio.{" "}
            </p>
            <Link
              href="/"
              className="inline-flex text-white bg-primary-green hover:bg-primary-green/80 focus:ring-4 focus:outline-none focus:ring-primary-green/30 font-medium rounded-sm text-sm px-5 py-2.5 text-center  my-4"
            >
              Volver al home
            </Link>
          </div>
        </div>
      </section>
    </>
  );
};

export default Page404;
