//Next
import Head from "next/head";

//components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { Breadcrumbs } from "@/components/Breadcrumbs";
import { ListCourses } from "@/components/ListCourses";

export default function Courses() {
  const breadcrumbs = [{ name: "Cursos", url: "/courses" }];

  return (
    <>
      <Head>
        <title>Cursos Online | CESCO</title>
      </Head>

      <Header />

      <main
        className="w-full over bg-zinc-100"
        id="main-content"
        role="main"
      >
        <div className="w-full border-b mb-8 py-4">
          <div className="container">
            <Breadcrumbs items={breadcrumbs} />
          </div>
        </div>

        <div className="container position-relative">
          <div className="row ">
            <div className="col-12">
              <header className="relative mx-0 mb-5 pb-3 border-b block">
                <h1 className="text-2xl text-center md:text-left md:text-3xl font-bold text-primary-green">
                  Cursos y Diplomados CESCO
                </h1>
              </header>
            </div>
          </div>
          <ListCourses />
        </div>
      </main>

      <Footer />
    </>
  );
}
