//Next
import Head from "next/head";

//components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { Breadcrumbs } from "@/components/Breadcrumbs";

//Models

//Externals
import { ListCombos } from "@/components/ListCombos";

export default function Combos() {
  const breadcrumbs = [
    { name: "Cursos", url: "/courses" },
    { name: "Combos", url: "/courses/combos" },
  ];

  return (
    <>
      <Head>
        <title>Combos | CESCO</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />

        <main
          className="w-full flex-grow bg-zinc-100"
          id="main-content"
          role="main"
        >
          <div className="w-full border-b mb-8 py-4">
            <div className="container">
              <Breadcrumbs items={breadcrumbs} />
            </div>
          </div>

          <div className="container">
            <div className="row relative">
              <div className="col-md-12 col-lg-12">
                <div className="row">
                  <div className="col-12">
                    <header className="relative mx-0 mb-5 pb-3 border-b block">
                      <h1 className="text-3xl font-bold text-primary-green">
                        Combos CESCO
                      </h1>
                    </header>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <ListCombos />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>

        <Footer />
      </div>
    </>
  );
}
