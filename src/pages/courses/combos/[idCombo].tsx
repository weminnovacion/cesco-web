//React
import { useContext, useEffect, useState } from "react";

//Next
import Head from "next/head";
import { useRouter } from "next/router";

//components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { Course } from "@/components/course";

//Models
import { ComboDetail } from "@/model/comboDetail.model";
import { RequestStatus } from "@/model/request-status";

//Hooks
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";

//External
import axios from "axios";
import { API_URL } from "../../../../constants";
import { CartContext } from "@/contexts/CartContext";
import { ComboProps } from "@/model/comboProps.model";
import { CartItem } from "@/model/cart.model";

export default function Combos() {

  const initialCombo: ComboDetail = {
    combo: {
      CategoryCourse: {
        Id: 0, Name: ''
      },
      Id: 0,
      Name: '',
      CourseHourTotal: 0,
      CreateOn: '',
      Description: '',
      Img: '',
      NumCourses: 0,
      UpdateOn: '',
      Price: 0,
      PriceTotal: 0,
      ComboValueWithoutDiscount: 0,
      PercentageDiscountForTheCombo: 0,
      DiscountValueForTheCombo: 0,
      TotalValueOfTheComboWithTheDiscount: 0,
    },
    courses: []
  }

  const [combo, setCombo] = useState<ComboDetail>(initialCombo);
  const [status, setStatus] = useState<RequestStatus>("init");
  const { currencyWithSuffixFormatter } = useCurrencyFormatter();

  const router = useRouter();
  const { idCombo } = router.query;

  const { addToCart } = useContext(CartContext);

  const handleAddToCart = (combo: ComboProps) => {
    const item: CartItem = { ...combo, type: "combo" };

    addToCart(item);
    router.push("/cart");
  };

  const getCombo = async () => {
    try {
      setStatus("loading");
      const response = await axios.get(
        API_URL + "coursePackage/getComboById?id=" + idCombo
      );
      let coursesData = response?.data?.Data;
      setCombo(coursesData);
      setStatus("success");
    } catch (error) {
      setStatus("error");
    }
  };

  useEffect(() => {
    getCombo();
  }, []);

  return (
    <>
      <Head>
        <title>Combos | CESCO</title>
      </Head>

      <div className="flex flex-col">
        <Header />

        <main
          className="w-full h-full flex-grow bg-zinc-100"
          id="main-content"
          role="main"
        >
          <section className="bg-center bg-no-repeat bg-cover bg-[url('https://images.pexels.com/photos/1454360/pexels-photo-1454360.jpeg')] bg-gray-700 bg-blend-multiply">
            <div className="container px-4 py-16 lg:py-20">
              <div className="row">
                {status === "success" && (
                  <>
                    <div className="col-12 col-md">
                      <div className="row">
                        <div className="col-12">
                          <p className="text-lg text-white font-semibold">
                            Combo de {combo?.combo.NumCourses} cursos
                          </p>
                          <h1 className="text-3xl text-white font-bold">
                            {combo?.combo.Name}
                          </h1>
                          <p className="text-base text-white">
                            {combo?.combo.Description}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-12 col-md-3">
                      <div className="row">
                        <div className="col-12 flex justify-center">
                          <span className="text-white text-center text-3xl font-bold">
                            {combo &&
                            combo.combo.PriceTotal &&
                            combo.combo.Price ? (
                              <>
                                {currencyWithSuffixFormatter(
                                  combo?.combo.Price
                                )}
                              </>
                            ) : (
                              ""
                            )}
                          </span>
                        </div>
                        <div className="col-12 flex justify-center mb-4">
                          <span className="text-white text-center font-base">
                            {combo &&
                            combo.combo.PriceTotal &&
                            combo.combo.Price ? (
                              <>
                                <span className="text-center">
                                  {Math.round(
                                    ((combo.combo.PriceTotal -
                                      combo?.combo.Price) /
                                      combo?.combo.PriceTotal) *
                                      100
                                  )}
                                  % Dto.
                                </span>
                                <b>
                                  <span className="line-through">
                                    {currencyWithSuffixFormatter(
                                      combo?.combo.PriceTotal
                                    )}
                                  </span>
                                </b>
                              </>
                            ) : (
                              ""
                            )}
                          </span>
                        </div>
                        <div className="col-12">
                          <button 
                            className="btn btn-primary-green w-full"
                            onClick={() => {
                              handleAddToCart(combo.combo);
                            }}
                          >
                            Comprar
                          </button>
                        </div>
                      </div>
                    </div>
                  </>
                )}
                {status === "loading" && (
                  <>
                    <div role="status" className="max-w-sm animate-pulse mb-4">
                      <div className="h-2.5 bg-zinc-300 rounded w-48 mb-4" />
                      <div className="h-2 bg-zinc-300 rounded max-w-[360px] mb-2.5" />
                      <div className="h-2 bg-zinc-300 rounded max-w-[330px] mb-2.5" />
                      <div className="h-2 bg-zinc-300 rounded max-w-[300px] mb-2.5" />
                      <div className="h-2 bg-zinc-300 rounded max-w-[360px]" />
                      <span className="sr-only">Loading...</span>
                    </div>
                  </>
                )}
              </div>
            </div>
          </section>

          <div className="container py-20">
            <div className="row">
              {status === "success" && (
                <>
                  <div className="col-12 mb-5">
                    <h1 className="font-extrabold text-center text-2xl text-zinc-700 mb-5">
                      Los {combo?.combo.NumCourses} cursos de este combo
                    </h1>
                  </div>
                  {combo?.courses.map((course, index) => {
                    course.isCombo = true;
                    return (
                      <div className="col-md-6 col-lg-3 mb-4" key={index}>
                        <Course {...course}></Course>
                      </div>
                    );
                  })}
                </>
              )}
              {status === "loading" &&
                [0, 1, 2, 3, 4, 5].map((item) => (
                  <>
                    <div
                      className="col-md-6 col-lg-3 mb-5"
                      key={"card-loading-" + item}
                    >
                      <div
                        role="status"
                        className="flex items-center justify-center h-40 max-w-sm bg-zinc-300 rounded animate-pulse mb-4"
                      >
                        <svg
                          className="w-12 h-12 text-zinc-200"
                          xmlns="http://www.w3.org/2000/svg"
                          aria-hidden="true"
                          fill="currentColor"
                          viewBox="0 0 384 512"
                        >
                          <path d="M361 215C375.3 223.8 384 239.3 384 256C384 272.7 375.3 288.2 361 296.1L73.03 472.1C58.21 482 39.66 482.4 24.52 473.9C9.377 465.4 0 449.4 0 432V80C0 62.64 9.377 46.63 24.52 38.13C39.66 29.64 58.21 29.99 73.03 39.04L361 215z" />
                        </svg>
                        <span className="sr-only">Loading...</span>
                      </div>
                      <div
                        role="status"
                        className="max-w-sm animate-pulse mb-4"
                      >
                        <div className="h-2.5 bg-zinc-300 rounded w-48 mb-4" />
                        <div className="h-2 bg-zinc-300 rounded max-w-[360px] mb-2.5" />
                        <div className="h-2 bg-zinc-300 rounded mb-2.5" />
                        <div className="h-2 bg-zinc-300 rounded max-w-[330px] mb-2.5" />
                        <div className="h-2 bg-zinc-300 rounded max-w-[300px] mb-2.5" />
                        <div className="h-2 bg-zinc-300 rounded max-w-[360px]" />
                        <span className="sr-only">Loading...</span>
                      </div>
                      <div
                        role="status"
                        className="max-w-sm animate-pulse mb-4"
                      >
                        <div className="h-10 bg-zinc-300 rounded w-full mb-4" />
                        <span className="sr-only">Loading...</span>
                      </div>
                    </div>
                  </>
                ))}
            </div>
          </div>
        </main>

        <Footer />
      </div>
    </>
  );
}
