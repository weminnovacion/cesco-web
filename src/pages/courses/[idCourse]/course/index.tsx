//React
import { useContext, useEffect, useState } from "react";
//Next
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";

//Components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { Breadcrumbs } from "@/components/Breadcrumbs";
import { Tabs } from "@/components/Tabs";
import { Tab } from "@headlessui/react";

//Models
import { CourseProps } from "@/model/courseProps.model";

//Hooks
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFilm,
  faLock,
  faEye,
  faClock,
  faUserAlt,
  faBook,
  faGraduationCap,
  faCartShopping,
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { format, intervalToDuration } from "date-fns";
import { parseCookies } from "nookies";
import classNames from "classnames";
import { RequestStatus } from "@/model/request-status";
import { API_URL } from "../../../../../constants";
import { CartContext } from "@/contexts/CartContext";
import { CartItem } from "@/model/cart.model";

export default function CoursePage() {
  const router = useRouter();
  const { idCourse } = router.query;
  const id = typeof idCourse === "string" ? idCourse.match(/^\d+/)?.[0] : null;
  const courseName =
    typeof idCourse === "string"
      ? idCourse.replace(/\d+[-]/g, "").replace(/-/g, " ")
      : null;
  const nameRouteCapitalize = courseName
    ? courseName.toString().charAt(0).toUpperCase() + courseName.slice(1)
    : "";

  const [course, setCourse] = useState<CourseProps>();
  const [status, setStatus] = useState<RequestStatus>("init");
  const [dateCreateProfessor, setDateCreateProfessor] = useState("");
  const { addToCart } = useContext(CartContext);

  const { currencyWithSuffixFormatter } = useCurrencyFormatter();

  const handleAddToCart = (course: any) => {
    const item: CartItem = { ...course, type: "course" };
    addToCart(item);
    router.push("/cart");
  };

  const breadcrumbs = [
    { name: "Cursos", url: "/courses" },
    { name: nameRouteCapitalize, url: "/courses" },
    { name: "Contenidos", url: "#" },
  ];

  const getCourseById = async () => {
    const cookies = parseCookies();
    const token = cookies.token;
    try {
      const config = {
        headers: {
          Authorization: token,
        },
      };
      setStatus("loading");
      const response = await axios.get(
        API_URL + "cours/getById?id=" + id,
        config
      );
      setStatus("success");
      let courseData = response.data.Data;
      setCourse(courseData);
      setDateCreateProfessor(courseData.Professor.CreateOn);
    } catch (error) {
      setStatus("error");
    }
  };

  useEffect(() => {
    if (id) {
      getCourseById();
    }
  }, [id]);

  const getFormattedDuration = (startDate: any, endDate: any) => {
    const duration = intervalToDuration({
      start: startDate,
      end: endDate,
    });
    return `${duration.days}d: ${duration.hours}h: ${duration.minutes}m: ${duration.seconds}s`;
  };

  return (
    <>
      <Head>
        <title>Aprende Online | CESCO</title>
      </Head>

      <Header />

      <main className="w-full " id="main-content" role="main">
        <div className="w-full border-b mb-8 py-4">
          <div className="container">
            <Breadcrumbs items={breadcrumbs} />
          </div>
        </div>
        <div className="w-full">
          <div className="container">
            {status === "loading" && (
              <>
                <div className="row">
                  <div className="col-12 col-md-8 mb-5">
                    <div className="row">
                      <div className="col-12 mb-5">
                        <div
                          role="status"
                          className="flex items-center justify-center h-56 max-w-full bg-gray-300 rounded-lg animate-pulse "
                        >
                          <svg
                            className="w-12 h-12 text-gray-200 "
                            xmlns="http://www.w3.org/2000/svg"
                            aria-hidden="true"
                            fill="currentColor"
                            viewBox="0 0 384 512"
                          >
                            <path d="M361 215C375.3 223.8 384 239.3 384 256C384 272.7 375.3 288.2 361 296.1L73.03 472.1C58.21 482 39.66 482.4 24.52 473.9C9.377 465.4 0 449.4 0 432V80C0 62.64 9.377 46.63 24.52 38.13C39.66 29.64 58.21 29.99 73.03 39.04L361 215z" />
                          </svg>
                          <span className="sr-only">Loading...</span>
                        </div>
                      </div>
                      <div className="col-12 mb-5">
                        <div
                          role="status"
                          className="space-y-2.5 animate-pulse max-w-lg"
                        >
                          <div className="flex items-center w-full space-x-2">
                            <div className="h-2.5 bg-gray-200 rounded-full w-32" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-24" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-full" />
                          </div>
                          <div className="flex items-center w-full space-x-2 max-w-[480px]">
                            <div className="h-2.5 bg-gray-200 rounded-full w-full" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-full" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-24" />
                          </div>
                          <div className="flex items-center w-full space-x-2 max-w-[400px]">
                            <div className="h-2.5 bg-gray-300 rounded-full w-full" />
                            <div className="h-2.5 bg-gray-200 rounded-full w-80" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-full" />
                          </div>
                          <div className="flex items-center w-full space-x-2 max-w-[480px]">
                            <div className="h-2.5 bg-gray-200 rounded-full w-full" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-full" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-24" />
                          </div>
                          <div className="flex items-center w-full space-x-2 max-w-[440px]">
                            <div className="h-2.5 bg-gray-300 rounded-full w-32" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-24" />
                            <div className="h-2.5 bg-gray-200 rounded-full w-full" />
                          </div>
                          <div className="flex items-center w-full space-x-2 max-w-[360px]">
                            <div className="h-2.5 bg-gray-300 rounded-full w-full" />
                            <div className="h-2.5 bg-gray-200 rounded-full w-80" />
                            <div className="h-2.5 bg-gray-300 rounded-full w-full" />
                          </div>
                          <span className="sr-only">Loading...</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-md-4 ">
                    <div role="status" className="max-w-sm animate-pulse mb-5">
                      <div className="h-2.5 bg-gray-200 rounded-full w-48 mb-4" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[360px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[330px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[300px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[360px]" />
                      <span className="sr-only">Loading...</span>
                    </div>
                    <div role="status" className="max-w-sm animate-pulse mb-5">
                      <div className="h-2.5 bg-gray-200 rounded-full w-48 mb-4" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[360px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[330px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[300px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[360px]" />
                      <span className="sr-only">Loading...</span>
                    </div>
                  </div>
                </div>
              </>
            )}
            {status === "success" && (
              <>
                <div className="row mb-12">
                  <div
                    className={classNames("col-12 ", {
                      "col-md-10": course?.AlreadyBought,
                      "col-md-8": !course?.AlreadyBought,
                    })}
                  >
                    <div className="row">
                      <div className="col-12 mb-4">
                        <h1 className="text-2xl font-bold mb-1">
                          {course?.Name}
                        </h1>
                        <h2 className="text-base">
                          Un curso de <strong>{course?.Professor.Name}</strong>,{" "}
                          {course?.Professor.Profession}
                        </h2>
                      </div>

                      <div className="col-12 mb-4">
                        <div className="w-full h-auto max-w-full border border-gray-200 rounded-lg overflow-hidden">
                          <div
                            style={{
                              padding: "56.25% 0 0 0",
                              position: "relative",
                            }}
                          >
                            <iframe
                              src={course?.UrlVideoPresentation}
                              frameBorder={0}
                              allow="autoplay; fullscreen; picture-in-picture"
                              allowFullScreen
                              style={{
                                position: "absolute",
                                top: 0,
                                left: 0,
                                width: "100%",
                                height: "100%",
                              }}
                              title="avril (2)"
                            />
                          </div>
                        </div>
                      </div>

                      <div className="col-12 mb-4">
                        <Tabs>
                          <Tab title="Presentación">
                            <div className="row">
                              <div className="col-12">
                                <h3 className="font-bold text-3xl my-3">
                                  Objetivo del curso
                                </h3>
                              </div>
                              <div className="col-12 mb-8">
                                <p className="mb-3">{course?.Description}</p>
                              </div>
                            </div>
                          </Tab>
                          <Tab title="Contenidos">
                            <div className="row">
                              <div className="col-12">
                                <ul className="mt-6 mb-12 list-none last:mb-12">
                                  <li>
                                    <h4 className="text-xl font-bold">
                                      Modulos
                                    </h4>
                                    <ul className="mt-6 pt-2 border-t list-none">
                                      {course?.Modules.map(
                                        (moduleItem, index) => (
                                          <li className="pl-12 mb-8 relative">
                                            <h5 className="my-2 text-lg">
                                              <Link
                                                href={`/courses/${id}-${courseName
                                                  ?.toLowerCase()
                                                  .replace(
                                                    /\s+/g,
                                                    "-"
                                                  )}/course/${
                                                  moduleItem.Id
                                                }-${moduleItem.Name?.toLowerCase().replace(
                                                  /\s+/g,
                                                  "-"
                                                )}/lessons`}
                                                className="hover:text-blue-600 transition-all duration-300"
                                              >
                                                <FontAwesomeIcon
                                                  icon={
                                                    moduleItem.OpenLesson ||
                                                    course?.AlreadyBought
                                                      ? faEye
                                                      : faLock
                                                  }
                                                  className="left-3 top-1 text-base absolute text-zinc-300 w-4 h-4"
                                                />
                                                {moduleItem.Name}
                                              </Link>
                                            </h5>
                                          </li>
                                        )
                                      )}
                                    </ul>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </Tab>
                        </Tabs>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-md-4">
                    <div className="row">
                      <div className="col-12">
                        <div className="bg-white">
                          {!course?.AlreadyBought && (
                            <>
                              <span className="text-3xl mb-2 block">
                                {course?.IsPromotion &&
                                  currencyWithSuffixFormatter(
                                    course.Price - course?.DisountedPrice
                                  )}
                                {!course?.IsPromotion &&
                                  course &&
                                  currencyWithSuffixFormatter(
                                    course.PriceWithDiscount
                                  )}
                              </span>
                              <span className="block mb-4 text-primary-orange">
                                {course?.IsPromotion && (
                                  <>
                                    <span>
                                      {course?.DiscountRate + "% Dto."}
                                    </span>
                                    <b className="line-through ml-2">
                                      {currencyWithSuffixFormatter(
                                        course.Price
                                      )}
                                    </b>
                                  </>
                                )}
                                {!course?.IsPromotion && course && (
                                  <>
                                    <span>
                                      {Math.round(
                                        ((course.Price -
                                          course.PriceWithDiscount) /
                                          course.Price) *
                                          100
                                      ) + "% Dto."}
                                    </span>
                                    <b className="line-through ml-2">
                                      {currencyWithSuffixFormatter(
                                        course.Price
                                      )}
                                    </b>
                                  </>
                                )}
                              </span>
                              <button
                                type="button"
                                onClick={() => {
                                  handleAddToCart(course);
                                }}
                                className={`btn btn-primary-green w-full flex items-center`}
                              >
                                <FontAwesomeIcon icon={faCartShopping} />
                                <span className="ml-2">Comprar </span>
                              </button>
                              <br />
                              {course?.IsPromotion && (
                                <>
                                  <div className="flex items-center text-primary-orange text-sm mb-4">
                                    <FontAwesomeIcon
                                      icon={faClock}
                                      className="mr-2"
                                    />
                                    <span>
                                      La oferta termina en{" "}
                                      {getFormattedDuration(
                                        new Date(
                                          course.ExpirationDate.toString()
                                        ),
                                        new Date()
                                      )}
                                    </span>
                                  </div>
                                </>
                              )}

                              <ul>
                                <li className="flex items-center gap-2 mb-3 text-xs">
                                  <FontAwesomeIcon
                                    icon={faUserAlt}
                                    className="text-primary-green"
                                  />
                                  <span>
                                    {course?.TotalStudent} estudiantes
                                  </span>
                                </li>
                                <li className="flex items-center gap-2 mb-3 text-xs">
                                  <FontAwesomeIcon
                                    icon={faFilm}
                                    className="text-primary-green"
                                  />
                                  <span>
                                    {course?.TotalModules} Modulos (
                                    {course?.HoursTotalCourse} Hr)
                                  </span>
                                </li>
                                <li className="flex items-center gap-2 mb-3 text-xs">
                                  <FontAwesomeIcon
                                    icon={faBook}
                                    className="text-primary-green"
                                  />
                                  <span>
                                    Categoria: {course?.CategoryCourse.Name}
                                  </span>
                                </li>
                                <li className="flex items-center gap-2 mb-3 text-xs">
                                  <FontAwesomeIcon
                                    icon={faGraduationCap}
                                    className="text-primary-green"
                                  />
                                  <span>Tipo: {course?.TypeCourse.Name}</span>
                                </li>
                                <li className="flex items-center gap-2 mb-3 text-xs">
                                  <FontAwesomeIcon
                                    icon={faClock}
                                    className="text-primary-green"
                                  />
                                  <span>Audio: Español</span>
                                </li>
                              </ul>
                            </>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <hr className="my-5" />
                  </div>
                  <div className="col-12 col-md-8">
                    <div className="col-12">
                      <div className="flex mb-8">
                        <div className="w-44 h-44 bg-zinc-200">
                          <img
                            src={course?.Professor.Photo}
                            alt={course?.Professor.Name}
                          />
                        </div>
                        <div className="ml-6 mb-6">
                          <h3 className="text-3xl fon-bold">
                            <h1 className="hover:text-primary-green transition-all duration-300">
                              {course?.Professor.Name}
                            </h1>
                          </h3>
                          <p className="text-zinc-400">
                            {course?.Professor.Profession}
                          </p>
                          <p className="text-black text-xs first-letter-uppercase">
                            Se unió en{" "}
                            {format(
                              new Date(
                                dateCreateProfessor
                                  ? dateCreateProfessor
                                  : "2019-01-01"
                              ),
                              "MMMM yyyy"
                            )}
                          </p>
                        </div>
                      </div>
                      <p>{course?.Professor.Biography}</p>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </main>
      <Footer />
    </>
  );
}
