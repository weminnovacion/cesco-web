//React
import { useContext, useEffect, useState } from "react";
import { useRouter } from "next/router";

//Next
import Head from "next/head";
import Link from "next/link";
import { format, intervalToDuration } from "date-fns";

//Components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { Tabs } from "@/components/Tabs";
import { Tab } from "@headlessui/react";
import { Breadcrumbs } from "@/components/Breadcrumbs";

//Models
import { CourseProps } from "@/model/courseProps.model";

//Externals
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLock,
  faClock,
  faEye,
  faFilePdf,
  faFileDownload,
  faBook,
  faCartShopping,
} from "@fortawesome/free-solid-svg-icons";
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";
import { parseCookies } from "nookies";
import { ModuleCourse } from "@/model/moduleCourse.model";
import { RequestStatus } from "@/model/request-status";
import { API_URL } from "../../../../../../../constants";
import { CartItem } from "@/model/cart.model";
import { CartContext } from "@/contexts/CartContext";

export default function LessonsPage() {
  const { currencyWithSuffixFormatter } = useCurrencyFormatter();
  const [dateCreateProfessor, setDateCreateProfessor] = useState("");
  const [status, setStatus] = useState<RequestStatus>("init");
  const [module, setModule] = useState<ModuleCourse>();
  const [course, setCourse] = useState<CourseProps>();
  const { addToCart } = useContext(CartContext);

  const router = useRouter();
  const { idCourse } = router.query;
  const { idLessons } = router.query;

  const id = typeof idCourse === "string" ? idCourse.match(/^\d+/)?.[0] : 0;
  const courseName =
    typeof idCourse === "string"
      ? idCourse.replace(/\d+[-]/g, "").replace(/-/g, " ")
      : null;
  const nameRouteCapitalize = courseName
    ? courseName.toString().charAt(0).toUpperCase() + courseName.slice(1)
    : "";

  const idles =
    typeof idLessons === "string" ? idLessons.match(/^\d+/)?.[0] : 0;
  const lessonName =
    typeof idLessons === "string"
      ? idLessons.replace(/\d+[-]/g, "").replace(/-/g, " ")
      : null;
  const lessonNameRouteCapitalize = lessonName
    ? lessonName.toString().charAt(0).toUpperCase() + lessonName.slice(1)
    : "";

  const breadcrumbs = [
    { name: "Cursos", url: "/courses" },
    {
      name: nameRouteCapitalize,
      url: "/courses/" + id + "-" + courseName + "/course",
    },
    { name: lessonNameRouteCapitalize, url: "#" },
  ];

  const handleAddToCart = (course: any) => {
    const item: CartItem = { ...course, type: "course" };
    addToCart(item);
    router.push("/cart");
  };

  const getCourseById = async () => {
    const cookies = parseCookies();
    const token = cookies.token;
    try {
      const config = {
        headers: {
          Authorization: token,
        },
      };
      setStatus("loading");
      const response = await axios.get(
        API_URL + "cours/getById?id=" + id,
        config
      );
      setStatus("success");
      let courseData = response.data.Data;
      setCourse(courseData);
      setDateCreateProfessor(courseData.Professor.CreateOn);
    } catch (error) {
      setStatus("error");
    }
  };

  useEffect(() => {
    if (id) {
      getCourseById();
    }
  }, [id, idLessons]);

  useEffect(() => {
    if (course && course.Modules) {
      setModule(course.Modules.find((e) => e.Id == idles));
    }
  }, [course]);

  const getFormattedDuration = (startDate: any, endDate: any) => {
    const duration = intervalToDuration({
      start: startDate,
      end: endDate,
    });

    return `${duration.days}d: ${duration.hours}h: ${duration.minutes}m: ${duration.seconds}s`;
  };

  return (
    <>
      <Head>
        <title>
          Presentación | Productividad para diseñadores: crea un flujo de
          trabajo eficiente | CESCO
        </title>
      </Head>

      <Header />

      <main className="w-full " id="main-content" role="main">
        <div className="w-full border-b mb-8 py-4">
          <div className="container">
            <Breadcrumbs items={breadcrumbs} />
          </div>
        </div>
        <div className="w-full">
          <div className="container">
            {status === "loading" && (
              <>
                <div className="row">
                  <div className="col-12 col-md-8 mb-5">
                    <div className="row">
                      <div className="col-12 mb-5">
                        <div
                          role="status"
                          className="flex items-center justify-center h-[27.1875rem] max-w-full bg-gray-300 rounded-lg animate-pulse "
                        >
                          <svg
                            className="w-14 h-14 text-gray-200 "
                            xmlns="http://www.w3.org/2000/svg"
                            aria-hidden="true"
                            fill="currentColor"
                            viewBox="0 0 384 512"
                          >
                            <path d="M361 215C375.3 223.8 384 239.3 384 256C384 272.7 375.3 288.2 361 296.1L73.03 472.1C58.21 482 39.66 482.4 24.52 473.9C9.377 465.4 0 449.4 0 432V80C0 62.64 9.377 46.63 24.52 38.13C39.66 29.64 58.21 29.99 73.03 39.04L361 215z" />
                          </svg>
                          <span className="sr-only">Loading...</span>
                        </div>
                      </div>
                      <div className="col-12 mb-5">
                        <div role="status" className="animate-pulse mb-5">
                          <div className="h-8 bg-gray-200 rounded-full w-4/5 mb-4" />
                          <div className="h-2 bg-gray-200 rounded-full max-w-[360px] mb-2.5" />
                          <span className="sr-only">Loading...</span>
                        </div>
                      </div>
                      <div className="col-12 mb-5">
                        <div
                          role="status"
                          className="space-y-8 animate-pulse md:space-y-0 md:space-x-8 md:flex md:items-center"
                        >
                          <div className="flex items-center justify-center w-full h-48 bg-gray-300 rounded sm:w-96 ">
                            <svg
                              className="w-12 h-12 text-gray-200"
                              xmlns="http://www.w3.org/2000/svg"
                              aria-hidden="true"
                              fill="currentColor"
                              viewBox="0 0 640 512"
                            >
                              <path d="M480 80C480 35.82 515.8 0 560 0C604.2 0 640 35.82 640 80C640 124.2 604.2 160 560 160C515.8 160 480 124.2 480 80zM0 456.1C0 445.6 2.964 435.3 8.551 426.4L225.3 81.01C231.9 70.42 243.5 64 256 64C268.5 64 280.1 70.42 286.8 81.01L412.7 281.7L460.9 202.7C464.1 196.1 472.2 192 480 192C487.8 192 495 196.1 499.1 202.7L631.1 419.1C636.9 428.6 640 439.7 640 450.9C640 484.6 612.6 512 578.9 512H55.91C25.03 512 .0006 486.1 .0006 456.1L0 456.1z" />
                            </svg>
                          </div>
                          <div className="w-full">
                            <div className="h-2.5 bg-gray-200 rounded-full  w-48 mb-4" />
                            <div className="h-2 bg-gray-200 rounded-full  max-w-[480px] mb-2.5" />
                            <div className="h-2 bg-gray-200 rounded-full  mb-2.5" />
                            <div className="h-2 bg-gray-200 rounded-full  max-w-[440px] mb-2.5" />
                            <div className="h-2 bg-gray-200 rounded-full  max-w-[460px] mb-2.5" />
                            <div className="h-2 bg-gray-200 rounded-full  max-w-[360px]" />
                          </div>
                          <span className="sr-only">Loading...</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-md-4 ">
                    <div role="status" className="max-w-sm animate-pulse mb-5">
                      <div className="h-2.5 bg-gray-200 rounded-full w-48 mb-4" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[360px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[330px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[300px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[360px]" />
                      <span className="sr-only">Loading...</span>
                    </div>
                    <div role="status" className="max-w-sm animate-pulse mb-5">
                      <div className="h-2.5 bg-gray-200 rounded-full w-48 mb-4" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[360px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[330px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[300px] mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full max-w-[360px]" />
                      <span className="sr-only">Loading...</span>
                    </div>
                  </div>
                </div>
              </>
            )}
            {status === "success" && (
              <>
                <div className="row mb-12">
                  <div className="col-12 col-md-8">
                    <div className="row">
                      <div className="col-12 mb-4">
                        <div className="row">
                          <div className="col-12 mb-4">
                            <div className="w-full h-auto max-w-full border border-gray-200 rounded-lg overflow-hidden">
                              <div
                                style={{
                                  padding: "56.25% 0 0 0",
                                  position: "relative",
                                }}
                              >
                                <iframe
                                  src={module?.UrlVideo}
                                  frameBorder={0}
                                  allow="autoplay; fullscreen; picture-in-picture"
                                  allowFullScreen
                                  style={{
                                    position: "absolute",
                                    top: 0,
                                    left: 0,
                                    width: "100%",
                                    height: "100%",
                                  }}
                                  title="avril (2)"
                                />
                              </div>
                            </div>
                          </div>

                          <div className="col-12 mb-4">
                            <div className="row">
                              <div className="col-12">
                                <h1 className="text-2xl font-bold mb-1">
                                  {course?.Name}: {module?.Name}
                                </h1>
                                <h2 className="text-base">
                                  Un curso de{" "}
                                  <strong>{course?.Professor.Name}</strong>,{" "}
                                  {course?.Professor.Profession}
                                </h2>
                              </div>
                            </div>
                          </div>

                          <div className="col-12 mb-4">
                            <Tabs>
                              <Tab title="Descripción">
                                <div className="row">
                                  <div className="col-12">
                                    <h3 className="font-bold text-lg my-3">
                                      Descripción
                                    </h3>
                                  </div>

                                  <div className="col-12 mb-8">
                                    <p className="mb-3">
                                      {module?.Description}
                                    </p>
                                  </div>
                                </div>
                              </Tab>
                              <Tab title="Material de apoyo">
                                <div className="row">
                                  <div className="col-12 mb-4">
                                    <h3 className="font-bold text-lg my-3">
                                      Material de apoyo
                                    </h3>
                                  </div>
                                  <div className="col-12">
                                    {module?.Resources.map((e, i) => {
                                      return (
                                        <a
                                          href={e.Value}
                                          key={i}
                                          download={e.Name.trim()}
                                          target="_blank"
                                          className=" block p-2 mb-4 bg-white border-b hover:bg-zinc-300/50 transition-all duration-200"
                                        >
                                          <div className="row">
                                            <div className="col-auto">
                                              <div className="w-8 h-8 flex justify-center items-center ">
                                                <FontAwesomeIcon
                                                  icon={faFilePdf}
                                                />
                                              </div>
                                            </div>
                                            <div className="col">
                                              <div className="flex items-center h-full">
                                                <h3 className="text-sm text-zinc-900 font-bold text-ellipsis max-w-full">
                                                  {e.Name}
                                                </h3>
                                              </div>
                                            </div>
                                            <div className="col-auto">
                                              <div className="w-8 h-8 flex justify-center items-center ">
                                                <FontAwesomeIcon
                                                  icon={faFileDownload}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </a>
                                      );
                                    })}
                                  </div>
                                </div>
                              </Tab>
                              {/* <Tab title="Preguntas y respuestas">
                                  <div className="flex flex-wrap -mx-3">
                                    <div className="w-full px-3">
                                      <h2 className="text-lg font-bold text-zinc-900 mb-4">Preguntas destacadas en este curso <span className="text-zinc-600">(11)</span></h2>
                                      
                                      {isPublicMessage ? (
                                        <>

                                          <div className="flex flex-wrap -mx-3">
                                            <div className="flex-grow-1 flex-shrink-0 basis-auto w-auto max-w-full ml-auto px-3">
                                              <button className="btn btn-default btn-flat flex items-center text-sm" onClick={togglePublicMessage}>
                                                Volver a todas las preguntas
                                                <FontAwesomeIcon icon={faPlus} className="ml-2" />
                                              </button>
                                            </div>
                                          </div>

                                          <div className="flex flex-wrap -mx-3 mt-5">
                                            <div className="flex-grow-1 flex-shrink-0 basis-auto w-full max-w-full px-3 mb-4">
                                              <div className="border border-zinc-400 p-3">
                                                <h4 className="text-lg font-bold">
                                                  Consejos para obtener respuestas a tus preguntas más rápido
                                                </h4>
                                                <ul className="list-decimal pl-5">
                                                  <li className="pl-3 text-sm">
                                                    Busca para comprobar si tu pregunta se ha preguntado antes
                                                  </li>
                                                  <li className="pl-3 text-sm">
                                                    Revisa la gramática y la ortografía
                                                  </li>
                                                </ul>
                                              </div>
                                            </div>

                                            <div className="flex-grow-1 flex-shrink-0 basis-auto w-full max-w-full px-3 mb-4">
                                              <label htmlFor="message" className="block mb-2 text-sm font-bold text-gray-900 dark:text-white">Título o resumen</label>
                                              <textarea id="message" rows={4} className="block p-2.5 w-full text-sm text-gray-900  border border-gray-300 focus:ring-blue-500 focus:border-blue-500dark:focus:border-blue-500 resize-none" placeholder="P.ej.: En el minuto 05:28, no he entendido esta parte, Aquí tienes una captura de pantalla de lo que intente hacer..."></textarea>
                                            </div>
                                            
                                            <div className="flex-grow-1 flex-shrink-0 basis-auto w-full max-w-full px-3 mb-4">
                                              <button className="btn btn-primary-green w-full" type="button">
                                                <span>Publicar</span>
                                              </button>
                                            </div>
                                          </div> 
                                        </>
                                      ) : (
                                        <>

                                          <div className="flex flex-wrap -mx-3">
                                            <div className="flex-grow-1 flex-shrink-0 basis-auto w-auto max-w-full ml-auto px-3">
                                              <button className="btn btn-default btn-flat flex items-center text-sm" onClick={togglePublicMessage}>
                                                Hacer otra pregunta
                                                <FontAwesomeIcon icon={faPlus} className="ml-2" />
                                              </button>
                                            </div>
                                          </div>

                                          <div className="flex flex-wrap -mx-3">
                                            <div className="flex-grow flex-shrink-0 basis-0 w-full max-w-full px-3 py-4 mb-4 bg-white hover:bg-zinc-300/50 transition-all duration-200">
                                              <div className="flex flex-wrap -mx-3">
                                                <div className="flex-grow-0 flex-shrink-0 basis-auto w-auto max-w-full px-3">
                                                  <div className="w-8 h-8 rounded-full bg-zinc-400"></div>
                                                </div>
                                                <div className="flex-grow flex-shrink-0 basis-0 w-full max-w-full px-3">
                                                  <h3 className="text-sm text-zinc-900 font-bold text-ellipsis max-w-full">
                                                    Solución!!! Ubicación correcta en Windows del archivo .db (archivo de la base de datos) + Bonus Linux
                                                  </h3>
                                                  <p className="text-xs text-zinc-900 mb-4">
                                                    Buenas tardes colegas,Encontrar el archivo .db puede ser un poco complejo por lo que todos tenem
                                                  </p>
                                                  <div className="flex items-center">
                                                    <a href="#" className="text-xs underline text-blue-600 mr-2">Jose Acevedo</a>
                                                    {" - "}
                                                    <span className="text-xs ml-2">
                                                      hace 1 mes
                                                    </span>
                                                  </div>
                                                </div>
                                                <div className="flex-grow-0 flex-shrink-0 basis-auto w-auto max-w-full px-3">
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </>
                                      )}
                                      

                                    </div>
                                  </div>
                                </Tab> */}
                            </Tabs>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="hidden md:block col-md-4">
                    <div className="row">
                      {!course?.AlreadyBought && (
                        <>
                          <div className="col-12">
                            <div className="bg-white">
                              <span className="text-3xl mb-2 block">
                                {course?.IsPromotion &&
                                  currencyWithSuffixFormatter(
                                    course.Price - course?.DisountedPrice
                                  )}
                                {!course?.IsPromotion &&
                                  course &&
                                  currencyWithSuffixFormatter(
                                    course.PriceWithDiscount
                                  )}
                              </span>
                              <span className="block mb-4 text-primary-orange">
                                {course?.IsPromotion && (
                                  <>
                                    <span>
                                      {course?.DiscountRate + "% Dto."}
                                    </span>
                                    <b className="line-through ml-2">
                                      {currencyWithSuffixFormatter(
                                        course.Price
                                      )}
                                    </b>
                                  </>
                                )}
                                {!course?.IsPromotion && course && (
                                  <>
                                    <span>
                                      {Math.round(
                                        ((course.Price -
                                          course.PriceWithDiscount) /
                                          course.Price) *
                                          100
                                      ) + "% Dto."}
                                    </span>
                                    <b className="line-through ml-2">
                                      {currencyWithSuffixFormatter(
                                        course.Price
                                      )}
                                    </b>
                                  </>
                                )}
                              </span>
                              <button
                                type="button"
                                onClick={() => {
                                  handleAddToCart(course);
                                }}
                                className={`btn btn-primary-green w-full flex items-center`}
                              >
                                <FontAwesomeIcon icon={faCartShopping} />
                                <span className="ml-2">Comprar </span>
                              </button>
                              {course?.IsPromotion && (
                                <>
                                  <div className="flex items-center text-primary-orange text-sm mb-4">
                                    <FontAwesomeIcon
                                      icon={faClock}
                                      className="mr-2"
                                    />
                                    <span>
                                      La oferta termina en{" "}
                                      {getFormattedDuration(
                                        new Date(
                                          course.ExpirationDate.toString()
                                        ),
                                        new Date()
                                      )}
                                    </span>
                                  </div>
                                </>
                              )}
                            </div>
                          </div>
                          <div className="col-12">
                            <hr className="my-5" />
                          </div>
                        </>
                      )}
                      <div className="col-12">
                        <nav role="navigation">
                          <ul className="flex flex-col flex-nowrap mb-12 list-none">
                            <li>
                              <a className="block relative pr-6 text-xs uppercase bg-transparent before:content-[''] before:absolute before:top-0 before:-left-2 before:w-1 before:h-[0.875rem] before:bg-zinc-900 ">
                                Contenido del curso
                              </a>
                              <ul className="mt-6 mx-0 mb-4 list-none ">
                                <li className="active">
                                  <ul className="mt-3 mx-0 mb-8 list-none">
                                    {course?.Modules.map((e, i) => {
                                      return (
                                        <>
                                          <li className="mb-3" key={e.Id}>
                                            <Link
                                              href={`/courses/${id}-${courseName
                                                ?.toLowerCase()
                                                .replace(/\s+/g, "-")}/course/${
                                                e.Id
                                              }-${e.Name?.toLowerCase().replace(
                                                /\s+/g,
                                                "-"
                                              )}/lessons`}
                                              className="pr-8 text-xs block relative text-blue hover:text-primary-green transition-all duration-300  "
                                            >
                                              <span className="overflow-hidden text-ellipsis whitespace-nowrap block min-w-0">
                                                {e.Name}
                                              </span>
                                              <FontAwesomeIcon
                                                icon={
                                                  e.OpenLesson ||
                                                  course?.AlreadyBought
                                                    ? faEye
                                                    : faLock
                                                }
                                                className="absolute top-1/2 text-zinc-500 right-0 -translate-y-1/2"
                                              />
                                            </Link>
                                          </li>
                                        </>
                                      );
                                    })}
                                    {course?.AlreadyBought &&
                                      course.ActiveEvaluation &&
                                      !course.IsCertificate && (
                                        <>
                                          <li className="mb-3">
                                            <Link
                                              href={`/courses/exam/` + id}
                                              className="pr-8 text-xs block relative text-blue hover:text-primary-green transition-all duration-300  "
                                            >
                                              <span className="overflow-hidden text-ellipsis whitespace-nowrap block min-w-0">
                                                Presentar examen
                                              </span>
                                              <FontAwesomeIcon
                                                icon={faBook}
                                                className="absolute top-1/2 text-zinc-500 right-0 -translate-y-1/2"
                                              />
                                            </Link>
                                          </li>
                                        </>
                                      )}
                                  </ul>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                  </div>

                  <div className="col-12">
                    <hr className="my-5" />
                  </div>
                  <div className="col-12 col-md-8">
                    <div className="row">
                      <div className="col-12">
                        <div className="flex mb-8">
                          <div className="w-44 h-44 bg-zinc-200">
                            <img
                              src={course?.Professor.Photo}
                              alt={course?.Professor.Name}
                            />
                          </div>
                          <div className="ml-6 mb-6">
                            <h3 className="text-3xl fon-bold">
                              <h1 className="hover:text-primary-green transition-all duration-300">
                                {course?.Professor.Name}
                              </h1>
                            </h3>
                            <p className="text-zinc-400">
                              {course?.Professor.Profession}
                            </p>
                            <p className="text-black text-xs first-letter-uppercase">
                              Se unió en{" "}
                              {format(
                                new Date(
                                  dateCreateProfessor
                                    ? dateCreateProfessor
                                    : "2019-01-01"
                                ),
                                "MMMM yyyy"
                              )}
                            </p>
                          </div>
                        </div>
                        <p>{course?.Professor.Biography}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </main>
      <Footer />
    </>
  );
}
