//Next
import Head from "next/head";

//components
import { Header } from "@/components/Header";
import { TakeExamForm } from "@/components/take-exam-form";

export default function TakeExam() {
  return (
    <>
      <Head>
        <title>Tomar examen | CESCO</title>
      </Head>

      <div className="flex flex-col w-full h-full">
        <Header></Header>
        <main className="w-full flex-grow" id="main-content" role="main">
          <TakeExamForm />
        </main>
      </div>
    </>
  );
}
