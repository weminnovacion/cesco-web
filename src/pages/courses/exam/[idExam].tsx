//Next
import Link from "next/link";
import Head from "next/head";

//components
import { Header } from "@/components/Header";
import { parseCookies } from "nookies";
import { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { RequestStatus } from "@/model/request-status";
import { API_URL } from "../../../../constants";

export default function Exam() {
  const router = useRouter();
  const { idExam } = router.query;

  const [status, setStatus] = useState<RequestStatus>("init");
  const [exam, setExam] = useState<ExamPrecentation>();

  const getPrecentation = async () => {
    const cookies = parseCookies();
    const token = cookies.token;

    try {
      const config = {
        headers: {
          Authorization: token,
        },
      };
      setStatus("loading");
      const response = await axios.get(
        API_URL +
          "evaluationCourse/GetInfoCabEvaluationbyCourseId?id=" +
          idExam,
        config
      );
      setStatus("success");
      let examData = response.data.Data;

      setExam(examData);
    } catch (error) {
      setStatus("error");
    }
  };

  useEffect(() => {
    if (idExam) {
      getPrecentation();
    }
  }, [idExam]);

  return (
    <>
      <Head>
        <title>Examen | CESCO</title>
      </Head>

      <div className="flex flex-col w-full h-full">
        <Header></Header>

        <main className="w-full h-full flex-grow" id="main-content" role="main">
          <div className="container my-10">
            {status === "success" && (
              <div className="row">
                <div className="col-12">
                  <h2 className="text-3xl text-zinc-700 font-bold mb-1">
                    Excelente, validarás todo lo aprendido
                  </h2>
                  <p className="text-lg text-zinc-700 font-medium">
                    {exam?.NameCourse}
                  </p>
                  <hr className="my-4" />

                  <p className="text-base text-zinc-700 font-medium mb-3">
                    Para certificarte, puedes presentar el examen o publicar un
                    proyecto.
                  </p>

                  <p className="text-xl text-zinc-700 font-bold mb-4">
                    Presentar el examen
                  </p>

                  <ul className="list-disc pl-5 mb-4">
                    <li>
                      Puedes presentar el examen un total de 5 veces (Una vez
                      excedido este límite de intentos, debería comprar la
                      evaluación una vez más).
                    </li>
                    <li>
                      El examen consta de <strong>{exam?.NumQuestion}</strong>{" "}
                      preguntas.
                    </li>
                    <li>
                      Tienes <strong>30</strong> minutos para presentarlo.
                    </li>
                    <li>
                      Necesitas <strong>{exam?.MinQuestiosTrue}</strong>{" "}
                      respuestas correctas para aprobarlo.
                    </li>
                    <li>
                      Porcentaje de aprobacion mayor a <strong>80%</strong>.
                    </li>
                  </ul>
                </div>
                <div className="row">
                  <div className="col-auto">
                    <Link
                      href={`/courses/exam/tomar-examen/${idExam}`}
                      type="button"
                      className="btn btn-primary-green"
                    >
                      Presentar examen
                    </Link>
                  </div>
                </div>
              </div>
            )}

            {status === "loading" && (
              <div className="row">
                <div className="col-12">
                  <div role="status" className="max-w-xl animate-pulse">
                    <div className="h-9 bg-gray-300 rounded max-w-[380px] mb-1"></div>
                    <div className="h-7 bg-gray-200 rounded max-w-[420px] mb-2.5"></div>
                    <hr className="my-4" />
                    <div className="h-6 bg-gray-200 rounded max-w-[360px] mb-3"></div>
                    <div className="h-7 bg-gray-300 rounded max-w-[320px] mb-2.5"></div>
                    <div className="h-2 bg-gray-200 rounded max-w-[360px] mb-2.5"></div>
                    <div className="h-2 bg-gray-200 rounded max-w-[300px] mb-2.5"></div>
                    <div className="h-2 bg-gray-200 rounded max-w-[200px] mb-2.5"></div>
                    <div className="h-2 bg-gray-200 rounded max-w-[360px] mb-2.5"></div>
                    <div className="h-2 bg-gray-200 rounded max-w-[380px] mb-2.5"></div>
                    <span className="sr-only">Loading...</span>
                  </div>
                </div>
              </div>
            )}
          </div>
        </main>
      </div>
    </>
  );
}
