import Head from "next/head";

//components
import { Header } from "@/components/Header";

export default function ResultExam() {
  return (
    <>
      <Head>
        <title>Resultados | CESCO</title>
      </Head>

      <div className="flex flex-col w-full h-full">
        <Header></Header>

        <main className="w-full flex-grow" id="main-content" role="main">
          <div className="container my-10">
            <div className="row">
              <div className="col-12">
                <p className="text-lg text-zinc-700 font-bold ">
                  Curso de Angular Forms: Creación y Optimización de Formularios
                  Web
                </p>
                <hr className="my-4" />
              </div>
            </div>
            <div className="row">
              <div className="col">
                <p className="text-2xl text-zinc-700 font-bold mb-3">
                  ¡No te rindas!
                </p>
                <p className="text-zinc-700">
                  Necesitas una calificación mínima de 9.0 para aprobar.
                </p>
              </div>
              <div className="col-auto">
                <div className="bg-zinc-200 rounded px-8 py-4">
                  <div className="flex items-center">
                    <div className="">
                      <p className="text-5xl font-bold text-zinc-700 text-center">
                        0
                      </p>
                      <p className="text-zinc-700 text-center">Calificación</p>
                    </div>
                    <div className="h-16 border-r border-zinc-700 mx-4"></div>
                    <div className="">
                      <p className="text-3xl font-medium  text-center">
                        <span className="text-zinc-700">0</span>{" "}
                        <span className="text-zinc-500">/ 27</span>
                      </p>
                      <p className="text-zinc-700 text-center">Aciertos</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-auto">
                <button className="btn btn-primary-green" type="button">
                  Volver a intentar
                </button>
              </div>
            </div>
          </div>
        </main>
      </div>
    </>
  );
}
