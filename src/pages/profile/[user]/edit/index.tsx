//React
import { ChangeEvent, useContext, useEffect, useState } from "react";
//Next
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
//components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { Breadcrumbs } from "@/components/Breadcrumbs";
import { Modal } from "@/components/Modal";
//Context
import { AuthContext } from "@/contexts/AuthContext";
//Models
import { RequestStatus } from "@/model/request-status";
//Externals
import axios from "axios";
import classNames from "classnames";
import { useForm } from "react-hook-form";
import { API_URL } from "../../../../../constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { parseCookies } from "nookies";
import { AvatarUploader } from "@/components/AvatarUploader";

export default function User() {
  const { userInfo } = useContext(AuthContext);

  const breadcrumbs = [
    { name: "Mi perfil", url: "/profile/" + userInfo?.Username },
    { name: "Editar perfil", url: "/profile/" + userInfo?.Username + "/edit" },
  ];

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    setError,
  } = useForm<RegisterFormInput>();
  const [Indentifications, setIdentifications] = useState<Identification[]>([]);
  const [profession, setProfession] = useState<Profession[]>([]);
  const [gender, setGender] = useState<Gender[]>([]);
  const [countries, setCountries] = useState<Countries[]>([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [status, setStatus] = useState<RequestStatus>("init");

  const [selectedCountry, setSelectedCountry] = useState<Countries | undefined>(
    undefined
  );
  const [selectedDepartment, setSelectedDepartment] = useState<
    Departaments | undefined
  >(undefined);
  const [selectedCity, setSelectedCity] = useState<Cities | undefined>(
    undefined
  );

  let data = {
    pageSize: 50,
    pageNumber: 1,
  };

  const getTypeIdentification = async () => {
    try {
      const response = await axios.get(API_URL + "typeIndentification/page", {
        data,
      });
      return response.data?.Data;
    } catch (error) {}
  };

  const getTypeProfession = async () => {
    try {
      const response = await axios.get(API_URL + "profession/page", {
        data,
      });
      return response.data?.Data?.Content;
    } catch (error) {}
  };

  const getGender = async () => {
    try {
      const response = await axios.get(API_URL + "gender/page", {
        data,
      });
      return response.data?.Data;
    } catch (error) {}
  };

  const getUbiGeo = async () => {
    try {
      const response = await axios.get(API_URL + "location/getCountry");
      return response.data?.Data;
    } catch (error) {}
  };

  const onSubmit = async (data: RegisterFormInput) => {
    const cookies = parseCookies();
    const token = cookies.token;

    try {
      const config = {
        headers: {
          Authorization: token,
        },
      };

      setStatus("loading");
      const response = await axios.put(
        API_URL + "user/updateInfo",
        data,
        config
      );
      if (response.status == 200) {
        if (response.data?.Status == 200) {
          setStatus("success");
          handleToggleModal();
        } else {
          setStatus("error");
          setTimeout(() => {
            setStatus("init");
          }, 3000);

          response.data?.Data.map((e: any, i: number) => {
            setError(e.Key, {
              message: e.Messages[0],
            });
          });
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const fetchData = async () => {
    setStatus("loading");
    try {
      const [
        responseTypeIdentification,
        responseTypeProfession,
        responseUbiGeo,
        responseGender,
      ] = await Promise.all([
        getTypeIdentification(),
        getTypeProfession(),
        getUbiGeo(),
        getGender(),
      ]);

      setGender(responseGender);
      setIdentifications(responseTypeIdentification);
      setProfession(responseTypeProfession);
      setCountries(responseUbiGeo);

      if (userInfo) {
        console.log('userInfo');
        
        setValue("IdTypeIdentification", userInfo.TypeIdentification.Id);
        setValue("Identification", userInfo.Identification);
        setValue("IdProfession", userInfo.Profession.Id);
        setValue("FirstName", userInfo.FirstName);
        setValue("SecondName", userInfo.SecondName);
        setValue("SecondSurname", userInfo.SecondSurname);
        setValue("Surname", userInfo.Surname);
        setValue("Address", userInfo.Address);
        setValue(
          "Birthdate",
          new Date(userInfo.Birthdate).toISOString().split("T")[0]
        );
        setValue("Biography", userInfo.Biography);
        setValue("Email", userInfo.Email);
        setValue("IdGender", userInfo.Gender.Id);
        setValue("Phone", userInfo.Phone);
        setValue("IdCity", userInfo.City.Id);
        setValue("IdDepartment", userInfo.Department.Id);
        setValue("IdCountry", userInfo.Country.Id);
      }
      setStatus("success");
    } catch (error) {
      setStatus("error");
      setTimeout(() => {
        setStatus("init");
      }, 3000);
    }
  };

  useEffect(() => {
    fetchData();
  }, [userInfo]);

  useEffect(() => {
    if (countries) {
      const countryList = countries.find(
        (country) => country.Id === getValues("IdCountry")
      );
      setSelectedCountry(countryList);
    }
  }, [countries]);

  useEffect(() => {
    if (selectedCountry) {
      const departmentsList = selectedCountry?.Departaments.find(
        (department) => department.Id === getValues("IdDepartment")
      );
      setSelectedDepartment(departmentsList);
    }
  }, [selectedCountry]);

  const handleToggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const handleCountryChange = (event: any) => {
    const countryId = parseInt(event.target.value);
    const selectedCountry = countries.find(
      (country) => country.Id === countryId
    );
    setSelectedCountry(selectedCountry);
    setSelectedDepartment(undefined);
    setSelectedCity(undefined);
  };

  const handleDepartmentChange = (event: any) => {
    const departmentId = parseInt(event.target.value);
    const selectedDepartment = selectedCountry?.Departaments.find(
      (department: any) => department.Id === departmentId
    );
    setSelectedDepartment(selectedDepartment);
    setSelectedCity(undefined);
  };

  const handleCityChange = (event: any) => {
    const cityId = parseInt(event.target.value);
    const selectedCity = selectedDepartment?.Cities.find(
      (city: any) => city.Id === cityId
    );
    setSelectedCity(selectedCity);
  };

  return (
    <>
      <Head>
        <title>Editar perfil | CESCO</title>
      </Head>

      <Header />
      <main className="w-full bg-zinc-100" id="main-content" role="main">
        <div className="w-full border-b mb-8 py-4">
          <div className="container">
            <Breadcrumbs items={breadcrumbs} />
          </div>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-3 hidden md:block">
              <nav role="navigation mb-12">
                <ul className="flex flex-wrap pl-0 list-none flex-col mb-4">
                  <li className="relative mb-2">
                    <Link
                      href={`/profile/${userInfo?.Username}/edit`}
                      className="inline-block text-sm text-black hover:text-blue-600 transition-all duration-300"
                    >
                      Editar perfil
                    </Link>
                  </li>
                  <li className="relative mb-2">
                    <Link
                      href={`/profile/${userInfo?.Username}/settings`}
                      className="inline-block text-sm text-black hover:text-blue-600 transition-all duration-300"
                    >
                      Configuración de cuenta
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
            <div className="col-12 col-md-9">
              <div className="row">
                <AvatarUploader />
                <div className="col-12">
                  <form
                    onSubmit={handleSubmit(onSubmit)}
                    className="w-full bg-white mb-4 p-8"
                  >
                    <div className="row">
                      <div className="col-12 mb-4">
                        <h3 className="text-lg font-bold text-black">
                          SOBRE TI
                        </h3>
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="IdTypeIdentification"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Tipo de identificación
                        </label>
                        <select
                          id="IdTypeIdentification"
                          className={classNames("form-control", {
                            invalid: errors.IdTypeIdentification,
                          })}
                          disabled
                          {...register("IdTypeIdentification", {
                            required: "Este campos es obligatorio",
                          })}
                        >
                          <option value="">Seleccione...</option>
                          {Indentifications.map((item) => (
                            <option key={item.Id} value={item.Id}>
                              {item.Name}
                            </option>
                          ))}
                        </select>
                        {errors.IdTypeIdentification && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.IdTypeIdentification.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="Identification"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Identificación
                        </label>
                        <input
                          type="text"
                          id="Identification"
                          className={classNames("form-control", {
                            invalid: errors.Identification,
                          })}
                          placeholder="000 000 0000"
                          disabled
                          {...register("Identification", {
                            required: "Este campos es obligatorio",
                            pattern: {
                              value: /^[0-9]+$/,
                              message: "Solo se permiten números en este campo",
                            },
                            minLength: 7,
                            maxLength: 10,
                          })}
                        />
                        {errors.Identification && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.Identification.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="IdProfession"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Profesión
                        </label>
                        <select
                          id="IdProfession"
                          className={classNames("form-control", {
                            invalid: errors.IdProfession,
                          })}
                          disabled
                          {...register("IdProfession", {
                            required: "Este campos es obligatorio",
                          })}
                        >
                          <option value="">Seleccione...</option>
                          {profession.map((item) => (
                            <option key={item.Id} value={item.Id}>
                              {item.Name}
                            </option>
                          ))}
                        </select>
                        {errors.IdProfession && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.IdProfession.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="FirstName"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Primer Nombre
                        </label>
                        <input
                          type="text"
                          id="FirstName"
                          className={classNames("form-control", {
                            invalid: errors.FirstName,
                          })}
                          placeholder="Primer Nombre"
                          disabled
                          {...register("FirstName", {
                            required: "Este campos es obligatorio",
                          })}
                        />
                        {errors.FirstName && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.FirstName.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="SecondName"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Segundo Nombre
                        </label>
                        <input
                          type="text"
                          id="SecondName"
                          className={classNames("form-control", {
                            invalid: errors.SecondName,
                          })}
                          placeholder="Segundo Nombre"
                          disabled
                          {...register("SecondName", {
                            required: false,
                          })}
                        />
                        {errors.SecondName && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.SecondName.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="Surname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Primer Apellido
                        </label>
                        <input
                          type="text"
                          id="Surname"
                          className={classNames("form-control", {
                            invalid: errors.Surname,
                          })}
                          placeholder="Primer Apellido"
                          disabled
                          {...register("Surname", {
                            required: "Este campos es obligatorio",
                          })}
                        />
                        {errors.Surname && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.Surname.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="SecondSurname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Segundo Apellido
                        </label>
                        <input
                          type="text"
                          id="SecondSurname"
                          className={classNames("form-control", {
                            invalid: errors.SecondSurname,
                          })}
                          placeholder="Segundo Apellido"
                          disabled
                          {...register("SecondSurname", {
                            required: false,
                          })}
                        />
                        {errors.SecondSurname && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.SecondSurname.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="IdCountry"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          País
                        </label>
                        <select
                          id="IdCountry"
                          className={classNames("form-control", {
                            invalid: errors.IdCountry,
                          })}
                          disabled={status === "loading"}
                          {...register("IdCountry", {
                            required: false,
                            onChange: handleCountryChange,
                          })}
                        >
                          <option value="">Seleccione...</option>
                          {countries.map((country) => (
                            <option key={country.Id} value={country.Id}>
                              {country.Name}
                            </option>
                          ))}
                        </select>
                        {errors.IdCountry && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.IdCountry.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="IdDepartment"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Departamento
                        </label>
                        <select
                          id="IdDepartment"
                          disabled={!selectedCountry || status === "loading"}
                          className={classNames("form-control", {
                            invalid: errors.IdDepartment,
                          })}
                          {...register("IdDepartment", {
                            required: false,
                            onChange: handleDepartmentChange,
                          })}
                        >
                          <option value="">Seleccione...</option>
                          {selectedCountry?.Departaments.map((department) => (
                            <option key={department.Id} value={department.Id}>
                              {department.Name}
                            </option>
                          ))}
                        </select>
                        {errors.IdDepartment && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.IdDepartment.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="IdCity"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Ciudad
                        </label>
                        <select
                          id="IdCity"
                          className={classNames("form-control", {
                            invalid: errors.IdCity,
                          })}
                          disabled={!selectedDepartment || status === "loading"}
                          {...register("IdCity", {
                            required: false,
                            onChange: handleCityChange,
                          })}
                        >
                          <option value="">Seleccione...</option>
                          {selectedDepartment &&
                            selectedDepartment.Cities.map((city) => (
                              <option key={city.Id} value={city.Id}>
                                {city.Name}
                              </option>
                            ))}
                        </select>
                        {errors.IdCity && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.IdCity.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="Email"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Correo electrónico
                        </label>
                        <input
                          type="email"
                          id="Email"
                          className={classNames("form-control", {
                            invalid: errors.Email,
                          })}
                          placeholder="name@example.com"
                          disabled={status === "loading"}
                          {...register("Email", {
                            required: "Este campos es obligatorio",
                            pattern: {
                              value:
                                /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i,
                              message: "Ingresa un correo electrónico válido",
                            },
                          })}
                        />
                        {errors.Email && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.Email.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="Phone"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Teléfono
                        </label>
                        <input
                          type="tel"
                          id="Phone"
                          className={classNames("form-control", {
                            invalid: errors.Phone,
                          })}
                          placeholder="000-000-0000"
                          disabled={status === "loading"}
                          {...register("Phone", {
                            required: "Este campos es obligatorio",
                            pattern: {
                              value: /^3[0-9]{2}[-\s]?[0-9]{3}[-\s]?[0-9]{4}$/,
                              message: "Ingresa un número de teléfono válido",
                            },
                            maxLength: 10,
                            minLength: 10,
                          })}
                        />
                        {errors.Phone && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.Phone.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="IdGender"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Genero
                        </label>
                        <select
                          id="IdGender"
                          className={classNames("form-control", {
                            invalid: errors.IdGender,
                          })}
                          disabled={status === "loading"}
                          {...register("IdGender", {
                            required: false,
                          })}
                        >
                          <option value="">Seleccione...</option>
                          {gender.map((item) => (
                            <option key={item.Id} value={item.Id}>
                              {item.Name}
                            </option>
                          ))}
                        </select>
                        {errors.IdGender && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.IdGender.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-6 mb-5">
                        <label
                          htmlFor="Birthdate"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Fecha cumpleaños
                        </label>
                        <input
                          type="date"
                          id="Birthdate"
                          className={classNames("form-control", {
                            invalid: errors.Birthdate,
                          })}
                          disabled={status === "loading"}
                          {...register("Birthdate", {
                            required: "Este campos es obligatorio",
                          })}
                        />
                        {errors.Birthdate && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.Birthdate.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-12 mb-5">
                        <label
                          htmlFor="Address"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Direccion
                        </label>
                        <input
                          type="tel"
                          id="Address"
                          className={classNames("form-control", {
                            invalid: errors.Address,
                          })}
                          placeholder="Direccion"
                          disabled={status === "loading"}
                          {...register("Address", {
                            required: false,
                          })}
                        />
                        {errors.Address && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.Address.message}
                          </span>
                        )}
                      </div>
                      <div className="col-12 col-md-12 mb-5">
                        <label
                          htmlFor="Biography"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Biografia
                        </label>
                        <textarea
                          id="Biography"
                          className={classNames("form-control", {
                            invalid: errors.Biography,
                          })}
                          placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"
                          disabled={status === "loading"}
                          {...register("Biography", {
                            required: false,
                            maxLength: 250,
                            minLength: 30,
                          })}
                        />
                        {errors.Biography && (
                          <span role="alert" className="text-red-500 text-xs">
                            {errors.Biography.message}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-12 col-md-auto">
                        <button
                          className="btn btn-primary-green w-full md:w-auto"
                          type="submit"
                        >
                          Guardar cambios
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      <Modal isOpen={isModalOpen} onClose={handleToggleModal} size="max-w-md">
        <div className="py-8 px-4 lg:px-6">
          <button
            onClick={handleToggleModal}
            className="w-8 h-8 absolute top-0 right-0 flex justify-center items-center text-zinc-700 opacity-80 text-xl"
          >
            <FontAwesomeIcon icon={faTimes} className="text-primary-green" />
          </button>

          <h3 className="text-3xl font-semibold text-center mb-5">
            Perfil Actualizado
          </h3>

          <p className="text-lg text-center text-zinc-500 mb-5">
            Los datos modificados fueron actualizados correctamente
          </p>

          <FontAwesomeIcon
            icon={faCircleCheck}
            className="text-center mx-auto text-8xl text-primary-green block my-10"
          />

          <div className="row">
            <div className="col-auto mx-auto">
              <button
                type="button"
                className="btn btn-primary-green mb-5"
                onClick={handleToggleModal}
              >
                Continuar
              </button>
            </div>
          </div>
        </div>
      </Modal>

      <Footer />
    </>
  );
}
