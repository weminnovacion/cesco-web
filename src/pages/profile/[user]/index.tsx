//React
import { useContext, useEffect, useState } from "react";

//Next
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { parseCookies } from "nookies";

//Context
import { AuthContext } from "@/contexts/AuthContext";

//Models
import { RequestStatus } from "@/model/request-status";

//components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faBook,
  faCalendar,
  faDownload,
  faFilm,
  faUserAlt,
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { Tabs } from "@/components/Tabs";
import { Tab } from "@/components/Tab";
import { useInitials } from "@/hooks/useInitials";
import { format } from "date-fns";
import { API_URL } from "../../../../constants";

export default function User() {
  const [status, setStatus] = useState<RequestStatus>("init");
  const [courses, setCourses] = useState<MyCourse[]>([]);

  const { isLoggedIn, userInfo } = useContext(AuthContext);
  const initials = useInitials(userInfo?.Username ? userInfo?.Username : "");

  const router = useRouter();
  const { user } = router.query;

  const getCourses = async (token: any): Promise<any> => {
    try {
      const config = {
        headers: {
          Authorization: "Bearer " + token,
        },
      };
      const response = await axios.get(API_URL + "cours/myCourse", config);
      let coursesData = response.data.Data;
      return coursesData;
    } catch (error) {}
  };

  const fetchData = async () => {
    const cookies = parseCookies();
    const token = cookies.token;
    try {
      setStatus("loading");
      const responses = await Promise.all([getCourses(token)]);
      if (responses) {
        setStatus("success");
        const [responseCourses] = responses;
        setCourses(responseCourses);
      }
    } catch (error) {
      setStatus("error");
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <Head>
        <title>@{userInfo?.Username} | CESCO</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />

        <main
          className="w-full h-full bg-zinc-100 flex-grow"
          id="main-content"
          role="main"
        >
          <div className="bg-zinc-700 w-full relative min-h-[12.5rem] text-white mb-8 pt-8">
            <div className="container">
              <div className="row">
                <div className="col-auto">
                  <div className="w-32 h-32 bg-zinc-100 border border-zinc-300 rounded-full overflow-hidden text-zinc-500 flex justify-center items-center">
                    {userInfo?.Photo && (
                      <img
                        className="w-full h-full bg-cover object-cover bg-center"
                        src={userInfo?.Photo}
                        alt={userInfo?.Username}
                      />
                    )}
                    {!userInfo?.Photo && (
                      <span className="text-5xl font-semibold">{initials}</span>
                    )}
                  </div>
                </div>
                <div className="col">
                  <h1 className="text-3xl font-bold">
                    {userInfo?.FirstName} {userInfo?.Surname}{" "}
                  </h1>
                  <p className="mb-1">{userInfo?.Profession?.Name}</p>
                  <p className="mb-1">
                    {userInfo?.City?.Name}, {userInfo?.Department?.Name},{" "}
                    {userInfo?.Country?.Name}
                  </p>
                  <div className="row">
                    <div className="col-auto">
                      <Link
                        href={`/profile/${userInfo?.Username}/edit`}
                        className="btn btn-primary-green btn-small shrink-0"
                        type="button"
                      >
                        Editar Perfil
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container ">
            <div className="row mb-10">
              <div className="col-12">
                <Tabs>
                  <Tab title="Actividad">
                    <div className="row">
                      <div className="col-12 mb-5">
                        <h1 className="text-2xl font-bold">Actividad</h1>
                      </div>
                      <div className="col-12">
                        <div className="my-16">
                          <div className="text-center shadow shadow-zinc-200 bg-white p-8 mb-8">
                            <h3 className="my-3 font-bold text-lg">
                              Aun no tienes Actividad en CESCO
                            </h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Tab>
                  <Tab title="Mis cursos">
                    <div className="row">
                      <div className="col-12 mb-5">
                        <h1 className="text-2xl font-bold">
                          Tus cursos y certificados
                        </h1>
                      </div>
                      {status === "loading" && (
                        <div
                          role="status"
                          className="space-y-8 animate-pulse md:space-y-0 md:space-x-8 md:flex md:items-center mb-5"
                        >
                          <div className="flex items-center justify-center w-full h-48 bg-gray-300 rounded sm:w-96">
                            <svg
                              className="w-12 h-12 text-gray-200"
                              xmlns="http://www.w3.org/2000/svg"
                              aria-hidden="true"
                              fill="currentColor"
                              viewBox="0 0 640 512"
                            >
                              <path d="M480 80C480 35.82 515.8 0 560 0C604.2 0 640 35.82 640 80C640 124.2 604.2 160 560 160C515.8 160 480 124.2 480 80zM0 456.1C0 445.6 2.964 435.3 8.551 426.4L225.3 81.01C231.9 70.42 243.5 64 256 64C268.5 64 280.1 70.42 286.8 81.01L412.7 281.7L460.9 202.7C464.1 196.1 472.2 192 480 192C487.8 192 495 196.1 499.1 202.7L631.1 419.1C636.9 428.6 640 439.7 640 450.9C640 484.6 612.6 512 578.9 512H55.91C25.03 512 .0006 486.1 .0006 456.1L0 456.1z" />
                            </svg>
                          </div>
                          <div className="w-full">
                            <div className="h-2.5 bg-gray-200 rounded-full w-48 mb-4" />
                            <div className="h-2 bg-gray-200 rounded-full max-w-[480px] mb-2.5" />
                            <div className="h-2 bg-gray-200 rounded-full mb-2.5" />
                            <div className="h-2 bg-gray-200 rounded-full max-w-[440px] mb-2.5" />
                            <div className="h-2 bg-gray-200 rounded-full max-w-[460px] mb-2.5" />
                            <div className="h-2 bg-gray-200 rounded-full max-w-[360px]" />
                          </div>
                          <span className="sr-only">Loading...</span>
                        </div>
                      )}
                      {status === "success" &&
                        courses?.length > 0 &&
                        courses.map((course) => (
                          <div className="col-12" key={course.IdCourse}>
                            <div className="bg-white mb-5 p-5">
                              <div className="row">
                                <div className="col-4">
                                  <div className="bg-zinc-200 w-full h-52 relative overflow-hidden">
                                    <img
                                      src={course.ImgProfile}
                                      alt={course.NameCourse}
                                      className="w-full h-full bg-cover object-cover"
                                    />
                                  </div>
                                </div>
                                <div className="col">
                                  <div className="row">
                                    <div className="col-12 mb-5">
                                      <h1 className="text-xl font-bold">
                                        {course.NameCourse}
                                      </h1>
                                      <p className="text-sm">
                                        Un curso de{" "}
                                        <strong>{course.Professor}</strong>
                                      </p>
                                      <br />
                                      <ul>
                                        <li className="flex items-center gap-2 mb-3 text-xs">
                                          <FontAwesomeIcon
                                            icon={faUserAlt}
                                            className="text-primary-green"
                                          />
                                          <span>
                                            {course?.TotalStudent} estudiantes
                                          </span>
                                        </li>
                                        <li className="flex items-center gap-2 mb-3 text-xs">
                                          <FontAwesomeIcon
                                            icon={faFilm}
                                            className="text-primary-green"
                                          />
                                          <span>
                                            {course?.TotalModules} Modulos (
                                            {course.HoursContent} Hr)
                                          </span>
                                        </li>
                                        <li className="flex items-center gap-2 mb-3 text-xs">
                                          <FontAwesomeIcon
                                            icon={faCalendar}
                                            className="text-primary-green"
                                          />
                                          <span>
                                            Fecha Certificacion:{" "}
                                            {format(
                                              new Date(course.DateCertificate),
                                              "yyyy-MM-dd"
                                            )}
                                          </span>
                                        </li>
                                      </ul>
                                    </div>
                                    <div className="col-12">
                                      <div className="row">
                                        <div className="col-auto">
                                          <Link
                                            href={`/courses/${
                                              course.IdCourse
                                            }-${course.NameCourse?.toLowerCase().replace(
                                              /\s+/g,
                                              "-"
                                            )}/course`}
                                            className="btn btn-primary-green btn-small flex items-center"
                                            type="button"
                                          >
                                            <span>Ver curso</span>
                                            <div className="w-4 h-4 flex justify-center items-center ms-2">
                                              <FontAwesomeIcon
                                                icon={faArrowRight}
                                              />
                                            </div>
                                          </Link>
                                        </div>
                                        {!course.Certificate &&
                                          course.ActiveEvaluation && (
                                            <div className="col-auto">
                                              <Link
                                                href={`/courses/exam/${course.IdCourse}`}
                                                className="btn btn-primary-green btn-small flex items-center"
                                                type="button"
                                              >
                                                <span>Realizar evaluación</span>
                                                <div className="w-4 h-4 flex justify-center items-center ms-2">
                                                  <FontAwesomeIcon
                                                    icon={faBook}
                                                  />
                                                </div>
                                              </Link>
                                            </div>
                                          )}
                                        {course.Certificate ? (
                                          <div className="col-auto">
                                            <a
                                              href={course.UrlCertificate}
                                              className="btn btn-primary-green btn-small"
                                              role="button"
                                              target="_blank"
                                            >
                                              <span>Certificado</span>
                                              <div className="w-4 h-4 flex justify-center items-center ms-2">
                                                <FontAwesomeIcon
                                                  icon={faDownload}
                                                />
                                              </div>
                                            </a>
                                          </div>
                                        ) : (
                                          <div className="col-auto">
                                            <a
                                              href={course.UrlConstancy}
                                              className="btn btn-primary-green btn-small"
                                              role="button"
                                              target="_blank"
                                            >
                                              <span>Constancia</span>
                                              <div className="w-4 h-4 flex justify-center items-center ms-2">
                                                <FontAwesomeIcon
                                                  icon={faDownload}
                                                />
                                              </div>
                                            </a>
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      {(status === "error" || status === "success") &&
                        !courses?.length && (
                          <div className="my-16">
                            <div className="text-center shadow shadow-zinc-200 bg-white p-8 mb-8">
                              <h3 className="my-3 font-bold text-lg">
                                Aún no has realizado ningún curso en CESCO.
                              </h3>
                              <Link href="/courses" className="underline">
                                Comienza a aprender{" "}
                                <FontAwesomeIcon
                                  className="ml-2"
                                  icon={faArrowRight}
                                />
                              </Link>
                            </div>
                          </div>
                        )}
                    </div>
                  </Tab>
                </Tabs>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    </>
  );
}
