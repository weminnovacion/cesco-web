//React
import { useContext, useState } from "react";
//Next
import Head from "next/head";
import Link from "next/link";

//components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { Breadcrumbs } from "@/components/Breadcrumbs";
import { Modal } from "@/components/Modal";

//Context
import { AuthContext } from "@/contexts/AuthContext";

//Models
import { RequestStatus } from "@/model/request-status";

//Externals
import { useForm } from "react-hook-form";
import classNames from "classnames";
import { API_URL } from "../../../../../constants";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircleCheck,
  faClose,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import { parseCookies } from "nookies";

type FormData = {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
};

export default function Settings() {
  const { userInfo } = useContext(AuthContext);

  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
    reset,
  } = useForm<FormData>();
  const [status, setStatus] = useState<RequestStatus>("init");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isError, setIsError] = useState(false);
  const [isErrorMessage, setIsErrorMessage] = useState(null);

  const onSubmit = (data: FormData) => {
    // Realizar la lógica para cambiar la contraseña aquí
    const { currentPassword, newPassword } = data;
    updatePassword(currentPassword, newPassword);
  };

  const validateConfirmPassword = (value: string) => {
    const { newPassword } = getValues();
    return value === newPassword || "Las contraseñas no coinciden.";
  };

  const updatePassword = async (
    currentPassword: string,
    newPassword: string
  ) => {
    const cookies = parseCookies();
    const token = cookies.token;

    try {
      const config = {
        headers: {
          Authorization: token,
        },
      };
      setStatus("loading");
      const response = await axios.post(
        API_URL + "user/updatePassword",
        {
          CurrentPass: currentPassword,
          NewPass: newPassword,
        },
        config
      );

      if (response.data.Status == 200) {
        setIsError(false);
        setStatus("success");
        reset();
        handleToggleModal();
      } else {
        setIsError(true);
        setIsErrorMessage(response.data.Message);
        setStatus("error");
        handleToggleModal();
        setTimeout(() => {
          setStatus("init");
        }, 3000);
      }
    } catch (error) {
      setStatus("error");
    }
  };

  const handleToggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const breadcrumbs = [
    { name: "Mi perfil", url: "/profile/" + userInfo?.Username },
    {
      name: "Configuración de cuenta",
      url: "/profile/" + userInfo?.Username + "/settings",
    },
  ];

  return (
    <>
      <Head>
        <title>Configuración de cuenta | CESCO</title>
      </Head>

      <Header />
      <main className="w-full bg-zinc-100" id="main-content" role="main">
        <div className="w-full border-b mb-8 py-4">
          <div className="container">
            <Breadcrumbs items={breadcrumbs} />
          </div>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-3 md:block hidden">
              <nav role="navigation mb-12">
                <ul className="flex flex-wrap pl-0 list-none flex-col mb-4">
                  <li className="relative mb-2">
                    <Link
                      href={`/profile/${userInfo?.Username}/edit`}
                      className="inline-block text-sm text-black hover:text-primary-green transition-all duration-300"
                    >
                      Editar perfil
                    </Link>
                  </li>
                  <li className="relative mb-2">
                    <Link
                      href={`/profile/${userInfo?.Username}/settings`}
                      className="inline-block text-sm text-black hover:text-primary-green transition-all duration-300"
                    >
                      Configuración de cuenta
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
            <div className="col-12 col-md-9">
              <div className="w-full bg-white border-b border-zinc-300 mb-4 p-8">
                <form className="row" onSubmit={handleSubmit(onSubmit)}>
                  <div className="col-12 mb-8">
                    <h3 className="text-lg font-bold text-black">CONTRASEÑA</h3>
                    <p className="text-zinc-500">
                      Cambia tu contraseña. Recomendamos usar una contraseña
                      segura que no uses en ningún otro sitio.
                    </p>
                  </div>
                  <div className="col-12">
                    <div className="relative mb-4">
                      <label htmlFor="password">Contraseña actual</label>
                      <input
                        type="password"
                        id="password"
                        className={classNames("form-control", {
                          invalid: errors.currentPassword,
                        })}
                        disabled={status === "loading"}
                        placeholder="Contraseña"
                        {...register("currentPassword", {
                          required: "Este campos es obligatorio",
                        })}
                      />
                      <span className="text-sm text-zinc-500">
                        <small>
                          Por seguridad, introduce tu contraseña actual
                        </small>
                      </span>
                      <br />
                      {errors.currentPassword && (
                        <span role="alert" className="text-red-500 text-xs">
                          {errors.currentPassword.message}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="col-12 ">
                    <div className="relative mb-4">
                      <label htmlFor="password">Nueva contraseña</label>
                      <input
                        type="password"
                        id="password"
                        className={classNames("form-control", {
                          invalid: errors.newPassword,
                        })}
                        disabled={status === "loading"}
                        placeholder="Contraseña"
                        {...register("newPassword", {
                          required: "Este campos es obligatorio",
                        })}
                      />
                      {errors.newPassword && (
                        <span role="alert" className="text-red-500 text-xs">
                          {errors.newPassword.message}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="relative mb-4">
                      <label htmlFor="password">Repetir contraseña</label>
                      <input
                        type="password"
                        id="password"
                        className={classNames("form-control", {
                          invalid: errors.confirmPassword,
                        })}
                        disabled={status === "loading"}
                        placeholder="Contraseña"
                        {...register("confirmPassword", {
                          required: "Este campos es obligatorio",
                          validate: validateConfirmPassword,
                        })}
                      />
                      {errors.confirmPassword && (
                        <span role="alert" className="text-red-500 text-xs">
                          {errors.confirmPassword.message}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="col-12 mb-4">
                    <div className="flex ">
                      <Link
                        href="/auth/password/edit"
                        role="button"
                        className="text-sm hover:text-secondary transition-all duration-300"
                      >
                        ¿Olvidaste tu contraseña?
                      </Link>
                    </div>
                  </div>
                  <div className="col-auto">
                    <button
                      type="submit"
                      className="btn btn-primary-green mb-5"
                    >
                      {status === "loading" ? (
                        <span>Loading...</span>
                      ) : (
                        <span>Actualizar contraseña</span>
                      )}
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Modal isOpen={isModalOpen} onClose={handleToggleModal} size="max-w-md">
        <div className="py-8 px-4 lg:px-6">
          <button
            onClick={handleToggleModal}
            className="w-8 h-8 absolute top-0 right-0 flex justify-center items-center text-zinc-700 opacity-80 text-xl"
          >
            <FontAwesomeIcon icon={faTimes} className="text-primary-green" />
          </button>

          <h3 className="text-3xl font-semibold text-center text-primary-green mb-5">
            {!isError
              ? "Actualizacion exitosa"
              : "Ocurrio en error al actualizar"}
          </h3>

          <p className="text-lg text-center text-zinc-500 mb-5">
            {!isError
              ? "Has cambiado satisfactoriamente tu contraseña"
              : isErrorMessage}
          </p>

          <FontAwesomeIcon
            icon={!isError ? faCircleCheck : faClose}
            className={`text-center mx-auto text-8xl block my-10 ${
              !isError ? "text-primary-green" : "text-primary-red"
            }`}
          />

          <div className="row">
            <div className="col-auto mx-auto">
              <button
                type="button"
                className={`btn btn-primary-green mb-5`}
                onClick={handleToggleModal}
              >
                Continuar
              </button>
            </div>
          </div>
        </div>
      </Modal>
      <Footer />
    </>
  );
}
