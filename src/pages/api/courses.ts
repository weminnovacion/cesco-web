import { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';
import dotenv from 'dotenv';
import { API_URL } from '../../../constants';

dotenv.config();

export function getCourses() {
  const response = axios.get(`${API_URL}cours/page`);
  return response;
}

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== 'GET') {
    res.setHeader('Allow', ['GET']);
    res.status(405).json({ message: `Method ${req.method} is not allowed` });
  } else {
    const courses = getCourses();
    res.status(200).json(courses);
  }
}