import { NextApiRequest, NextApiResponse } from "next";

export const signInHandler = (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== "POST") {
    res.status(405).json({ message: "Método no permitido" });
    return;
  }

  const { email, password } = req.body;

  // Realiza la lógica de autenticación, por ejemplo, verifica las credenciales con una base de datos
  if (email === "usuario@example.com" && password === "contraseña") {
    res.status(200).json({ message: "Inicio de sesión exitoso" });
  } else {
    res.status(401).json({ message: "Credenciales inválidas" });
  }
};