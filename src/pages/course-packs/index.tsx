//React
import { useContext, useEffect, useState } from "react";

//Next
import Head from "next/head";

//components
import { Header } from "@/components/Header";
import { Course } from "@/components/course";
import { Footer } from "@/components/Footer";

//Context
import { AuthContext } from "@/contexts/AuthContext";

//models
import { CourseProps } from "@/model/courseProps.model";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTimes } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { API_URL } from "../../../constants";

export default function CoursePacks() {
  const [courses, setCourses] = useState<CourseProps[]>([]);

  const { isLoggedIn } = useContext(AuthContext);

  const getCourses = async () => {
    try {
      const response = await axios.get(API_URL + "cours/page");
      let coursesData = response.data.Data.Content;
      setCourses(coursesData);
    } catch (error) {}
  };

  useEffect(() => {
    getCourses();
  }, []);

  return (
    <>
      <Head>
        <title>Crea tu combo - CESCO</title>
      </Head>

      <Header />

      <main className="w-full" id="main-content" role="main">
        <div className="bg-white w-full shadow p-5">
          <div className="container">
            <div className="flex flex-wrap -mx-3">
              <div className="flex-shrink-0 flex items-center w-auto px-3">
                <div className="flex flex-wrap -mx-3">
                  <div className="flex-shrink-0 w-auto px-3">
                    <div className="relative w-16 h-16 flex justify-center items-center border border-zinc-300 rounded-sm font-medium text-base text-zinc-400">
                      <button className="absolute top-0 right-0 w-6 h-6 flex justify-center items-center text-white text-base">
                        <FontAwesomeIcon icon={faTimes} />
                      </button>
                      <img
                        className="bg-cover h-full"
                        src={
                          "https://cdn.domestika.org/c_fill,dpr_1.0,f_auto,h_258,pg_1,t_base_params,w_459/v1585071413/course-covers/000/001/218/1218-original.jpg?1585071413, https://cdn.domestika.org/c_fill,dpr_2.0,f_auto,h_258,pg_1,t_base_params,w_459/v1585071413/course-covers/000/001/218/1218-original.jpg?1585071413 2x"
                        }
                        alt={"curso"}
                      />
                    </div>
                  </div>
                  <div className="flex-shrink-0 w-auto px-3">
                    <div className="w-16 h-16 flex justify-center items-center border border-zinc-300 rounded-sm font-medium text-base text-zinc-400">
                      2
                    </div>
                  </div>
                  <div className="flex-shrink-0 w-auto px-3">
                    <div className="w-16 h-16 flex justify-center items-center border border-zinc-300 rounded-sm font-medium text-base text-zinc-400">
                      3
                    </div>
                  </div>
                  <div className="flex-shrink-0 flex items-end w-auto px-3 ">
                    <span className="text-sm">(1 de 3)</span>
                  </div>
                </div>
              </div>
              <div className="flex-shrink-0 w-auto ml-auto px-3">
                <h3 className="text-lg font-bold mb-4">
                  $87.000 COP{" "}
                  <span className="text-secondary line-through text-sm">
                    $233.700 COP
                  </span>
                  <span className="text-secondary text-sm"> 63% Dto.*</span>
                </h3>
                <button
                  className="btn btn-primary-green btn-lg w-full flex items-center"
                  disabled
                >
                  <span className="mx-auto">Combo de 3 cursos</span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="flex w-full h-full">
          <div className="bg-zinc-100 flex-grow w-full h-full py-10">
            <div className="container">
              <div className="flex flex-wrap -mx-3 ">
                {courses.map((course, index) => (
                  <div
                    className="w-full md:w-1/2 lg:w-1/4 px-3 mb-4"
                    key={index}
                  >
                    <Course {...course}>
                      <button className="btn btn-primary-green w-full flex items-center">
                        <FontAwesomeIcon icon={faPlus} />
                        <span className="mx-auto">Añadir al combo</span>
                      </button>
                    </Course>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </main>

      <Footer />
    </>
  );
}
