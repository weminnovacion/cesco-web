//Next
import Head from "next/head";

//Components
import { Header } from "@/components/Header";

//Externals
import axios from "axios";
import classNames from "classnames";
import { useForm } from "react-hook-form";
import { API_URL } from "../../../../../constants";
import { RequestStatus } from "@/model/request-status";
import { useState } from "react";

type EditPasswordFormInputs = {
  email: string;
};

export default function EditPassword() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
  } = useForm<EditPasswordFormInputs>();
  const [status, setStatus] = useState<RequestStatus>("init");

  const onSubmit = async (data: EditPasswordFormInputs) => {
    try {
      setStatus("loading");
      const response = await axios.post(
        API_URL + "oauth/generateCode?email=" + data.email,
        data
      );
      if (response.status == 200) {
        if (response.data?.Status == 200) {
          setStatus("success");
          setError("email", {
            message: response.data.Data,
          });
        } else {
          setStatus("error");
          setError("email", {
            message: "Se envio un correo electronico con los pasos a seguir",
          });
          setTimeout(() => {
            setStatus("init");
          }, 10000);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <Head>
        <title>Restablecer contraseña | CESCO</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />

        <section className="bg-primary-green/5 flex-grow">
          <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto lg:py-24">
            <h3 className="flex items-center mb-6 text-4xl font-bold text-primary-green ">
              CESCO
            </h3>
            <div className="w-full p-6 bg-white border border-primary-green/25 rounded shadow dark:border md:mt-0 sm:max-w-md dark:bg-gray-800 dark:border-gray-700 sm:p-8">
              <h1 className="mb-1 text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                ¿Olvidaste tu contraseña?
              </h1>
              <p className="font-light text-sm text-gray-500 dark:text-gray-400">
                ¡No te preocupes! Simplemente escriba su correo electrónico y le
                enviaremos un código para restablecer ¡tu contraseña!
              </p>
              <form
                onSubmit={handleSubmit(onSubmit)}
                className="mt-4 space-y-4 lg:mt-5 md:space-y-5"
              >
                <div>
                  <label
                    htmlFor="email"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Correo electrónico
                  </label>
                  <input
                    type="email"
                    id="email"
                    className={classNames("form-control", {
                      invalid: errors.email,
                    })}
                    placeholder="name@example.com"
                    disabled={status === "loading"}
                    {...register("email", {
                      required: "Este campos es obligatorio",
                      pattern: {
                        value:
                          /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i,
                        message: "Ingresa un correo electrónico válido",
                      },
                    })}
                  />
                  {errors.email && status === "error" && (
                    <span role="alert" className={`text-red-500 text-xs`}>
                      {errors.email.message}
                    </span>
                  )}
                  {errors.email && status === "success" && (
                    <span role="alert" className={`text-grem-500 text-xs`}>
                      {errors.email.message}
                    </span>
                  )}
                </div>
                <button type="submit" className="btn btn-primary-green w-full">
                  Restablecer la contraseña
                </button>
              </form>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
