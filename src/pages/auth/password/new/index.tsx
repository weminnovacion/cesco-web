//Next
import Head from "next/head";
//Components
import { Header } from "@/components/Header";

export default function NewPasssword() {
  return (
    <>
      <Head>
        <title>Nueva contraseña | CESCO</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />

        <section className="bg-primary-green/5 flex-grow">
          <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto lg:py-24">
            <h3 className="flex items-center mb-6 text-4xl font-bold text-primary-green ">
              CESCO
            </h3>
            <div className="w-full p-6 bg-white border border-primary-green/25 rounded shadow dark:border md:mt-0 sm:max-w-md dark:bg-gray-800 dark:border-gray-700 sm:p-8">
              <h2 className="mb-1 text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                Cambiar la contraseña
              </h2>
              <form className="mt-4 space-y-4 lg:mt-5 md:space-y-5" action="#">
                <div>
                  <label
                    htmlFor="password"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Nueva contraseña
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    placeholder="••••••••"
                    className="form-control"
                    required
                  />
                </div>
                <div>
                  <label
                    htmlFor="confirm-password"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Confirmar Contraseña
                  </label>
                  <input
                    type="confirm-password"
                    name="confirm-password"
                    id="confirm-password"
                    placeholder="••••••••"
                    className="form-control"
                    required
                  />
                </div>
                <button type="submit" className="btn btn-primary-green w-full">
                  Restablecer la contraseña
                </button>
              </form>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
