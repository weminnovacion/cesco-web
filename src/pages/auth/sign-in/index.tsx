//React
import { useContext, useEffect } from "react";

//Next
import { useRouter } from "next/router";
import Head from "next/head";

//Components
import { Header } from "@/components/Header";

//Contexts
import { AuthContext } from "@/contexts/AuthContext";
import { RequestStatus } from "@/model/request-status";
import Link from "next/link";
import { SignInForm } from "@/components/sign-in-form";

type SignInFormProps = {
  handleStateForm: (status: RequestStatus, message?: string) => void;
};

export default function SignIn() {
  const { isLoggedIn } = useContext(AuthContext);
  const router = useRouter();

  const handleSubmit: SignInFormProps["handleStateForm"] = (status) => {
    if (status === "success") {
      router.push("/courses");
    } else if (status === "error") {
    }
  };

  useEffect(() => {
    if (isLoggedIn) {
      router.push("/");
    }
  }, []);

  return (
    <>
      <Head>
        <title>Iniciar sesión | CESCO</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />
        <section className="bg-primary-green/5 flex-grow">
          <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:py-24">
            <h3 className="flex items-center mb-6 text-4xl font-bold text-primary-green ">
              CESCO
            </h3>
            <div className="w-full bg-white border border-primary-green/25 rounded shadow  md:mt-0 sm:max-w-md xl:p-0 ">
              <div className="p-6 sm:p-8">
                <h1 className="text-xl font-bold leading-tight tracking-tight text-zinc-700 md:text-2xl mb-5">
                  Iniciar sesión con tu cuenta
                </h1>

                <SignInForm handleStateForm={handleSubmit} />
                <div className="row">
                  <div className="col-12">
                    <p className="text-sm font-light text-zinc-500 dark:text-zinc-400">
                      ¿Aún no tienes una cuenta?{" "}
                      <Link
                        href="/auth/sign-up"
                        className="font-medium text-primary-green hover:underline "
                      >
                        registrarse
                      </Link>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
