import { useContext, useEffect } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import { Header } from "@/components/Header";
import { SignUpForm } from "@/components/sign-up-form";
import { AuthContext } from "@/contexts/AuthContext";
import { RequestStatus } from "@/model/request-status";
import Link from "next/link";

type SignInFormProps = {
  handleStateForm: (status: RequestStatus, message?: string) => void;
};

export default function SignUp() {
  const { isLoggedIn } = useContext(AuthContext);
  const router = useRouter();

  const handleSubmit: SignInFormProps["handleStateForm"] = (status) => {
    if (status === "success") {
    } else if (status === "error") {
    }
  };

  useEffect(() => {
    if (isLoggedIn) {
      router.push("/");
    }
  }, []);

  return (
    <>
      <Head>
        <title>Registrarse | CESCO</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />
        <section className="bg-primary-green/5 flex-grow">
          <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:py-24">
            <a
              href="#"
              className="flex items-center mb-6 text-4xl font-bold text-primary-green "
            >
              CESCO
            </a>
            <div className="w-full bg-white border border-primary-green/25 rounded shadow md:mt-0 sm:max-w-3xl xl:p-0 ">
              <div className="p-6  sm:p-8">
                <h1 className="text-xl font-bold leading-tight tracking-tight text-zinc-700 md:text-2xl mb-5">
                  Crear una cuenta
                </h1>
                <SignUpForm handleStateForm={handleSubmit} />
                <div className="row">
                  <div className="col-12">
                    <p className="text-sm font-light text-zinc-500 ">
                      ¿Ya tienes una cuenta?{" "}
                      <Link
                        href="/auth/sign-in"
                        className="font-medium text-primary-green hover:underline "
                      >
                        Iniciar sesión
                      </Link>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
