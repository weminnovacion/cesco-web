//React
import { useEffect, useState } from "react";

//Next
import Head from "next/head";
import { useRouter } from "next/router";

//Models
import { RequestStatus } from "@/model/request-status";

//Components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";

//Externals
import { useForm } from "react-hook-form";
import { API_URL } from "../../../constants";
import axios from "axios";
import { format, parseISO } from "date-fns";

interface CertificateVerificationForm {
  FullName: string;
  TypeIdentification: string;
  Identification: string;
  Phone: string;
  Email: string;
  NameCourse: string;
  UrlCertificate: string;
  DateCertificate: string;
  DateExpiration: string;
}

export default function certificateVerifications() {
  const [status, setStatus] = useState<RequestStatus>("init");

  const router = useRouter();
  const { token } = router.query;

  const { register, setValue } = useForm<CertificateVerificationForm>();

  const fetchData = async () => {
    setStatus("loading");
    try {
      const response = await axios.get(
        API_URL + "assignment/validateCertificateById?token=" + token
      );
      setStatus("success");
      if (response) {
        if (response.data.Status == 200) {
          const data = response.data.Data as CertificateVerificationForm;
          setValue("FullName", data.FullName);
          setValue("TypeIdentification", data.TypeIdentification);
          setValue(
            "Identification",
            data.TypeIdentification + ", " + data.Identification
          );
          setValue("Phone", data.Phone);
          setValue(
            "DateCertificate",
            format(parseISO(data.DateCertificate), "MMM d, yyyy")
          );
          setValue(
            "DateExpiration",
            format(parseISO(data.DateExpiration), "MMM d, yyyy")
          );
          setValue("Email", data.Email);
          setValue("NameCourse", data.NameCourse);
          setValue("UrlCertificate", data.UrlCertificate);
        } else {
          setStatus("error");
        }
      }
    } catch (error) {
      setStatus("error");
    }
  };

  useEffect(() => {
    if (token) {
      fetchData();
    }
  }, [token]);

  return (
    <>
      <Head>
        <title>Verificación de certificado | CESCO</title>
      </Head>
      <div className="flex flex-col h-full">
        <Header />
        <main
          role="main"
          className="flex-grow w-full bg-zinc-100 py-10 lg:py-10"
        >
          <div className="container">
            <div className="row">
              {status === "loading" ? (
                <>
                  <div className="col-8 col-lg-8">
                    <div className="row">
                      <div className="col-12 mb-5">
                        <label
                          htmlFor="fullname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Nombre del curso
                        </label>
                        <input
                          type="text"
                          id="fullname"
                          className="form-control"
                          placeholder="Pedro Rodriguez"
                          disabled={true}
                          {...register("NameCourse")}
                        />
                      </div>
                      <div className="col-12 col-lg-6 mb-5">
                        <label
                          htmlFor="fullname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Nombre completo
                        </label>
                        <input
                          type="text"
                          id="fullname"
                          className="form-control"
                          placeholder="Pedro Rodriguez"
                          disabled={true}
                          {...register("FullName")}
                        />
                      </div>
                      <div className="col-12 col-lg-6 mb-5">
                        <label
                          htmlFor="fullname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Identificación
                        </label>
                        <input
                          type="text"
                          id="fullname"
                          className="form-control"
                          placeholder="Pedro Rodriguez"
                          disabled={true}
                          {...register("Identification")}
                        />
                      </div>
                      <div className="col-12 col-lg-6 mb-5">
                        <label
                          htmlFor="fullname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Correo electrónico
                        </label>
                        <input
                          type="text"
                          id="fullname"
                          className="form-control"
                          placeholder="Pedro Rodriguez"
                          disabled={true}
                          {...register("Email")}
                        />
                      </div>
                      <div className="col-12 col-lg-6 mb-5">
                        <label
                          htmlFor="fullname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Teléfono
                        </label>
                        <input
                          type="text"
                          id="fullname"
                          className="form-control"
                          placeholder="Pedro Rodriguez"
                          disabled={true}
                          {...register("Phone")}
                        />
                      </div>

                      <div className="col-12 col-lg-6 mb-5">
                        <label
                          htmlFor="fullname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Fecha de certificado
                        </label>
                        <input
                          type="text"
                          id="fullname"
                          className="form-control"
                          placeholder="Pedro Rodriguez"
                          disabled={true}
                          {...register("DateCertificate")}
                        />
                      </div>
                      <div className="col-12 col-lg-6 mb-5">
                        <label
                          htmlFor="fullname"
                          className="block mb-2 text-sm font-medium text-zinc-700 "
                        >
                          Fecha de expiración
                        </label>
                        <input
                          type="text"
                          id="fullname"
                          className="form-control"
                          placeholder="Pedro Rodriguez"
                          disabled={true}
                          {...register("DateExpiration")}
                        />
                      </div>
                    </div>
                  </div>
                </>
              ) : null}
            </div>
          </div>
          <div className="container">
            {status == "success" ? (
              <>
                <div className="row">
                  <div className="col-12">
                    <div className="bg-white w-full rounded p-5 mb-5">
                      <div
                        role="status"
                        className="w-full p-4 space-y-4  divide-y divide-gray-200 rounded animate-pulse "
                      >
                        <div className="flex items-center justify-between">
                          <div>
                            <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                            <div className="w-32 h-2 bg-gray-200 rounded-full" />
                          </div>
                          <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                        </div>
                        <div className="flex items-center justify-between pt-4">
                          <div>
                            <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                            <div className="w-32 h-2 bg-gray-200 rounded-full" />
                          </div>
                          <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                        </div>
                        <div className="flex items-center justify-between pt-4">
                          <div>
                            <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                            <div className="w-32 h-2 bg-gray-200 rounded-full" />
                          </div>
                          <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                        </div>
                        <div className="flex items-center justify-between pt-4">
                          <div>
                            <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                            <div className="w-32 h-2 bg-gray-200 rounded-full" />
                          </div>
                          <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                        </div>
                        <div className="flex items-center justify-between pt-4">
                          <div>
                            <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                            <div className="w-32 h-2 bg-gray-200 rounded-full" />
                          </div>
                          <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                        </div>
                        <div className="py-4">
                          <div className="flex bg-gray-100 items-center justify-between p-4">
                            <div className="h-2.5 bg-gray-300 rounded-full w-24 " />
                            <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                          </div>
                        </div>
                        <span className="sr-only">Loading...</span>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            ) : null}
          </div>
        </main>
        <Footer />
      </div>
    </>
  );
}
