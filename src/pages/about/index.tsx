//Next
import Head from "next/head";

//Components
import { Footer } from "@/components/Footer";
import { Header } from "@/components/Header";

export default function About() {
  return (
    <>
      <Head>
        <title>Acerda de | CESCO</title>
      </Head>

      <div className="flex flex-col h-full">
        <Header />
        <main className="flex-grow  w-full" role="main">
          <section className="">
            <div className="text-center py-12 lg:py-24">
              <div className="container">
                <div className="row">
                  <div className="col-12 col-lg-12 mx-auto">
                    <h1 className="text-3xl font-extrabold leading-none tracking-tight text-gray-900 dark:text-white sm:text-5xl lg:text-7xl mb-10">
                      Centro de entrenamiento en salud de colombia
                    </h1>
                    <p className="mt-4 text-base font-normal text-gray-500 dark:text-gray-400 md:max-w-3xl md:mx-auto sm:text-xl mb-5">
                      Cesco SAS es un centro de entrenamiento de alta calidad
                      para el personal de la salud, bajo los lineamientos y
                      requerimientos de la Resolución 3100 del 2019 en el
                      Estándar de Talento Humano, con el fin de fortalecer los
                      conocimientos y desarrollar las habilidades en las
                      respectivas áreas de trabajo.
                    </p>
                  </div>
                  <div className="col-12">
                    <div className="relative mx-auto border-gray-800 dark:border-gray-800 bg-gray-800 border-[8px] rounded-t-xl h-[172px] max-w-[301px] md:h-[294px] md:max-w-[512px]">
                      <div className="rounded-lg overflow-hidden h-[156px] md:h-[278px] bg-white dark:bg-gray-800">
                        <img
                          src="https://cesco.s3.amazonaws.com/uploads/public/certificado_home.png"
                          className="h-[156px] md:h-[278px] w-full rounded-xl"
                          alt=""
                        />
                      </div>
                    </div>
                    <div className="relative mx-auto bg-gray-900 dark:bg-gray-700 rounded-b-xl rounded-t-sm h-[17px] max-w-[351px] md:h-[21px] md:max-w-[597px]">
                      <div className="absolute left-1/2 top-0 -translate-x-1/2 rounded-b-xl w-[56px] h-[5px] md:w-[96px] md:h-[8px] bg-gray-800"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section className="bg-white dark:bg-gray-900 container">
            <div className="gap-8 py-8 px-4 mx-auto max-w-screen-xl lg:grid lg:grid-cols-2 xl:gap-16 sm:py-16 lg:px-6 ">
              <div className="">
                <h2 className="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">
                  ¿Por qué elegir CESCO?
                </h2>
                <p className="mb-8 font-light lg:text-xl">
                  Promovemos la innovación a través de una plataforma educativa
                  confiable, rápida y fácil de usar, con el fin de mejorar la
                  experiencia educativa y consolidar una relación a largo plazo
                  con todos nuestros alumnos y participantes.
                </p>

                <img
                  className="mb-4 w-full lg:mb-5 rounded-lg"
                  src="https://cesco.s3.amazonaws.com/uploads/public/Profile+Humanizacion+(2)-min.jpg"
                  alt="feature image"
                />
              </div>
              <div className="text-gray-500 dark:text-gray-400 sm:text-lg">
                <div className="py-8 mb-6 border-t border-b border-gray-200 dark:border-gray-700">
                  <div className="flex">
                    <div className="flex justify-center items-center mr-4 w-8 h-8 bg-purple-100 rounded-full dark:bg-purple-900 shrink-0">
                      <svg
                        className="w-5 h-5 text-purple-600 dark:text-purple-300"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M3 3a1 1 0 000 2v8a2 2 0 002 2h2.586l-1.293 1.293a1 1 0 101.414 1.414L10 15.414l2.293 2.293a1 1 0 001.414-1.414L12.414 15H15a2 2 0 002-2V5a1 1 0 100-2H3zm11.707 4.707a1 1 0 00-1.414-1.414L10 9.586 8.707 8.293a1 1 0 00-1.414 0l-2 2a1 1 0 101.414 1.414L8 10.414l1.293 1.293a1 1 0 001.414 0l4-4z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </div>
                    <div>
                      <h3 className="mb-2 text-xl font-bold text-gray-900 dark:text-white">
                        Nuestra misión
                      </h3>
                      <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                        En el 2023, ser reconocidos por nuestros clientes como
                        una empresa confiable, profesional y de alto nivel
                        educativo, mediante la generación de valor en nuestros
                        servicios de formación continua.
                      </p>
                    </div>
                  </div>
                  <div className="flex pt-8">
                    <div className="flex justify-center items-center mr-4 w-8 h-8 bg-teal-100 rounded-full dark:bg-teal-900 shrink-0">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="w-5 h-5 text-teal-600 dark;text-teal-300"
                      >
                        <path
                          fillRule="evenodd"
                          d="M9.315 7.584C12.195 3.883 16.695 1.5 21.75 1.5a.75.75 0 01.75.75c0 5.056-2.383 9.555-6.084 12.436A6.75 6.75 0 019.75 22.5a.75.75 0 01-.75-.75v-4.131A15.838 15.838 0 016.382 15H2.25a.75.75 0 01-.75-.75 6.75 6.75 0 017.815-6.666zM15 6.75a2.25 2.25 0 100 4.5 2.25 2.25 0 000-4.5z"
                          clipRule="evenodd"
                        />
                        <path d="M5.26 17.242a.75.75 0 10-.897-1.203 5.243 5.243 0 00-2.05 5.022.75.75 0 00.625.627 5.243 5.243 0 005.022-2.051.75.75 0 10-1.202-.897 3.744 3.744 0 01-3.008 1.51c0-1.23.592-2.323 1.51-3.008z" />
                      </svg>
                    </div>
                    <div>
                      <h3 className="mb-2 text-xl font-bold text-gray-900 dark:text-white">
                        Nuestra visión
                      </h3>
                      <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                        Fortalecer las competencias del personal de la salud a
                        través de la formación continua, con el fin de mejorar
                        la calidad de vida de la población en Colombia.
                      </p>
                    </div>
                  </div>
                  <div className="flex pt-8">
                    <div className="flex justify-center items-center mr-4 w-8 h-8 bg-teal-100 rounded-full dark:bg-teal-900 shrink-0">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="w-5 h-5 text-teal-600 dark;text-teal-300"
                      >
                        <path d="M11.25 5.337c0-.355-.186-.676-.401-.959a1.647 1.647 0 01-.349-1.003c0-1.036 1.007-1.875 2.25-1.875S15 2.34 15 3.375c0 .369-.128.713-.349 1.003-.215.283-.401.604-.401.959 0 .332.278.598.61.578 1.91-.114 3.79-.342 5.632-.676a.75.75 0 01.878.645 49.17 49.17 0 01.376 5.452.657.657 0 01-.66.664c-.354 0-.675-.186-.958-.401a1.647 1.647 0 00-1.003-.349c-1.035 0-1.875 1.007-1.875 2.25s.84 2.25 1.875 2.25c.369 0 .713-.128 1.003-.349.283-.215.604-.401.959-.401.31 0 .557.262.534.571a48.774 48.774 0 01-.595 4.845.75.75 0 01-.61.61c-1.82.317-3.673.533-5.555.642a.58.58 0 01-.611-.581c0-.355.186-.676.401-.959.221-.29.349-.634.349-1.003 0-1.035-1.007-1.875-2.25-1.875s-2.25.84-2.25 1.875c0 .369.128.713.349 1.003.215.283.401.604.401.959a.641.641 0 01-.658.643 49.118 49.118 0 01-4.708-.36.75.75 0 01-.645-.878c.293-1.614.504-3.257.629-4.924A.53.53 0 005.337 15c-.355 0-.676.186-.959.401-.29.221-.634.349-1.003.349-1.036 0-1.875-1.007-1.875-2.25s.84-2.25 1.875-2.25c.369 0 .713.128 1.003.349.283.215.604.401.959.401a.656.656 0 00.659-.663 47.703 47.703 0 00-.31-4.82.75.75 0 01.83-.832c1.343.155 2.703.254 4.077.294a.64.64 0 00.657-.642z" />
                      </svg>
                    </div>
                    <div>
                      <h3 className="mb-2 text-xl font-bold text-gray-900 dark:text-white">
                        Nuestra razón de ser
                      </h3>
                      <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                        Somos una organización con una identidad de marca que se
                        caracteriza por ser transparente, tener calidad humana
                        y, sobre todo, contar con un equipo de trabajo
                        comprometido en apoyar causas sociales y mejorar la
                        calidad de vida de todos los colombianos. Nuestro
                        enfoque está en la formación y capacitación de alto
                        valor para todos los profesionales del sector salud en
                        Colombia.
                      </p>
                    </div>
                  </div>
                  <div className="flex pt-8">
                    <div className="flex justify-center items-center mr-4 w-8 h-8 bg-teal-100 rounded-full dark:bg-teal-900 shrink-0">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="w-5 h-5 text-teal-600 dark;text-teal-300"
                      >
                        <path d="M11.25 5.337c0-.355-.186-.676-.401-.959a1.647 1.647 0 01-.349-1.003c0-1.036 1.007-1.875 2.25-1.875S15 2.34 15 3.375c0 .369-.128.713-.349 1.003-.215.283-.401.604-.401.959 0 .332.278.598.61.578 1.91-.114 3.79-.342 5.632-.676a.75.75 0 01.878.645 49.17 49.17 0 01.376 5.452.657.657 0 01-.66.664c-.354 0-.675-.186-.958-.401a1.647 1.647 0 00-1.003-.349c-1.035 0-1.875 1.007-1.875 2.25s.84 2.25 1.875 2.25c.369 0 .713-.128 1.003-.349.283-.215.604-.401.959-.401.31 0 .557.262.534.571a48.774 48.774 0 01-.595 4.845.75.75 0 01-.61.61c-1.82.317-3.673.533-5.555.642a.58.58 0 01-.611-.581c0-.355.186-.676.401-.959.221-.29.349-.634.349-1.003 0-1.035-1.007-1.875-2.25-1.875s-2.25.84-2.25 1.875c0 .369.128.713.349 1.003.215.283.401.604.401.959a.641.641 0 01-.658.643 49.118 49.118 0 01-4.708-.36.75.75 0 01-.645-.878c.293-1.614.504-3.257.629-4.924A.53.53 0 005.337 15c-.355 0-.676.186-.959.401-.29.221-.634.349-1.003.349-1.036 0-1.875-1.007-1.875-2.25s.84-2.25 1.875-2.25c.369 0 .713.128 1.003.349.283.215.604.401.959.401a.656.656 0 00.659-.663 47.703 47.703 0 00-.31-4.82.75.75 0 01.83-.832c1.343.155 2.703.254 4.077.294a.64.64 0 00.657-.642z" />
                      </svg>
                    </div>
                    <div>
                      <h3 className="mb-2 text-xl font-bold text-gray-900 dark:text-white">
                        Politicas de calidad
                      </h3>
                      <ul className="font-light text-gray-500 dark:text-gray-400 list-item">
                        <li>
                          <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                            Fortalecer conocimientos
                          </p>
                        </li>
                        <li>
                          <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                            Desarrollo de habilidades
                          </p>
                        </li>
                        <li>
                          <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                            Servicio al cliente
                          </p>
                        </li>
                        <li>
                          <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                            Innovacion y desarrollo
                          </p>
                        </li>
                        <li>
                          <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                            Mejora continua
                          </p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
        <Footer />
      </div>
    </>
  );
}
