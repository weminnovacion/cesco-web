//Next
import Head from "next/head";

//components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";

export default function Requests() {
  return (
    <>
      <Head>
        <title>Enviar una solicitud | CESCO</title>
      </Head>

      <Header />
      <main className="w-full" id="main-content" role="main">
        <div className="container py-10">
          <div className="flex flex-wrap -mx-3">
            <div className="w-2/3 px-3">
              <h1 className="text-3xl font-bold mb-5">
                Formulario de contacto
              </h1>
              <p className="my-4 font-bold">
                Si necesitas ayuda personalizada, ¡éste es tu lugar!
              </p>
              <p className="text-sm mb-4">
                Te haremos algunas preguntas para distribuir internamente tu
                mensaje y garantizar que terminas hablando con la persona
                adecuada.
              </p>
              <p className="text-sm mb-4">¿Qué tipo de ayuda estás buscando?</p>
              <div className="flex flex-wrap -mx-3">
                <div className="flex-shrink-0 w-auto px-3 mb-4">
                  <a role="button" className="btn btn-primary-green btn-small ">
                    Quiero información
                  </a>
                </div>
                <div className="flex-shrink-0 w-auto px-3 mb-4">
                  <a role="button" className="btn btn-primary-green btn-small ">
                    Tengo un problema
                  </a>
                </div>
                <div className="flex-shrink-0 w-auto px-3 mb-4">
                  <a role="button" className="btn btn-primary-green btn-small">
                    Quiero solicitar un cambio o reembolso
                  </a>
                </div>
              </div>
              <div className="flex flex-wrap -mx-3">
                <div className="w-full px-3">
                  <div className="relative mb-4">
                    <label htmlFor="email" className="text-sm mb-2">
                      Correo electrónico <span className="text-red-500">*</span>
                    </label>
                    <input
                      type="email"
                      id="email"
                      className="bg-white text-sm border rounded border-zinc-300 text-zinc-900 focus:ring-blue-500 focus:border-blue-500 block w-full px-3 py-2 "
                      required
                    />
                  </div>
                </div>
                <div className="w-full px-3">
                  <div className="relative mb-4">
                    <label htmlFor="email" className="text-sm mb-2">
                      ¿Sobre qué quieres más información?{" "}
                      <span className="text-red-500">*</span>
                    </label>
                    <select
                      name=""
                      id=""
                      className="bg-white text-sm border rounded border-zinc-300 text-zinc-900 focus:ring-blue-500 focus:border-blue-500 block w-full px-3 py-2 "
                      required
                    >
                      <option value="">Seleccione una opción</option>
                      <option value="">Sobre los cursos</option>
                      <option value="">Sobre el pago</option>
                      <option value="">Sobre mi perfil</option>
                    </select>
                  </div>
                </div>
                <div className="w-full px-3">
                  <div className="relative mb-4">
                    <label htmlFor="email" className="text-sm mb-2">
                      Soy... <span className="text-red-500">*</span>
                    </label>
                    <select
                      name=""
                      id=""
                      className="bg-white text-sm border rounded border-zinc-300 text-zinc-900 focus:ring-blue-500 focus:border-blue-500 block w-full px-3 py-2 "
                      required
                    >
                      <option value="">Seleccione una opción</option>
                      <option value="1">
                        Todavía no me he inscrito a ningún curso
                      </option>
                      <option value="">
                        Estudiante de uno o varios cursos
                      </option>
                      <option value=""></option>
                    </select>
                  </div>
                </div>
                <div className="w-full px-3">
                  <div className="relative mb-4">
                    <label htmlFor="email" className="text-sm mb-2">
                      Descripción<span className="text-red-500">*</span>
                    </label>
                    <textarea
                      name=""
                      id="email"
                      className="bg-white text-sm border rounded border-zinc-300 text-zinc-900 focus:ring-blue-500 focus:border-blue-500 block w-full px-3 py-2 "
                      required
                      cols={30}
                      rows={5}
                    ></textarea>
                  </div>
                </div>
                <div className="w-auto flex-shrink-0 ml-auto px-3">
                  <button type="button" className="btn btn-primary-green  ">
                    Enviar
                  </button>
                </div>
              </div>
            </div>
            <div className="w-1/3 px-3">
              <h1 className="text-3xl font-bold mb-8 pb-2">
                Preguntas frecuentes
              </h1>
              <a
                href="#"
                role="button"
                className="font-bold text-lg hover:underline hover:text-blue-600 transition-all duration-200"
              >
                ¿Cómo son los cursos de CESCO?
              </a>
              <p className="py-4">
                Comienza tu viaje creativo y aprende a tu ritmo con cursos 100%
                online
              </p>
              <a
                href="#"
                role="button"
                className="font-bold text-lg hover:underline hover:text-blue-600 transition-all duration-200"
              >
                No encuentro mi curso en mi perfil
              </a>
              <p className="py-4">
                Cuando te apuntas a un curso, es tuyo para siempre. Si no
                aparece en tu perfil, te ayudamos a encontrarlo.
              </p>
              <a
                href="#"
                role="button"
                className="font-bold text-lg hover:underline hover:text-blue-600 transition-all duration-200"
              >
                Problemas con los videos/audio de mi curso
              </a>
              <p className="py-4">
                Te mostramos paso a paso cómo resolver los problemas más
                frecuentes al reproducir el video y el audio de tus cursos.
              </p>
              <a
                href="#"
                role="button"
                className="font-bold text-lg hover:underline hover:text-blue-600 transition-all duration-200"
              >
                ¿Qué debo hacer si no puedo completar el pago?
              </a>
              <p className="py-4">
                Sigue estas instrucciones para garantizar que puedes completar
                el pago con los métodos disponibles
              </p>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </>
  );
}
