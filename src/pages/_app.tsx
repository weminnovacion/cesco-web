import type { AppProps } from "next/app";

import "@/styles/globals.scss";

import { SpeedDial } from "@/components/speedDial";
import { AuthProvider } from "@/contexts/AuthContext";
import { CartProvider } from "@/contexts/CartContext";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <CartProvider>
      <AuthProvider>
        <Component {...pageProps} />
        <SpeedDial />
      </AuthProvider>
    </CartProvider>
  );
}
