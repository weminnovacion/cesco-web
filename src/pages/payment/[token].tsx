//Next
import Head from "next/head";

//Components
import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationCircle } from "@fortawesome/free-solid-svg-icons";
import { faCircleCheck } from "@fortawesome/free-regular-svg-icons";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import axios from "axios";
import { parseCookies } from "nookies";
import { RequestStatus } from "@/model/request-status";
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";
import { format, parseISO } from "date-fns";
import { AuthContext } from "@/contexts/AuthContext";
import Link from "next/link";
import { API_URL } from "../../../constants";
import { CartContext } from "@/contexts/CartContext";

export default function Payment() {
  const [status, setStatus] = useState<RequestStatus>("init");
  const [verifyPayment, setVerifyPayment] = useState<VerifyPayment>();
  const [statusPayment, setStatusPayment] = useState();
  const { currencyWithSuffixFormatter } = useCurrencyFormatter();
  const { userInfo } = useContext(AuthContext);
  const { cleanCart } = useContext(CartContext);

  const router = useRouter();
  const { token } = router.query;

  const verificationTransaction = async (tokenBearer: any) => {
    try {
      const config = {
        headers: {
          Authorization: "Bearer " + tokenBearer,
        },
      };

      const response = await axios.get(
        API_URL + "checkout/verifyPayment?token=" + token,
        config
      );

      setStatusPayment(response.data.Status);
      return response.data.Data; 
    } catch (error: any) {
      setStatus("error");
      if (error.response.status === 500) {
        router.push("/auth/sign-in");
        alert("Debe iniciar sesión");
      }
    }
  };

  const fetchData = async () => {
    const cookies = parseCookies();
    const token = cookies.token;

    try {
      setStatus("loading");
      const responses = await Promise.all([verificationTransaction(token)]);
      if (responses) {
        setStatus("success");
        const [responseverificationTransaction] = responses;
        setVerifyPayment(responseverificationTransaction);
        cleanCart();
      }
    } catch (error) {
      setStatus("error");
    }
  };

  useEffect(() => {
    if (token) {
      fetchData();
    }
  }, [token]);

  return (
    <>
      <Head>
        <title>Pago recibido | CESCO</title>
      </Head>

      <div className="flex flex-col">
        <Header />
        <main
          className="flex-grow w-full h-full bg-zinc-100"
          id="main-content"
          role="main"
        >
          {status === "success" ? (
            <div className="container py-10">
              <div className="row">
                <div className="col-12 col-lg-6 mx-auto">
                  <div className="bg-white p-5 border rounded shadow w-full">
                    <div className="row">
                      {statusPayment == 200 && (
                        <>
                          <div className="col-auto mx-auto">
                            <FontAwesomeIcon
                              className="text-7xl text-primary-green mb-5"
                              icon={faCircleCheck}
                            />
                          </div>
                          <div className="col-12">
                            <h2 className="text-zinc-800 text-3xl mb-5 text-center font-semibold">
                              Pago recibido
                            </h2>
                            <p className="text-base text-zinc-500 text-center">
                              Recibimos tu pago de{" "}
                              <strong>
                                {verifyPayment && verifyPayment?.Price
                                  ? currencyWithSuffixFormatter(
                                      verifyPayment?.Price
                                    )
                                  : 0}
                              </strong>
                              , {`${userInfo?.FirstName} ${userInfo?.Surname}`}
                            </p>
                          </div>
                          <div className="col-12">
                            <hr className="my-5" />
                          </div>
                          <div className="col-12">
                            <div className="row">
                              <div className="col-12">
                                <h3 className="text-xl font-semibold text-center mb-5">
                                  Detalles del pago.
                                </h3>
                              </div>
                              <div className="col-12 mx-auto">
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Transaccion ID</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>{verifyPayment?.IdTransaction}</span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Monto del pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>
                                      {verifyPayment &&
                                        verifyPayment?.Price &&
                                        currencyWithSuffixFormatter(
                                          verifyPayment?.Price
                                        )}
                                    </span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Fecha de pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>
                                      {verifyPayment &&
                                        verifyPayment?.CreateOn &&
                                        format(
                                          parseISO(verifyPayment?.CreateOn),
                                          "MMM d, yyyy"
                                        )}
                                    </span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Método de pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>{verifyPayment?.MethodPayment}</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12">
                            <hr className="my-5" />
                          </div>
                          <div className="col-12">
                            <Link
                              href={"/my-courses"}
                              className="btn btn-primary-green w-full"
                            >
                              Ir a mis cursos
                            </Link>
                          </div>
                        </>
                      )}
                      {statusPayment == -2 && (
                        <>
                          <div className="col-auto mx-auto">
                            <FontAwesomeIcon
                              className="text-7xl text-orange-500 mb-5"
                              icon={faExclamationCircle}
                            />
                          </div>
                          <div className="col-12">
                            <h2 className="text-zinc-800 text-3xl mb-5 text-center font-semibold">
                              Pago pendiente
                            </h2>
                            <p className="text-base text-zinc-500 text-center">
                              Pago pendiente de{" "}
                              <strong>
                                {verifyPayment &&
                                  verifyPayment?.Price &&
                                  currencyWithSuffixFormatter(
                                    verifyPayment?.Price
                                  )}
                              </strong>
                              , {`${userInfo?.FirstName} ${userInfo?.Surname}`}
                            </p>
                          </div>
                          <div className="col-12">
                            <hr className="my-5" />
                          </div>
                          <div className="col-12">
                            <div className="row">
                              <div className="col-12">
                                <h3 className="text-xl font-semibold text-center mb-5">
                                  Detalles del pago.
                                </h3>
                              </div>
                              <div className="col-12 mx-auto">
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Transaccion ID</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>{verifyPayment?.IdTransaction}</span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Monto del pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>
                                      {verifyPayment &&
                                        verifyPayment?.Price &&
                                        currencyWithSuffixFormatter(
                                          verifyPayment?.Price
                                        )}
                                    </span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Fecha de pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>
                                      {verifyPayment &&
                                        verifyPayment?.CreateOn &&
                                        format(
                                          parseISO(verifyPayment?.CreateOn),
                                          "MMM d, yyyy"
                                        )}
                                    </span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Método de pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>{verifyPayment?.MethodPayment}</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12">
                            <hr className="my-5" />
                          </div>
                        </>
                      )}
                      {statusPayment == -3 && (
                        <>
                          <div className="col-auto mx-auto">
                            <FontAwesomeIcon
                              className="text-7xl text-red-500 mb-5"
                              icon={faExclamationCircle}
                            />
                          </div>
                          <div className="col-12">
                            <h2 className="text-zinc-800 text-3xl mb-5 text-center font-semibold">
                              No se pudo efectur el pago
                            </h2>
                            <p className="text-base text-zinc-500 text-center">
                              Pago fallido de{" "}
                              <strong>
                                {verifyPayment &&
                                  verifyPayment?.Price &&
                                  currencyWithSuffixFormatter(
                                    verifyPayment?.Price
                                  )}
                              </strong>
                              , {`${userInfo?.FirstName} ${userInfo?.Surname}`}
                            </p>
                          </div>
                          <div className="col-12">
                            <hr className="my-5" />
                          </div>
                          <div className="col-12">
                            <div className="row">
                              <div className="col-12">
                                <h3 className="text-xl font-semibold text-center mb-5">
                                  Detalles del pago.
                                </h3>
                              </div>
                              <div className="col-12 mx-auto">
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Transaccion ID</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>{verifyPayment?.IdTransaction}</span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Monto del pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>
                                      {verifyPayment &&
                                        verifyPayment?.Price &&
                                        currencyWithSuffixFormatter(
                                          verifyPayment?.Price
                                        )}
                                    </span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Fecha de pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>
                                      {verifyPayment &&
                                        verifyPayment?.CreateOn &&
                                        format(
                                          parseISO(verifyPayment?.CreateOn),
                                          "MMM d, yyyy"
                                        )}
                                    </span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col text-zinc-500 mb-5">
                                    <span>Método de pago</span>
                                  </div>
                                  <div className="col text-zinc-800 text-right font-semibold mb-5">
                                    <span>{verifyPayment?.MethodPayment}</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12">
                            <hr className="my-5" />
                          </div>
                          {/* <div className="col-12">
                              <button className="btn btn-outline-solid-white w-full">
                                Actualizar metodo de pago
                              </button>
                            </div> */}
                        </>
                      )}
                      {statusPayment == -1 && (
                        <>
                          <div className="col-auto mx-auto">
                            <FontAwesomeIcon
                              className="text-7xl text-red-500 mb-5"
                              icon={faExclamationCircle}
                            />
                          </div>
                          <div className="col-12">
                            <h2 className="text-zinc-800 text-3xl mb-5 text-center font-semibold">
                              La transaction no existe o ya fue gestionada
                            </h2>
                          </div>
                          <div className="col-12">
                            <hr className="my-5" />
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
          {status === "loading" ? (
            <>
              <div className="container py-10">
                <div className="col-12 col-lg-6 mx-auto">
                  <div
                    role="status"
                    className="bg-white w-full mx-auto p-4 space-y-4 border border-gray-200 divide-y divide-gray-200 rounded shadow animate-pulse "
                  >
                    <div className="flex items-center justify-between">
                      <div>
                        <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                        <div className="w-32 h-2 bg-gray-200 rounded-full" />
                      </div>
                      <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                    </div>
                    <div className="flex items-center justify-between pt-4">
                      <div>
                        <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                        <div className="w-32 h-2 bg-gray-200 rounded-full" />
                      </div>
                      <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                    </div>
                    <div className="flex items-center justify-between pt-4">
                      <div>
                        <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                        <div className="w-32 h-2 bg-gray-200 rounded-full" />
                      </div>
                      <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                    </div>
                    <div className="flex items-center justify-between pt-4">
                      <div>
                        <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                        <div className="w-32 h-2 bg-gray-200 rounded-full" />
                      </div>
                      <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                    </div>
                    <div className="flex items-center justify-between pt-4">
                      <div>
                        <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                        <div className="w-32 h-2 bg-gray-200 rounded-full" />
                      </div>
                      <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                    </div>
                    <span className="sr-only">Loading...</span>
                  </div>
                </div>
              </div>
            </>
          ) : null}
        </main>
        <Footer />
      </div>
    </>
  );
}
