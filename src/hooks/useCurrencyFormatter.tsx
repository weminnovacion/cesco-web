export function useCurrencyFormatter() {
  const currencyFormatter = (amount: number): string => {
    return amount?.toLocaleString("es-CO", {
      style: "currency",
      currency: "COP",
      minimumFractionDigits: 0,
      localeMatcher: "lookup",
    });
  };

  const currencyWithSuffixFormatter = (amount: number): string => {
    const formattedAmount = currencyFormatter(amount);
    return `${formattedAmount} COP`;
  };

  return { currencyFormatter, currencyWithSuffixFormatter };
}
