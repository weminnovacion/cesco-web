import { useEffect, useState } from "react";

export const useInitials = (name: string) => {
  const [initials, setInitials] = useState("");

  useEffect(() => {
    const calculatedInitials = name
      .split(" ")
      .map((str) => (str ? str[0].toUpperCase() : ""))
      .join("");

    setInitials(calculatedInitials);
  }, [name]);

  return initials;
};
