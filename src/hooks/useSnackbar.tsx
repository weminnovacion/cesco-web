import { useState, createContext, useContext, ReactNode } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheckCircle,
  faTimesCircle,
  faSpinner,
  faInfoCircle,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";

type SnackbarType = "loading" | "success" | "error" | "info";

interface SnackbarOptions {
  id: number;
  message: string;
  type: SnackbarType;
}

interface SnackbarContextValue {
  openSnackbar: (message: string, type: SnackbarType) => void;
  closeSnackbar: (id: number) => void;
}

const SnackbarContext = createContext<SnackbarContextValue | null>(null);

let snackbarId = 0;

interface SnackbarProviderProps {
  children: ReactNode;
}

const SnackbarProvider = ({ children }: SnackbarProviderProps) => {
  const [snackbars, setSnackbars] = useState<SnackbarOptions[]>([]);

  const openSnackbar = (message: string, type: SnackbarType) => {
    const newSnackbar: SnackbarOptions = {
      id: snackbarId++,
      message,
      type,
    };

    setSnackbars((prevSnackbars) => [...prevSnackbars, newSnackbar]);
  };

  const closeSnackbar = (id: number) => {
    setSnackbars((prevSnackbars) =>
      prevSnackbars.filter((snackbar) => snackbar.id !== id)
    );
  };

  const SnackbarIcon = ({ type }: { type: SnackbarType }) => {
    let icon: any;

    switch (type) {
      case "loading":
        icon = faSpinner;
        break;
      case "success":
        icon = faCheckCircle;
        break;
      case "error":
        icon = faTimesCircle;
        break;
      case "info":
      default:
        icon = faInfoCircle;
        break;
    }

    return <FontAwesomeIcon icon={icon} />;
  };

  const value: SnackbarContextValue = {
    openSnackbar,
    closeSnackbar,
  };

  return (
    <SnackbarContext.Provider value={value}>
      {children}
      {snackbars.map((snackbar) => (
        <div
          key={snackbar.id}
          className="fixed z-50 bottom-0 right-2 bg-green-500 text-white p-4 flex items-center justify-between"
        >
          <div className="flex items-center">
            <span className="mr-2">
              <SnackbarIcon type={snackbar.type} />
            </span>
            <span>{snackbar.message}</span>
          </div>
          <button
            className="w-6 h-6"
            onClick={() => closeSnackbar(snackbar.id)}
          >
            <FontAwesomeIcon icon={faTimes} />
          </button>
        </div>
      ))}
    </SnackbarContext.Provider>
  );
};

const useSnackbar = () => {
  const context = useContext(SnackbarContext);

  if (!context) {
    throw new Error("useSnackbar must be used within a SnackbarProvider");
  }

  return context;
};

export { SnackbarProvider, useSnackbar };
