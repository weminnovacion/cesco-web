import { ModuleCourse } from "./moduleCourse.model"

export interface CourseResponse {
  Id: number,
  CategoryCourse: {
    Id: number,
    Name: string
  },
  TypeCourse: {
    Id: number,
    Name: string
  },
  Professor: {
    Name: string,
    Profession: string,
    Descripcion: string,
    Photo: string,
    Biography: string,
    CreateOn: string,
  },
  IsAddToCart: boolean,
  AlreadyBought: boolean,
  IsPromotion: boolean
  ExpirationDate: string,
  Modules: ModuleCourse[],
  Name: string,
  isCombo: boolean,
  IsCertificate: boolean,
  Description: string,
  ImgCover: string,
  ActiveEvaluation: boolean,
  ImgProfile: string,
  HoursContent: number,
  HoursPractice: number,
  Previousknowledge: string,
  CourseElements: string,
  WhatYouWillLearn: string,
  UrlVideoPresentation: string,
  TotalModules: number,
  TotalStudent: number,
  HoursTotalCourse: number,
  Resolution: string,
  OpenCourse: boolean,
  Price: number,
  PriceWithDiscount: number,
  DisountedPrice: number,
  DiscountRate: number,
  CreateOn: string,
  UpdateOn: string
}