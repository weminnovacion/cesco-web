import { CourseProps } from "./courseProps.model";
import { ComboProps } from "./comboProps.model";

export type CartItem = CourseProps & { type: "course" } | ComboProps & { type: "combo" };