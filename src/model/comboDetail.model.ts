import { ComboResponse } from "./comboResponse.model"
import { CourseResponse } from "./courseResponse.model"

export interface ComboDetail {
    combo: ComboResponse,
    courses: CourseResponse[]
}