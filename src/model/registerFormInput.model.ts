interface RegisterFormInput {
  IdCountry: number;
  IdDepartment: number;
  IdCity: number;
  IdTypeIdentification: number;
  IdProfession: number;
  Identification: string;
  IdGender: number;
  FirstName: string;
  SecondName: string;
  SecondSurname: string;
  Address: string;
  Birthdate: string;
  Biography: string;
  Surname: string;
  Email: string;
  Phone: string;
  Password: string;
};

interface Identification {
  Id: number;
  Name: string;
}

interface Profession {
  Id: number;
  Name: string;
}

interface Gender {
  Id: number;
  Name: string;
}

interface Countries {
  Id: number;
  Name: string;
  Departaments: Departaments[]
}

interface Departaments {
  Id: number;
  Name: string;
  Cities: Cities[]
}

interface Cities {
  Id: number;
  Name: string;
}