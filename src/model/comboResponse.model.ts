export interface ComboResponse {
  CategoryCourse: {
    Id: number, Name: string
  },
  Id: number,
  Name: string,
  CourseHourTotal: number,
  CreateOn: string,
  Description: string,
  Img: string,
  NumCourses: number,
  UpdateOn: string,
  Price: number,
  PriceTotal: number,
  ComboValueWithoutDiscount: number,
  PercentageDiscountForTheCombo: number,
  DiscountValueForTheCombo: number,
  TotalValueOfTheComboWithTheDiscount: number,
}