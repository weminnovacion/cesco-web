interface RecordPayment {
  CardNumber: string,
  ExpYear: string,
  ExpMonth: string,
  Cvc: string
}