import { ReactNode } from "react";
import { ComboResponse } from "./comboResponse.model";

export interface ComboProps extends ComboResponse {
  children?: ReactNode
}