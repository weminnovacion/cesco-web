interface UserInfoResponse {
    Username: string,
    Rol: {
        Id: number,
        Name: string
    },
    TypeIdentification: {
        Id: number,
        Name: string
    },
    Gender: {
        Id: number,
        Name: string
    },
    Profession: {
        Id: number,
        Name: string,
        CreateOn: string
    },
    Country: UbiGeo,
    Department: UbiGeo,
    City: UbiGeo,
    Identification: string,
    FirstName: string,
    SecondName: string,
    Surname: string,
    SecondSurname: string,
    Birthdate: string,
    Biography: string,
    Phone: string,
    Email: string,
    Address: string,
    Photo: string,
    CreateOn: string,
    UpdateOn: string
}

interface UbiGeo {
    Id: number,
    Name: string
}