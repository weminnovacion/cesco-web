interface Evaluation {
  NameCourse: string,
  Questions: Question[]
}
interface Question {
  Id: number;
  Question: string;
  Dets: Option[];
}
interface Option {
  Id: number;
  Answer: string;
  Correct: boolean | null;
}
interface ResultQuestion {
  Calification: number;
  NameCourse: string;
  NumQuetions: number;
  NumQuetionsTrue: number;
  UrlCertificate: string | null;
}