import { ReactElement } from "react";
import { TabProps } from "@/model/tabProps.model";

export interface TabsProps {
  children: ReactElement<TabProps>[];
}