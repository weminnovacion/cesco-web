export interface PromotionCourse {
    PriceDiscount: number,
    ExpirationDate: string,
    DiscountRate: number,
}