interface VerifyPayment {
    IdTransaction: string,
    MethodPayment: string,
    Price: number | undefined,
    CreateOn: string | undefined
}