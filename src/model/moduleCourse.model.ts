export interface ModuleCourse {
  Id: string | 0 | undefined,
  Name: string,
  Description: string,
  UrlVideo: string,
  OpenLesson: boolean,
  CreateOn: string,
  UpdateOn: string
  Resources: Resources[]
}


export interface Resources {
  IdTypeResource: number,
  Name: string,
  Value: string
}