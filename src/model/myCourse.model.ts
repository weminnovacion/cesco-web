interface MyCourse {
  NameCourse: string,
  ImgProfile: string,
  Price: number,
  PriceDiscount: number,
  HoursContent: number,
  Certificate: boolean,
  UrlCertificate: string,
  UrlConstancy: string,
  IdCourse: number,
  ActiveEvaluation: boolean,
  Professor: string,
  TotalModules: number,
  DateCertificate: string,
  TotalStudent: number
}