import { ReactNode } from "react";
import { CourseResponse } from "./courseResponse.model";

export interface CourseProps extends CourseResponse {
  children?: ReactNode
}