export const LoadingCart = () => {
  return (
    <>
      <div className="row">
        <div className="col-12">
          <div className="bg-white w-full rounded p-5 mb-5">
            <div
              role="status"
              className="w-full p-4 space-y-4  divide-y divide-gray-200 rounded animate-pulse "
            >
              <div className="flex items-center justify-between">
                <div>
                  <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                  <div className="w-32 h-2 bg-gray-200 rounded-full" />
                </div>
                <div className="h-2.5 bg-gray-300 rounded-full w-12" />
              </div>
              <div className="flex items-center justify-between pt-4">
                <div>
                  <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                  <div className="w-32 h-2 bg-gray-200 rounded-full" />
                </div>
                <div className="h-2.5 bg-gray-300 rounded-full w-12" />
              </div>
              <div className="flex items-center justify-between pt-4">
                <div>
                  <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                  <div className="w-32 h-2 bg-gray-200 rounded-full" />
                </div>
                <div className="h-2.5 bg-gray-300 rounded-full w-12" />
              </div>
              <div className="flex items-center justify-between pt-4">
                <div>
                  <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                  <div className="w-32 h-2 bg-gray-200 rounded-full" />
                </div>
                <div className="h-2.5 bg-gray-300 rounded-full w-12" />
              </div>
              <div className="flex items-center justify-between pt-4">
                <div>
                  <div className="h-2.5 bg-gray-300 rounded-full w-24 mb-2.5" />
                  <div className="w-32 h-2 bg-gray-200 rounded-full" />
                </div>
                <div className="h-2.5 bg-gray-300 rounded-full w-12" />
              </div>
              <div className="py-4">
                <div className="flex bg-gray-100 items-center justify-between p-4">
                  <div className="h-2.5 bg-gray-300 rounded-full w-24 " />
                  <div className="h-2.5 bg-gray-300 rounded-full w-12" />
                </div>
              </div>
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
