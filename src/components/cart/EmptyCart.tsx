import Link from "next/link";
import {
  faArrowRight,
  faCartShopping,
  faInfo,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const EmptyCart = () => {
  return (
    <>
      <div className="row">
        <div className="col-12">
          <h1 className="text-zinc-800 font-semibold text-2xl mb-5">
            <FontAwesomeIcon icon={faCartShopping} className="mr-2" />
            Carrito
          </h1>
        </div>
        <div className="col-12">
          <div className="bg-white p-5 rounded">
            <div className="row">
              <div className="col-auto">
                <div className="w-12 h-12 flex justify-center items-center bg-zinc-100 text-blue-400 text-lg rounded-full">
                  <FontAwesomeIcon icon={faInfo} />
                </div>
              </div>
              <div className="col">
                <h3 className="text-zinc-800 font-semibold text-lg">
                  Tu carrito está vacío
                </h3>
                <p className="text-zinc-500 text-base">
                  Añade cursos a tu carrito y empieza a aprender con los mejores
                  profesionales.
                </p>
              </div>
              <div className="col-auto">
                <Link href={"/courses"} className="btn btn-outline-solid-white">
                  <span className="mr-2">Continuar comprando</span>
                  <FontAwesomeIcon icon={faArrowRight} />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
