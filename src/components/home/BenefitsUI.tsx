import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

export const BenefitsUI = () => {
  return (
    <>
      <section className="bg-white border-b border-primary-green w-full h-full py-10 md:py-20">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h1 className="mb-5 md:mb-10 text-2xl text-center font-extrabold tracking-tight leading-none text-primary-green md:text-4xl lg:text-4xl">
                Nuestros beneficios
              </h1>
            </div>
            <div className="col-lg-6 mb-10">
              <div className="row">
                <div className="col-auto">
                  <div className="w-8 h-8 md:w-10 md:h-10 rounded-full border border-primary-green text-primary-green flex justify-center items-center">
                    <FontAwesomeIcon icon={faCheck} />
                  </div>
                </div>
                <div className="col flex items-center">
                  <h2 className="text-base md:text-xl text-zinc-500 font-normal">
                    Cursos especializados impartidos por medicos certificados
                    del area de la salud.
                  </h2>
                </div>
              </div>
            </div>
            <div className="col-lg-6 mb-10">
              <div className="row">
                <div className="col-auto">
                  <div className="w-8 h-8 md:w-10 md:h-10 rounded-full border border-primary-green text-primary-green flex justify-center items-center">
                    <FontAwesomeIcon icon={faCheck} />
                  </div>
                </div>
                <div className="col flex items-center">
                  <h2 className="text-base md:text-xl text-zinc-500 font-normal">
                    Diplomas de certificacion en el arear que estudies y
                    apruebes.
                  </h2>
                </div>
              </div>
            </div>
            <div className="col-lg-6 mb-10">
              <div className="row">
                <div className="col-auto">
                  <div className="w-8 h-8 md:w-10 md:h-10 rounded-full border border-primary-green text-primary-green flex justify-center items-center">
                    <FontAwesomeIcon icon={faCheck} />
                  </div>
                </div>
                <div className="col flex items-center">
                  <h2 className="text-base md:text-xl text-zinc-500 font-normal">
                    Certificación inmediata con 2 años de validez a nivel
                    Nacional.
                  </h2>
                </div>
              </div>
            </div>
            <div className="col-lg-6 mb-10">
              <div className="row">
                <div className="col-auto">
                  <div className="w-8 h-8 md:w-10 md:h-10 rounded-full border border-primary-green text-primary-green flex justify-center items-center">
                    <FontAwesomeIcon icon={faCheck} />
                  </div>
                </div>
                <div className="col flex items-center">
                  <h2 className="text-base md:text-xl text-zinc-500 font-normal">
                    Descargar constancia de inscripción al curso inmediatamente.
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
