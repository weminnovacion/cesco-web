export const ReviewsUI = () => {
  return (
    <>
      <section className="bg-primary-green/5 border-b border-primary-green w-full h-full py-20">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h1 className="mb-5 md:mb-10 text-2xl text-center font-extrabold tracking-tight leading-none text-primary-green md:text-4xl lg:text-4xl">
                ¿Qué dicen nuestros clientes de nosotros?
              </h1>
            </div>
            <div className="col-lg-12">
              <div className="row">
                <div className="col-12 col-md-6 col-lg-4 mb-5">
                  <figure className="flex flex-col items-center justify-center p-8 text-center bg-white border-b border-primary-green rounded-t">
                    <blockquote className="max-w-2xl mx-auto mb-4 text-gray-500 lg:mb-8 ">
                      <p className="my-4">
                        Excelente curso y muy buen material. El Dr Rafael
                        Quintero explico muy bien del tema.
                      </p>
                    </blockquote>
                    <figcaption className="flex items-center justify-center space-x-3">
                      <img
                        className="rounded-full w-9 h-9"
                        src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/avatars/karen-nelson.png"
                        alt="profile picture"
                      />
                      <div className="space-y-0.5 font-medium  text-left">
                        <div>Bonnie Green</div>
                        <div className="text-sm text-gray-500 ">Auxiliar</div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div className="col-12 col-md-6 col-lg-4 mb-5">
                  <figure className="flex flex-col items-center justify-center p-8 text-center bg-white border-b border-primary-green rounded-t ">
                    <blockquote className="max-w-2xl mx-auto mb-4 text-gray-500 lg:mb-8 ">
                      <p className="my-4">
                        Me gusto mucho la oferta 5x1. Satisfecha con la
                        enseñanza y la atención excelente servicio.
                      </p>
                    </blockquote>
                    <figcaption className="flex items-center justify-center space-x-3">
                      <img
                        className="rounded-full w-9 h-9"
                        src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/avatars/roberta-casas.png"
                        alt="profile picture"
                      />
                      <div className="space-y-0.5 font-medium  text-left">
                        <div>Roberta Casas</div>
                        <div className="text-sm text-gray-500 ">
                          Lead designer at Dropbox
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div className="col-12 col-md-6 col-lg-4 mb-5">
                  <figure className="flex flex-col items-center justify-center p-8 text-center bg-white border-b border-primary-green rounded-t ">
                    <blockquote className="max-w-2xl mx-auto mb-4 text-gray-500 lg:mb-8 ">
                      <p className="my-4">
                        Me gusto mucho la oferta 5x1. Satisfecha con la
                        enseñanza y la atención excelente servicio.
                      </p>
                    </blockquote>
                    <figcaption className="flex items-center justify-center space-x-3">
                      <img
                        className="rounded-full w-9 h-9"
                        src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/avatars/roberta-casas.png"
                        alt="profile picture"
                      />
                      <div className="space-y-0.5 font-medium  text-left">
                        <div>Roberta Casas</div>
                        <div className="text-sm text-gray-500 ">
                          Lead designer at Dropbox
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
