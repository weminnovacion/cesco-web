export const CertificatesUI = () => {
  return (
    <>
      <section className="bg-white border-b border-primary-green w-full h-full py-20">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h1 className="mb-5 text-2xl text-center font-extrabold tracking-tight leading-none text-primary-green md:text-4xl lg:text-4xl">
                Obtendrás un certificado cada vez que completes un curso o
                diplomado
              </h1>
              <div className="row">
                <div className="col-12 col-lg-8 mx-auto">
                  <p className="mb-10 text-sm text-center tracking-tight leading-none text-zinc-500 md:text-base lg:text-lg">
                    Cuando completes un curso o diplomado que hayas comprado
                    obtendrás un certificado personalizado firmado por el
                    profesor de la clase.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="relative mx-auto border-gray-800 dark:border-gray-800 bg-gray-800 border-[8px] rounded-t-xl h-[172px] max-w-[301px] md:h-[294px] md:max-w-[512px]">
                <div className="rounded-lg overflow-hidden h-[156px] md:h-[278px] bg-white dark:bg-gray-800">
                  <img
                    src="https://cesco.s3.amazonaws.com/uploads/public/certificado_home.png"
                    className="h-[156px] md:h-[278px] w-full rounded-xl"
                    alt=""
                  />
                </div>
              </div>
              <div className="relative mx-auto bg-gray-900 dark:bg-gray-700 rounded-b-xl rounded-t-sm h-[17px] max-w-[351px] md:h-[21px] md:max-w-[597px]">
                <div className="absolute left-1/2 top-0 -translate-x-1/2 rounded-b-xl w-[56px] h-[5px] md:w-[96px] md:h-[8px] bg-gray-800"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
