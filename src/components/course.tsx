//React
import { FC, useContext, useState } from "react";

//Models
import { CourseProps } from "@/model/courseProps.model";

//Context
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";

//Components
import { Modal } from "@/components/Modal";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faBook,
  faBookOpen,
  faCartShopping,
  faClock,
  faFilm,
  faGraduationCap,
  faTimes,
  faUser,
  faUserAlt,
} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import { formatDuration, intervalToDuration } from "date-fns";
import { CartContext } from "@/contexts/CartContext";
import { CartItem } from "@/model/cart.model";
import { useRouter } from "next/router";

export const Course: FC<CourseProps> = ({ children, ...rest }) => {
  const {
    Id,
    CategoryCourse,
    TypeCourse,
    Professor,
    Name,
    Description,
    ImgProfile,
    HoursContent,
    UrlVideoPresentation,
    TotalModules,
    AlreadyBought,
    IsPromotion,
    TotalStudent,
    HoursTotalCourse,
    Price,
    PriceWithDiscount,
    DiscountRate,
    ExpirationDate,
  } = rest;
  const { currencyWithSuffixFormatter } = useCurrencyFormatter();
  const router = useRouter();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const { addToCart } = useContext(CartContext);

  const handleToggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const handleAddToCart = (course: CourseProps) => {
    const item: CartItem = { ...course, type: "course" };
    addToCart(item);
    router.push("/cart");
  };

  const getFormattedDuration = (startDate: any, endDate: any) => {
    const duration = intervalToDuration({
      start: startDate,
      end: endDate,
    });

    return `${duration.days}d: ${duration.hours}h: ${duration.minutes}m: ${duration.seconds}s`;
  };

  return (
    <>
      <div className="card-course border shadow">
        <button className="card-course-header" onClick={handleToggleModal}>
          <div className="absolute top-3 left-3 text-white bg-primary-orange/70 text-xs px-3">
            {CategoryCourse?.Name}
          </div>
          <img src={ImgProfile} alt={Name} />
          <div className="card-course-header-play">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
              className="w-6 h-6"
            >
              <path
                fillRule="evenodd"
                d="M4.5 5.653c0-1.426 1.529-2.33 2.779-1.643l11.54 6.348c1.295.712 1.295 2.573 0 3.285L7.28 19.991c-1.25.687-2.779-.217-2.779-1.643V5.653z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          {IsPromotion && (
            <>
              <div className="absolute bottom-2 left-1/2 -translate-x-1/2 w-full px-3">
                <div className="text-white  bg-primary-orange/80 text-xs rounded-sm p-1">
                  {DiscountRate}% DTO. Termina en{" "}
                  {getFormattedDuration(
                    new Date(ExpirationDate.toString()),
                    new Date()
                  )}
                </div>
              </div>
            </>
          )}
        </button>
        <div className="card-course-body">
          <div className="row">
            <div className="col-12">
              <div className="row">
                <div className="col-auto mb-3">
                  <div className="px-1 border text-zinc-900 rounded-sm text-xs">
                    Tipo de curso · {TypeCourse?.Name}
                  </div>
                </div>
                <div className="col-12">
                  <h3
                    className="text-lg text-zinc-700 hover:text-primary-green transition-all duration-300 font-bold mb-3 capitalize text-container-line-1"
                    title={Name}
                  >
                    <Link
                      href={`/courses/${Id}-${Name?.toLowerCase().replace(
                        /\s+/g,
                        "-"
                      )}/course`}
                      className="a-text-secondary "
                    >
                      {Name}
                    </Link>
                  </h3>
                  <p className="text-xs text-zinc-900 mb-3">
                    Un curso de {Professor?.Name}
                  </p>
                  <p className="text-xs text-zinc-500 text-container-line-3 mb-4">
                    {Description}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="card-course-footer">
          <div className="row mb-4">
            <div className="col-auto">
              <div className="flex items-center text-xs text-opacity">
                <div className="mr-1">
                  <FontAwesomeIcon
                    icon={faUser}
                    className="mr-1 text-primary-green"
                  />
                </div>
                {TotalStudent}
              </div>
            </div>
            <div className="col-auto">
              <div className="flex items-center text-xs text-opacity">
                <div className="mr-1">
                  <FontAwesomeIcon
                    icon={faBookOpen}
                    className="mr-1 text-primary-green"
                  />
                </div>
                {TotalModules}
              </div>
            </div>
            <div className="col-auto">
              <div className="flex items-center text-xs text-opacity">
                <div className="mr-1">
                  <FontAwesomeIcon
                    icon={faClock}
                    className="mr-1 text-primary-green"
                  />
                </div>
                {HoursContent}
              </div>
            </div>
          </div>
          <div className="text-xs text-primary-orange mb-4">
            <span className="text-primary-orange text-base justify-center">
              {IsPromotion && (
                <>
                  {DiscountRate}% Dto.
                  <strong className="line-through">
                    {currencyWithSuffixFormatter(Price)}
                  </strong>
                </>
              )}
              {!IsPromotion && PriceWithDiscount && Price ? (
                <>
                  {Math.round(((Price - PriceWithDiscount) / Price) * 100)}%
                  Dto.
                  <strong className="line-through">
                    {currencyWithSuffixFormatter(Price)}
                  </strong>
                </>
              ) : (
                ""
              )}
              {!IsPromotion && !PriceWithDiscount && (
                <>
                  100% Dto.
                  <strong className="line-through text-sm">
                    {" "}
                    {currencyWithSuffixFormatter(0)}
                  </strong>
                </>
              )}
              <br />
            </span>
          </div>
          {children}
        </div>
      </div>

      <Modal isOpen={isModalOpen} onClose={handleToggleModal} size="max-w-3xl">
        <div className="py-8 px-4 lg:px-6">
          <button
            onClick={handleToggleModal}
            className="w-8 h-8 absolute top-0 right-0 flex justify-center items-center text-zinc-700 opacity-80 text-xl"
          >
            <FontAwesomeIcon icon={faTimes} className="text-primary-green" />
          </button>
          <div className="row">
            <div className="col-12 mb-4">
              <div className="w-full h-auto max-w-full border border-gray-200 rounded-lg overflow-hidden">
                <div
                  style={{
                    padding: "56.25% 0 0 0",
                    position: "relative",
                  }}
                >
                  <iframe
                    src={UrlVideoPresentation}
                    frameBorder={0}
                    allow="autoplay; fullscreen; picture-in-picture"
                    allowFullScreen
                    className="w-full h-96 relative overflow-hidden bg-zinc-200 rounded-lg mb-4"
                    style={{
                      position: "absolute",
                      top: 0,
                      left: 0,
                      width: "100%",
                      height: "100%",
                    }}
                    title="avril (2)"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="row">
                <div className="col-12 col-lg-8">
                  <Link
                    href={`/courses/${Id}-${Name?.toLowerCase().replace(
                      /\s+/g,
                      "-"
                    )}/course`}
                    className="a-text-secondary font-bold h-4"
                  >
                    {Name}
                  </Link>
                  <p className="text-zinc-500 text-xs mb-3">
                    Un curso de {Professor?.Name}
                  </p>
                  <p className="text-sm text-container-line-6 mb-5">
                    {Description}
                  </p>
                </div>
                <div className="col-12 col-lg-4">
                  {AlreadyBought && (
                    <Link
                      href={`/courses/${Id}-${Name?.toLowerCase().replace(
                        /\s+/g,
                        "-"
                      )}/course`}
                      className="btn btn-tramitar-pedido w-full flex items-center"
                    >
                      <span>Entrar</span>
                      <FontAwesomeIcon icon={faArrowRight} className="ml-2" />
                    </Link>
                  )}
                  {!AlreadyBought && (
                    <button
                      type="button"
                      onClick={() => {
                        handleAddToCart(rest);
                      }}
                      className={`btn btn-primary-green w-full flex items-center`}
                    >
                      <FontAwesomeIcon icon={faCartShopping} />
                      <span className="mx-auto text-sm">
                        Comprar {currencyWithSuffixFormatter(PriceWithDiscount)}
                      </span>
                    </button>
                  )}

                  <br />
                  <span className="text-sm mb-2 text-primary-orange">
                    {IsPromotion && (
                      <>
                        {DiscountRate}% Dto.
                        <strong className="line-through">
                          {" "}
                          {currencyWithSuffixFormatter(Price)}
                        </strong>
                      </>
                    )}
                    {!IsPromotion && PriceWithDiscount && Price ? (
                      <>
                        {Math.round(
                          ((Price - PriceWithDiscount) / Price) * 100
                        )}
                        % Dto.
                        <strong className="line-through">
                          {" "}
                          {currencyWithSuffixFormatter(Price)}
                        </strong>
                      </>
                    ) : (
                      ""
                    )}
                    <br />
                    {IsPromotion && (
                      <>
                        <p className="text-sm mb-2 text-primary-orange">
                          Termina en{" "}
                          {getFormattedDuration(
                            new Date(ExpirationDate.toString()),
                            new Date()
                          )}
                        </p>
                      </>
                    )}
                  </span>
                  <div>
                    <ul>
                      <li className="flex items-center gap-2 mb-3 text-xs">
                        <FontAwesomeIcon
                          icon={faUserAlt}
                          className="text-primary-green"
                        />
                        <span>{TotalStudent} Estudiantes</span>
                      </li>
                      <li className="flex items-center gap-2 mb-3 text-xs">
                        <FontAwesomeIcon
                          icon={faFilm}
                          className="text-primary-green"
                        />
                        <span>
                          {TotalModules} modulo
                          {TotalModules?.toString.length > 0 ?? "s"} (
                          {HoursTotalCourse} Hr)
                        </span>
                      </li>
                      <li className="flex items-center gap-2 mb-3 text-xs">
                        <FontAwesomeIcon
                          icon={faBook}
                          className="text-primary-green"
                        />
                        <span>Categoria: {CategoryCourse?.Name}</span>
                      </li>
                      <li className="flex items-center gap-2 mb-3 text-xs">
                        <FontAwesomeIcon
                          icon={faGraduationCap}
                          className="text-primary-green"
                        />
                        <span>Tipo: {TypeCourse?.Name}</span>
                      </li>
                      <li className="flex items-center gap-2 mb-3 text-xs">
                        <FontAwesomeIcon
                          icon={faClock}
                          className="text-primary-green"
                        />
                        <span>Audio: Español</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};
