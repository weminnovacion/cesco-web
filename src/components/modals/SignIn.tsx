import { FC, useState, FormEvent, useEffect, useContext } from "react";

export type ButtonState =
  | "default"
  | "loading"
  | "disabled"
  | "success"
  | "error";

//Components
import { Modal } from "@/components/Modal";
import { Button } from "@/components/Button";

//Context
import { AuthContext } from "@/contexts/AuthContext";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

import { setCookie } from "nookies";
import axios from "axios";
import Link from "next/link";
import { API_URL } from "../../../constants";

interface ModalSignInProps {
  isOpen: boolean;
  onClose: () => void;
  handleToggleModalSignIn: () => void;
}

export const ModalSignIn: FC<ModalSignInProps> = ({
  isOpen,
  onClose,
  handleToggleModalSignIn,
}) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [isFormDisabled, setIsFormDisabled] = useState(false);
  const [buttonState, setButtonState] = useState<ButtonState>("default");
  const { login } = useContext(AuthContext);

  const handleSignIn = async (event: FormEvent) => {
    event.preventDefault();
    setButtonState("loading");
    setIsFormDisabled(true);
    try {
      const response = await axios.post(API_URL + "oauth/signin", {
        username,
        password,
      });
      // Procesar la respuesta en caso de éxito
      login();
      const token = response.data.Data.token;
      setCookie(null, "token", token, {
        maxAge: 30 * 24 * 60 * 60, // Expira en 30 días
        path: "/", // Ruta global
      });
      setButtonState("success");
      setTimeout(() => {
        setButtonState("default");
      }, 2000);
      onClose();
      setIsFormDisabled(false);
      setUsername("");
      setPassword("");
    } catch (error) {
      setButtonState("error");
      setIsFormDisabled(false);
      setTimeout(() => {
        setButtonState("default");
      }, 2000);
      if (axios.isAxiosError(error)) {
        setError("Error: " + error.message);
      } else {
        setError("Error: " + error);
      }
    }
  };

  const clearErrors = () => {
    setError("");
  };

  useEffect(() => {}, []);

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <form onSubmit={handleSignIn}>
          <div className="py-4 px-6 flex justify-between border-b">
            <h2 className="text-lg font-bold">Ingresar</h2>
            <button
              type="button"
              className="w-6 h-6 flex justify-center items-center text-zinc-700"
              onClick={onClose}
            >
              <FontAwesomeIcon icon={faTimes} />
            </button>
          </div>
          <div>
            {error && (
              <div
                id="alert-border-2"
                className="flex py-4 px-6 text-red-800 border-b-4 border-red-300 bg-red-50 dark:text-red-400 dark:bg-gray-800 dark:border-red-800"
                role="alert"
              >
                <svg
                  className="flex-shrink-0 w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <div className="ml-3 text-sm font-medium">{error}</div>
                <button
                  onClick={clearErrors}
                  type="button"
                  className="ml-auto -mx-1.5 -my-1.5 bg-red-50 text-red-500 rounded-md focus:ring-2 focus:ring-red-400 p-1.5 hover:bg-red-200 inline-flex h-8 w-8 dark:bg-gray-800 dark:text-red-400 dark:hover:bg-gray-700"
                  data-dismiss-target="#alert-border-2"
                  aria-label="Close"
                >
                  <span className="sr-only">Dismiss</span>
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    ></path>
                  </svg>
                </button>
              </div>
            )}
          </div>
          <div className="py-4 px-6 bg-zinc-100">
            <div className="flex flex-wrap -mx-3">
              <div className="w-full px-3">
                <div className="mb-4">
                  <input
                    type="text"
                    id="email"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3"
                    placeholder="Email"
                    required
                    disabled={isFormDisabled}
                  />
                </div>
              </div>
              <div className="w-full px-3">
                <div className="mb-4">
                  <input
                    type="password"
                    id="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3"
                    placeholder="Password"
                    required
                    disabled={isFormDisabled}
                  />
                  <div className="flex justify-end mt-2">
                    <Link
                      href="/auth/password/edit"
                      role="button"
                      className="text-sm text-right ml-auto hover:text-secondary transition-all duration-300"
                    >
                      ¿Olvidaste tu contraseña?
                    </Link>
                  </div>
                </div>
              </div>
              <div className="w-full px-3">
                <div className="mb-4">
                  <Button currentState={buttonState} typeButton={"submit"}>
                    Entrar
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="py-4 px-6 flex justify-center border-t">
            <p className="text-center text-base">¿No tienes cuenta?</p>
            <a
              role="button"
              className="ml-3 text-secondary hover:underline"
              onClick={() => {
                onClose();
                handleToggleModalSignIn();
              }}
            >
              Crear Cuenta
            </a>
          </div>
        </form>
      </Modal>
    </>
  );
};
