import { FC } from "react";

//Components
import { Modal } from "@/components/Modal";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

interface ModalSignUpProps {
  isOpen: boolean;
  onClose: () => void;
  handleToggleModalSignUp: () => void;
}

export const ModalSignUp: FC<ModalSignUpProps> = ({
  isOpen,
  onClose,
  handleToggleModalSignUp,
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} size="max-w-2xl">
      <div className="py-4 px-6 relative">
        <h1 className="text-center text-3xl font-bold text-secondary mb-2">
          CESCO
        </h1>
        <p className="text-zinc-800 text-center">
          Aprende con los mejores profesionales
        </p>
        <button
          className="w-6 h-6 flex justify-center items-center text-zinc-700 absolute top-2 right-2"
          onClick={onClose}
        >
          <FontAwesomeIcon icon={faTimes} />
        </button>
      </div>
      <div className="py-4 px-6 bg-zinc-100">
        <div className="flex flex-wrap -mx-3">
          <div className="w-full px-3">
            <div className="mb-4 text-sm text-zinc-500">
              Regístrate con tu email
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <div className="mb-4">
              <select
                defaultValue="US"
                id="IdTypeIdentification"
                className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3 placeholder:text-zinc-900"
                required
              >
                <option>Tipo de identificación...</option>
                <option value={"US"}>United States</option>
              </select>
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <div className="mb-4">
              <select
                defaultValue="US"
                id="IdProfession"
                className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3 placeholder:text-zinc-900"
                required
              >
                <option>Profesión...</option>
                <option value={"US"}>United States</option>
              </select>
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <div className="mb-4">
              <input
                type="text"
                id="Identification"
                className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3 placeholder:text-zinc-900 "
                placeholder="Identificación"
                required
              />
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <div className="mb-4">
              <input
                type="text"
                id="firstName"
                className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3 placeholder:text-zinc-900 "
                placeholder="Nombres"
                required
              />
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <div className="mb-4">
              <input
                type="text"
                id="surname"
                className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3 placeholder:text-zinc-900 "
                placeholder="Apellidos"
                required
              />
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <div className="mb-4">
              <input
                type="email"
                id="phone"
                className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3 placeholder:text-zinc-900 "
                placeholder="Teléfono / Celular"
                required
              />
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <div className="mb-4">
              <input
                type="email"
                id="email"
                className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3 placeholder:text-zinc-900 "
                placeholder="Email"
                required
              />
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <div className="mb-4">
              <input
                type="password"
                id="password"
                className="bg-white border border-zinc-300 text-zinc-900 text-base focus:ring-blue-500 focus:border-blue-500 block w-full p-3 placeholder:text-zinc-900 "
                placeholder="Password"
                required
              />
            </div>
          </div>
          <div className="w-full px-3">
            <div className="mb-4">
              <button className="btn btn-primary-green w-full" type="button">
                Crear Cuenta
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="py-4 px-6 flex justify-center border-t">
        <p className="text-center text-base">¿Ya tienes cuenta?</p>
        <a
          role="button"
          className="ml-3 text-secondary hover:underline"
          onClick={() => {
            handleToggleModalSignUp();
            onClose();
          }}
        >
          Entrar
        </a>
      </div>
    </Modal>
  );
};
