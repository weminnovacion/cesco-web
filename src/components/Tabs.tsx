import { FC, useState, Children } from "react";
import { TabsProps } from "@/model/tabsProps.model";

export const Tabs: FC<TabsProps> = ({ children }) => {
  const [activeIndex, setActiveIndex] = useState(0);

  const titles = Children.map(children, (child) => {
    const { title } = child.props;
    return title;
  });

  return (
    <div className="w-full">
      <div className="flex border-b border-zinc-200">
        {titles.map((title, index) => (
          <button
            key={title}
            className={`text-sm ${
              activeIndex === index
                ? "text-black border-b-2 border-black font-semibold"
                : "text-zinc-500 hover:text-zinc-700 font-medium"
            } px-4 py-2 focus:outline-none`}
            onClick={() => setActiveIndex(index)}
          >
            {title}
          </button>
        ))}
      </div>
      <div className="mt-4">{children[activeIndex].props.children}</div>
    </div>
  );
};
