import { FC, useContext, useEffect, useState } from "react";
import { RequestStatus } from "@/model/request-status";
import { setAuthToken } from "@/utils/api";
import { AuthContext } from "@/contexts/AuthContext";
import { Modal } from "@/components/Modal";
import axios from "axios";
import classNames from "classnames";
import { useForm } from "react-hook-form";
import { API_URL } from "../../../constants";
import Link from "next/link";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";

type SignInFormProps = {
  handleStateForm: (status: RequestStatus, message?: string) => void;
};

export const SignUpForm: FC<SignInFormProps> = ({ handleStateForm }) => {
  const router = useRouter();

  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
  } = useForm<RegisterFormInput>();
  const [errorResponse, setErrorResponse] = useState("");
  const [Indentifications, setIdentifications] = useState<Identification[]>([]);
  const [profession, setProfession] = useState<Profession[]>([]);
  const [status, setStatus] = useState<RequestStatus>("init");
  const { login } = useContext(AuthContext);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleToggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const getTypeIdentification = async () => {
    try {
      let data = {
        pageSize: 10,
        pageNumber: 1,
      };
      const response = await axios.get(API_URL + "typeIndentification/page", {
        data,
      });
      setIdentifications(response.data?.Data);
    } catch (error) {}
  };

  const getTypeProfession = async () => {
    try {
      const response = await axios.get(API_URL + "profession/page");
      setProfession(response.data?.Data?.Content);
    } catch (error) {}
  };

  const onSubmit = handleSubmit(async (data) => {
    try {
      setStatus("loading");
      const response = await axios.post(API_URL + "oauth/signout", data);
      if (response.status == 200) {
        if (response.data?.Status == 200) {
          setStatus("success");
          handleToggleModal();
        } else {
          setStatus("error");
          setTimeout(() => {
            setStatus("init");
          }, 3000);
          response.data?.Data.map((e: any, i: number) => {
            setError(e.Key, {
              message: e.Messages[0],
            });
          });

          setErrorResponse(response.data.Message);
        }
      }
    } catch (error) {
      console.error(error);
    }
  });

  useEffect(() => {
    const fetchData = async () => {
      setStatus("loading");
      try {
        const promiseOne = getTypeIdentification();
        const promiseTwo = getTypeProfession();
        await Promise.all([promiseOne, promiseTwo]);
        setStatus("success");
      } catch (error) {
        setStatus("error");
        setTimeout(() => {
          setStatus("init");
        }, 3000);
        console.error(error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    handleStateForm(status);
  }, [status]);

  return (
    <>
      <form onSubmit={onSubmit}>
        <div
          id="alert-border-2"
          className={classNames(
            " p-4 mb-4 text-red-800 border-t-4 border-red-300 bg-red-50 ",
            { hidden: !errorResponse, flex: errorResponse }
          )}
          role="alert"
        >
          <svg
            className="flex-shrink-0 w-5 h-5"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
              clipRule="evenodd"
            />
          </svg>
          <div className="ml-3 text-sm font-medium">{errorResponse}</div>
          <button
            onClick={() => setErrorResponse("")}
            type="button"
            className="ml-auto -mx-1.5 -my-1.5 bg-red-50 text-red-500 rounded-lg focus:ring-2 focus:ring-red-400 p-1.5 hover:bg-red-200 inline-flex h-8 w-8"
            data-dismiss-target="#alert-border-2"
            aria-label="Close"
          >
            <span className="sr-only">Dismiss</span>
            <svg
              aria-hidden="true"
              className="w-5 h-5"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>
        </div>
        <div className="row">
          <div className="col-12 col-md-6 mb-5">
            <label
              htmlFor="IdTypeIdentification"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Tipo de identificación
            </label>
            <select
              id="IdTypeIdentification"
              className={classNames("form-control", {
                invalid: errors.IdTypeIdentification,
              })}
              disabled={status === "loading"}
              {...register("IdTypeIdentification", {
                required: "Este campos es obligatorio",
              })}
            >
              <option value="">Seleccione...</option>
              {Indentifications.map((item) => (
                <option key={item.Id} value={item.Id}>
                  {item.Name}
                </option>
              ))}
            </select>
            {errors.IdTypeIdentification && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.IdTypeIdentification.message}
              </span>
            )}
          </div>
          <div className="col-12 col-md-6 mb-5">
            <label
              htmlFor="Identification"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Identificación
            </label>
            <input
              type="text"
              id="Identification"
              className={classNames("form-control", {
                invalid: errors.Identification,
              })}
              placeholder="000-000-0000"
              disabled={status === "loading"}
              {...register("Identification", {
                required: "Este campos es obligatorio",
                pattern: {
                  value: /^[0-9]+$/,
                  message: "Solo se permiten números en este campo",
                },
                minLength: 7,
                maxLength: 10,
              })}
            />
            {errors.Identification && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.Identification.message}
              </span>
            )}
          </div>
          <div className="col-12 col-md-6 mb-5">
            <label
              htmlFor="FirstName"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Nombres
            </label>
            <input
              type="text"
              id="FirstName"
              disabled={status === "loading"}
              className={classNames("form-control", {
                invalid: errors.FirstName,
              })}
              placeholder="Nombres"
              {...register("FirstName", {
                required: "Este campos es obligatorio",
              })}
            />
            {errors.FirstName && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.FirstName.message}
              </span>
            )}
          </div>
          <div className="col-12 col-md-6 mb-5">
            <label
              htmlFor="Surname"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Apellidos
            </label>
            <input
              type="text"
              id="Surname"
              disabled={status === "loading"}
              className={classNames("form-control", {
                invalid: errors.Surname,
              })}
              placeholder="Apellidos"
              {...register("Surname", {
                required: "Este campos es obligatorio",
              })}
            />
            {errors.Surname && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.Surname.message}
              </span>
            )}
          </div>
          <div className="col-12 col-md-3 mb-5">
            <label
              htmlFor="IdProfession"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Profesión
            </label>
            <select
              id="IdProfession"
              className={classNames("form-control", {
                invalid: errors.IdProfession,
              })}
              disabled={status === "loading"}
              {...register("IdProfession", {
                required: "Este campos es obligatorio",
              })}
            >
              <option value="">Seleccione...</option>
              {profession.map((item) => (
                <option key={item.Id} value={item.Id}>
                  {item.Name}
                </option>
              ))}
            </select>
            {errors.IdProfession && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.IdProfession.message}
              </span>
            )}
          </div>
          <div className="col-12 col-md-3 mb-5">
            <label
              htmlFor="Phone"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Teléfono
            </label>
            <input
              type="tel"
              id="Phone"
              className={classNames("form-control", { invalid: errors.Phone })}
              placeholder="000-000-0000"
              disabled={status === "loading"}
              {...register("Phone", {
                required: "Este campos es obligatorio",
                pattern: {
                  value: /^3[0-9]{2}[-\s]?[0-9]{3}[-\s]?[0-9]{4}$/,
                  message: "Ingresa un número de teléfono válido",
                },
                maxLength: 10,
                minLength: 10,
              })}
            />
            {errors.Phone && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.Phone.message}
              </span>
            )}
          </div>
          <div className="col-12 col-md-6 mb-5">
            <label
              htmlFor="Email"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Correo electrónico
            </label>
            <input
              type="email"
              id="Email"
              className={classNames("form-control", { invalid: errors.Email })}
              placeholder="name@example.com"
              disabled={status === "loading"}
              {...register("Email", {
                required: "Este campos es obligatorio",
                pattern: {
                  value: /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i,
                  message: "Ingresa un correo electrónico válido",
                },
              })}
            />
            {errors.Email && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.Email.message}
              </span>
            )}
          </div>
          <div className="col-12">
            <button type="submit" className="btn btn-primary-green w-full mb-5">
              {status === "loading" ? (
                <span>Loading...</span>
              ) : (
                <span>Crear cuenta</span>
              )}
            </button>
          </div>
        </div>
      </form>

      <Modal isOpen={isModalOpen} onClose={handleToggleModal} size="max-w-3xl">
        <div className="max-w-xl mx-auto my-10">
          <div className="row">
            <div className="col-12 mb-5">
              <h3 className="text-3xl text-center text-zinc-700 font-bold mb-5">
                Registro exitoso
              </h3>
              <div className="col-auto mx-auto mb-5">
                <FontAwesomeIcon
                  className="text-7xl text-primary-green mb-5"
                  icon={faCircleCheck}
                />
              </div>
              <p className="text-base text-center text-zinc-700">
                Estamos encantados de que te hayas registrado en nuestra
                plataforma. Aquí encontrarás un espacio donde podrás explorar,
                aprender y conectarte con una comunidad apasionada como la
                nuestra.
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-md-4 mx-auto">
              <Link
                href={"/auth/sign-in"}
                type="button"
                className="btn btn-primary-green w-full mb-5"
              >
                Comencemos
              </Link>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};
