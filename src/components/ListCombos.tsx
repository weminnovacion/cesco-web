import { useContext, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Combo } from "./combo";
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";
import { ComboProps } from "@/model/comboProps.model";
import { RequestStatus } from "@/model/request-status";
import {
  faBookBookmark,
  faCartShopping,
  faInfo,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { parseCookies } from "nookies";
import { CartContext } from "@/contexts/CartContext";
import { CartItem } from "@/model/cart.model";
import { API_URL } from "../../constants";

export const ListCombos = () => {
  const { currencyWithSuffixFormatter } = useCurrencyFormatter();

  const [status, setStatus] = useState<RequestStatus>("init");
  const [combos, setCombos] = useState<ComboProps[]>([]);
  const router = useRouter();
  const { addToCart } = useContext(CartContext);

  const handleAddToCart = (combo: ComboProps) => {
    const item: CartItem = { ...combo, type: "combo" };

    addToCart(item);
    router.push("/cart");
  };

  useEffect(() => {
    setStatus("loading");
    getCombos()
      .then((res) => {
        setCombos(res);
        setStatus("success");
      })
      .catch((error) => {
        setStatus("error");
      });
  }, []);

  const getCombos = async (): Promise<ComboProps[]> => {
    const cookies = parseCookies();
    const token = cookies.token;
    try {
      const config = {
        headers: {
          Authorization: token,
        },
      };
      const response = await axios.get(API_URL + "coursePackage/page", config);
      const combosData = response.data.Data.Content;
      return combosData;
    } catch (error) {
      throw error;
    }
  };

  return (
    <>
      <div className="row">
        {status === "success" && combos?.length && (
          <>
            {combos?.map((combo, index) => (
              <div className="col-md-6 col-lg-3 mb-4" key={index}>
                <Combo {...combo}>
                  <button
                    type="button"
                    className="btn btn-primary-green w-full flex items-center text-sm"
                    onClick={() => {
                      handleAddToCart(combo);
                    }}
                  >
                    {combo.Price ? (
                      <>
                        <div className="w-4 h-4 flex justify-center items-center">
                          <FontAwesomeIcon icon={faCartShopping} />
                        </div>
                        <span className="mx-auto text-sm">
                          Comprar {currencyWithSuffixFormatter(combo.Price)}
                        </span>
                      </>
                    ) : (
                      <span className="mx-auto text-xs">Obtener ahora</span>
                    )}
                  </button>
                </Combo>
              </div>
            ))}
          </>
        )}
        {status === "loading" &&
          [0, 1, 2, 3].map((item) => (
            <>
              <div
                className="col-md-6 col-lg-3 mb-5"
                key={"card-loading-" + item}
              >
                <div
                  role="status"
                  className="flex items-center justify-center h-40 max-w-sm bg-zinc-300 rounded animate-pulse mb-4"
                >
                  <svg
                    className="w-12 h-12 text-zinc-200"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    fill="currentColor"
                    viewBox="0 0 384 512"
                  >
                    <path d="M361 215C375.3 223.8 384 239.3 384 256C384 272.7 375.3 288.2 361 296.1L73.03 472.1C58.21 482 39.66 482.4 24.52 473.9C9.377 465.4 0 449.4 0 432V80C0 62.64 9.377 46.63 24.52 38.13C39.66 29.64 58.21 29.99 73.03 39.04L361 215z" />
                  </svg>
                  <span className="sr-only">Loading...</span>
                </div>
                <div role="status" className="max-w-sm animate-pulse mb-4">
                  <div className="h-2.5 bg-zinc-300 rounded w-48 mb-4" />
                  <div className="h-2 bg-zinc-300 rounded max-w-[360px] mb-2.5" />
                  <div className="h-2 bg-zinc-300 rounded mb-2.5" />
                  <div className="h-2 bg-zinc-300 rounded max-w-[330px] mb-2.5" />
                  <div className="h-2 bg-zinc-300 rounded max-w-[300px] mb-2.5" />
                  <div className="h-2 bg-zinc-300 rounded max-w-[360px]" />
                  <span className="sr-only">Loading...</span>
                </div>
                <div role="status" className="max-w-sm animate-pulse mb-4">
                  <div className="h-10 bg-zinc-300 rounded w-full mb-4" />
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            </>
          ))}
        {(status === "error" && !combos.length) ||
          (status === "success" && !combos.length && (
            <>
              <div className="col-12">
                <h1 className="text-zinc-800 font-semibold text-2xl mb-5">
                  <FontAwesomeIcon icon={faBookBookmark} className="mr-2" />
                  Cursos
                </h1>
              </div>
              <div className="col-12">
                <div className="bg-white p-5 rounded">
                  <div className="row">
                    <div className="col-auto">
                      <div className="w-12 h-12 flex justify-center items-center bg-zinc-100 text-blue-400 text-lg rounded-full">
                        <FontAwesomeIcon icon={faInfo} />
                      </div>
                    </div>
                    <div className="col">
                      <h3 className="text-zinc-800 font-semibold text-lg">
                        No hay más cursos para mostrar
                      </h3>
                      <p className="text-zinc-500 text-base">
                        Queremos seguir creando nuevos cursos para ti.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ))}
      </div>
    </>
  );
};
