import { useContext, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { AuthContext } from "@/contexts/AuthContext";
import { CartContext } from "@/contexts/CartContext";
import { Drawer } from "@/components/Drawer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartShopping,
  faBars,
  faSearch,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames";
import { ContactInformation } from "./ContactInformation";
import { useInitials } from "@/hooks/useInitials";
import { SearchComponent } from "./Search";
import { CookieConsentBanner } from "./cookieConsentBanner";

export const Header = () => {
  const router = useRouter();

  const { isLoggedIn, logout, userInfo } = useContext(AuthContext);
  const { courses, combos } = useContext(CartContext);
  const totalItems = courses.length + combos.length;
  const [isUserMenuVisible, setIsUserMenuVisible] = useState(false);
  const [isOpenDrawer, setIsOpenDrawer] = useState(false);
  const [isFieldSearch, setisFieldSearch] = useState(false);
  const initials = useInitials(userInfo?.FirstName ? userInfo?.FirstName : "");

  const handleToggleUserMenu = () => {
    setIsUserMenuVisible(!isUserMenuVisible);
  };

  const handleToggleDrawer = () => {
    setIsOpenDrawer(!isOpenDrawer);
  };

  const handleToggleFieldSearch = () => {
    setisFieldSearch(!isFieldSearch);
  };

  const handleLogout = async () => {
    logout();
    router.push("/");
  };

  return (
    <>
      <header id="site-header" className="sticky top-0 z-50" role="banner">
        <nav className="navbar navbar-primary" role="navigation">
          <div className="container">
            <div
              className={classNames("w-full", {
                row: isFieldSearch,
                hidden: !isFieldSearch,
              })}
            >
              <div className="relative w-full">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5 text-zinc-500 "
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    ></path>
                  </svg>
                </div>
                <input
                  type="search"
                  id="search"
                  className="navbar-search"
                  placeholder="Buscar cursos..."
                  required
                />
                <button
                  onClick={handleToggleFieldSearch}
                  className=" absolute right-3 top-1/2 -translate-y-1/2 w-8 h-8 flex justify-center items-center text-zinc-500"
                >
                  <FontAwesomeIcon icon={faTimes} />
                </button>
              </div>
            </div>
            <div
              className={classNames("w-full", {
                row: !isFieldSearch,
                hidden: isFieldSearch,
              })}
            >
              <div className="col-auto flex items-center">
                <button
                  type="button"
                  onClick={handleToggleDrawer}
                  className=" w-10 h-10 text-white flex md:hidden justify-center items-center hover:ring hover:ring-primary-green focus:ring-primary-green focus-within:ring-primary-green mr-2"
                >
                  <FontAwesomeIcon icon={faBars} />
                </button>
                <Link href="/" className="logo font-bold">
                  CESCO
                </Link>
              </div>
              <div className="col-auto hidden lg:flex items-center">
                <nav
                  className=" w-full flex md:w-auto"
                  id="navbar-default"
                  role="navigation"
                >
                  <ul className="font-medium flex flex-col p-4 md:p-0 md:flex-row md:space-x-8 md:mt-0 md:border-0">
                    <li>
                      <Link
                        href="/courses"
                        className="navbar-item "
                        aria-current="page"
                      >
                        Cursos
                      </Link>
                    </li>
                    <li>
                      <Link
                        href="/courses/combos"
                        className="navbar-item "
                        aria-current="page"
                      >
                        Combos
                      </Link>
                    </li>
                    <li>
                      <Link href="/about" className="navbar-item">
                        Nosotros
                      </Link>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="col md:flex hidden items-center">
                <SearchComponent />
              </div>
              <div className="col-auto ml-auto flex items-center">
                <div className="row">
                  <div className="col-auto md:hidden block">
                    <button
                      onClick={handleToggleFieldSearch}
                      className="btn btn-primary-green btn-small btn-flat shrink-0  relative"
                      type="button"
                    >
                      <div className="w-6 h-6 text-lg">
                        <FontAwesomeIcon icon={faSearch} />
                      </div>
                    </button>
                  </div>
                  {isLoggedIn ? (
                    <>
                      <div className="col-auto">
                        <Link
                          href={"/cart"}
                          className="btn btn-primary-green btn-small btn-flat shrink-0  relative"
                          type="button"
                        >
                          <div className="w-6 h-6 text-lg">
                            <FontAwesomeIcon icon={faCartShopping} />
                          </div>
                          <div
                            className={classNames(
                              "absolute items-center justify-center w-5 h-5 text-[0.5rem] font-bold text-white bg-red-600 rounded-full -top-1 right-0 ",
                              {
                                "inline-flex": totalItems,
                                hidden: !totalItems,
                              }
                            )}
                          >
                            {totalItems}
                          </div>
                        </Link>
                      </div>
                      <div className="col-auto relative">
                        <button
                          onClick={handleToggleUserMenu}
                          type="button"
                          className="inline-flex items-center justify-center w-9 h-9 overflow-hidden bg-zinc-100 rounded-full "
                        >
                          {userInfo?.Photo ? (
                            <img
                              src={userInfo?.Photo}
                              alt="Preview"
                              className="w-full h-full bg-cover object-cover bg-center"
                            />
                          ) : (
                            <span className="font-semibold text-zinc-600">
                              {initials}
                            </span>
                          )}
                        </button>
                        <div
                          id="userDropdown"
                          className={classNames(
                            "absolute top-12 right-4 z-10 bg-white divide-y divide-zinc-100 rounded shadow w-44",
                            {
                              hidden: !isUserMenuVisible,
                              block: isUserMenuVisible,
                            }
                          )}
                        >
                          <div className="px-4 py-3 text-sm text-zinc-900">
                            <div>{userInfo?.Username}</div>
                            <div
                              className="font-semibold truncate"
                              title={userInfo?.Email}
                            >
                              {userInfo?.Email}
                            </div>
                          </div>
                          <ul
                            className="py-2 text-sm text-zinc-800 "
                            aria-labelledby="avatarButton"
                          >
                            <li>
                              <Link
                                href={`/profile/${userInfo?.Username}`}
                                className="block px-4 py-2 hover:bg-zinc-100"
                              >
                                Perfil
                              </Link>
                            </li>
                            <li>
                              <Link
                                href="/my-courses"
                                className="block px-4 py-2 hover:bg-zinc-100"
                              >
                                Mis Cursos
                              </Link>
                            </li>
                            <li>
                              <Link
                                href={`/profile/${userInfo?.Username}/settings`}
                                className="block px-4 py-2 hover:bg-zinc-100"
                              >
                                Configuración
                              </Link>
                            </li>
                          </ul>
                          <div className="py-1">
                            <a
                              role="button"
                              onClick={handleLogout}
                              className="block px-4 py-2 text-sm text-zinc-800 hover:bg-zinc-100"
                            >
                              Cerrar sesión
                            </a>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : (
                    // Elementos que se muestran cuando el usuario no está autenticado
                    <>
                      <div className="col-auto">
                        <Link
                          href={"/cart"}
                          className="btn btn-primary-green btn-small btn-flat shrink-0  relative"
                          type="button"
                        >
                          <div className="w-6 h-6 text-lg">
                            <FontAwesomeIcon icon={faCartShopping} />
                          </div>
                          <div
                            className={classNames(
                              "absolute items-center justify-center w-5 h-5 text-[0.5rem] font-bold text-white bg-red-600 rounded-full -top-1 right-0 ",
                              {
                                "inline-flex": totalItems,
                                hidden: !totalItems,
                              }
                            )}
                          >
                            {totalItems}
                          </div>
                        </Link>
                      </div>
                      <div className="col-auto hidden md:block">
                        <Link
                          href={"/auth/sign-in"}
                          className="btn btn-primary-green btn-small btn-flat shrink-0"
                          type="button"
                        >
                          Entrar
                        </Link>
                      </div>
                      <div className="col-auto hidden md:block">
                        <Link
                          href={"/auth/sign-up"}
                          className="btn btn-primary-green btn-small shrink-0"
                          type="button"
                        >
                          Crear cuenta
                        </Link>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </nav>
      </header>
      <ContactInformation />
      <Drawer
        isOpenDrawer={isOpenDrawer}
        handleToggleDrawer={handleToggleDrawer}
      />
      <CookieConsentBanner />
    </>
  );
};
