import { useBackdropContext } from "@/contexts/BackdropContext";

export const Backdrop: React.FC = () => {
  const { isBackdropOpen } = useBackdropContext();

  return isBackdropOpen ? (
    <div
      drawer-backdrop=""
      className="bg-zinc-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-30"
    ></div>
  ) : null;
};
