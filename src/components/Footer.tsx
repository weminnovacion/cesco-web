import {
  faFacebook,
  faInstagram,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const Footer = () => {
  return (
    <>
      <footer className="p-3 md:p-8 bg-primary-green/5">
        <div className="container">
          <div className="row mb-5">
            <div className="col-12 col-md-4">
              <h2 className="text-2xl font-bold text-primary-green mb-4">
                CESCO
              </h2>
              <p className="text-sm text-zinc-500 mb-4">
                En CESCO ofrecemos los cursos y diplomados de formación
                continua, dirigidos al personal de la salud en Colombia, bajo la
                resolución 3100 del 2019.
              </p>
              <div className="row mb-5">
                <div className="col-auto">
                  <a
                    href="https://www.facebook.com/profile.php?id=100090679791262&mibextid=ZbWKwL"
                    target="_blank"
                    className="w-5 h-5 flex justify-center items-center text-primary-green"
                  >
                    <FontAwesomeIcon icon={faFacebook} />
                  </a>
                </div>
                <div className="col-auto">
                  <a
                    href="https://www.instagram.com/cescoeducacion"
                    target="_blank"
                    className="w-5 h-5 flex justify-center items-center text-primary-green"
                  >
                    <FontAwesomeIcon icon={faInstagram} />
                  </a>
                </div>
                <div className="col-auto">
                  <a
                    href="https://www.youtube.com/channel/UCHLdz2BBalec8NwbwLjZ-3g"
                    target="_blank"
                    className="w-5 h-5 flex justify-center items-center text-primary-green"
                  >
                    <FontAwesomeIcon icon={faYoutube} />
                  </a>
                </div>
              </div>
            </div>
            <div className="col-auto">
              <div className="row">
                <div className="col">
                  <p className="font-bold text-primary-green ml-4 mb-4">
                    Legal
                  </p>
                  <ul className="flex flex-wrap">
                    <li className="font-normal text-zinc-500 mb-4 mx-4">
                      <a href="#" className="hover:underline">
                        Términos y condiciones
                      </a>
                    </li>
                    <li className="font-normal text-zinc-500 mb-4 mx-4">
                      <a href="#" className="hover:underline">
                        Privacidad
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          {/* <div className="row mb-5">
            <div className="col-12 col-md-8 mx-auto">
              <form >   
                <label htmlFor="default-search" className="mb-3 text-sm font-semibold text-primary-green ">Suscríbete a nuestro boletín de noticias:</label>
                <div className="relative">
                    <input type="search" id="default-search" className="form-control pr-28" placeholder="Ingresa tu correo electrónico" required/>
                    <button type="submit" className="btn btn-small btn-primary-green absolute right-2.5 top-1/2 -translate-y-1/2 ">Suscríbete</button>
                </div>
              </form>
            </div>
          </div> */}
          <div className="row">
            <div className="col-12">
              <div className="w-full border-t"></div>
            </div>
            <div className="col-auto mx-auto text-center text-zinc-500 py-5">
              © 2023 Todos los derechos reservados
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};
