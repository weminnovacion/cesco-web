//Next
import Link from "next/link";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faHomeAlt } from "@fortawesome/free-solid-svg-icons";

export const Breadcrumbs = ({ items }: BreadcrumbsProps) => {
  return (
    <nav className="text-sm font-medium">
      <ol className="list-none p-0 inline-flex">
        <li>
          <Link href={"/"} className="text-primary-green text-xs">
            <FontAwesomeIcon icon={faHomeAlt} />
          </Link>
          <span className="mx-2 text-zinc-400 text-xs">
            {" "}
            <FontAwesomeIcon icon={faChevronRight} />{" "}
          </span>
        </li>
        {items.map((item, index) => (
          <li key={index}>
            {index !== 0 && (
              <span className="mx-2 text-zinc-400 text-xs ">
                {" "}
                <FontAwesomeIcon icon={faChevronRight} />{" "}
              </span>
            )}
            <Link
              href={item.url}
              className="text-black text-xs hover:text-primary-green first-letter-uppercase"
            >
              {item.name}
            </Link>
          </li>
        ))}
      </ol>
    </nav>
  );
};
