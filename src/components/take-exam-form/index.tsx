import { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import { Modal } from "@/components/Modal";
import { RequestStatus } from "@/model/request-status";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { parseCookies } from "nookies";
import axios from "axios";
import { API_URL } from "../../../constants";

export const TakeExamForm = () => {
  const router = useRouter();
  const { idExam } = router.query;

  const [status, setStatus] = useState<RequestStatus>("init");
  const [submitForm, setSubmitForm] = useState<boolean>(false);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [currentSelectedAnswer, setCurrentSelectedAnswer] = useState<
    number | null
  >(null);
  const [questions, setQuestions] = useState<Evaluation | null>(null);
  const [timeRemaining, setTimeRemaining] = useState<number | null>(null);
  const [resultQuestion, setResultQuestion] = useState<ResultQuestion>();
  const [selectedAnswers, setSelectedAnswers] = useState<number[]>([]);
  const selectedAnswersRef = useRef(selectedAnswers);
  const progress =
    (currentQuestion / ((questions?.Questions?.length ?? 0) - 1)) * 100;
  const isLastQuestion =
    currentQuestion === (questions?.Questions?.length ?? 0) - 1;

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleToggleModal = () => {
    setIsModalOpen(!isModalOpen);
    router.push("/my-courses");
  };

  const getAssessmentQuestions = async (): Promise<any> => {
    const cookies = parseCookies();
    const token = cookies.token;
    const config = {
      headers: {
        Authorization: token,
      },
    };
    setStatus("loading");
    const response = await axios.get(
      API_URL + "evaluationCourse/getEvaluation?id=" + idExam,
      config
    );
    setStatus("success");
    const evaluationData = response.data.Data;
    return evaluationData;
  };

  const responseEvaluation = async () => {
    const cookies = parseCookies();
    const token = cookies.token;
    if (typeof idExam === "string") {
      const json = {
        IdCourse: parseInt(idExam),
        Answers: selectedAnswers.map((answerId) => ({ Id: answerId })),
      };
      console.log(selectedAnswers);
      const config = {
        headers: {
          Authorization: "Bearer " + token,
        },
      };
      try {
        setStatus("loading");
        const response = await axios.post(
          API_URL + "responseEvaluation/ResponseEvaluation",
          json,
          config
        );
        const resultQuestion: ResultQuestion = {
          Calification: response.data.Data.Calification,
          NameCourse: response.data.Data.NameCourse,
          NumQuetions: response.data.Data.NumQuetions,
          NumQuetionsTrue: response.data.Data.NumQuetionsTrue,
          UrlCertificate: response.data.Data.UrlCertificate,
        };
        setResultQuestion(resultQuestion);
        setIsModalOpen(true);
        if (response.data.Status == 200) {
          setStatus("success");
        }
      } catch (error) {
        setStatus("error");
      }
    }
  };

  const handleNextQuestion = () => {
    if (questions === null) {
      return;
    }

    if (currentSelectedAnswer !== null) {
      const updatedSelectedAnswers = [...selectedAnswers];
      updatedSelectedAnswers[currentQuestion] = currentSelectedAnswer;
      setSelectedAnswers(updatedSelectedAnswers);
    }

    const nextQuestion = (currentQuestion + 1) % questions.Questions.length;
    setCurrentQuestion(isLastQuestion ? currentQuestion : nextQuestion);
    setCurrentSelectedAnswer(null); // Reinicia la respuesta seleccionada

    if (isLastQuestion && isNextButtonEnabled()) {
      setSubmitForm(true);
    }
  };

  const handlePreviousQuestion = () => {
    if (questions === null) {
      return;
    }
    if (currentQuestion === 0) {
      return;
    }
    setCurrentQuestion(
      (currentQuestion - 1 + questions.Questions.length) %
        questions.Questions.length
    );
  };

  const handleAnswerSelect = (answerId: number) => {
    setCurrentSelectedAnswer(answerId);
  };

  const isNextButtonEnabled = () => {
    return currentSelectedAnswer !== null;
  };

  useEffect(() => {
    if (idExam) {
      getAssessmentQuestions().then((response) => {
        setQuestions(response);
        setTimeRemaining(30 * 60);
      });
    }
  }, [idExam]);

  useEffect(() => {
    if (timeRemaining === null) {
      return; // Don't start the timer until timeRemaining is set
    }

    const timer = setInterval(() => {
      setTimeRemaining((prevTime) => (prevTime !== null ? prevTime - 1 : null));
    }, 1000);

    if (timeRemaining === 0) {
      // Se acabó el tiempo, realiza alguna acción aquí
      clearInterval(timer);
      router.push("/courses/exam/" + idExam);
    }

    return () => {
      clearInterval(timer);
    };
  }, [timeRemaining]);

  useEffect(() => {
    selectedAnswersRef.current = selectedAnswers;
    console.log(selectedAnswersRef);
  }, [selectedAnswers]);

  return (
    <>
      {status === "success" && (
        <>
          <div className="container py-10">
            <div className="row">
              <div className="col-12">
                <div className="row">
                  <div className="col">
                    <p className="text-lg text-zinc-700 font-bold mb-4">
                      {questions?.NameCourse}
                    </p>
                  </div>
                  <div className="col-auto ml-auto">
                    <span className="text-zinc-500">Pregunta </span>
                    <span className="text-zinc-800 ">
                      {currentQuestion + 1} de {questions?.Questions.length}{" "}
                    </span>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="w-full bg-gray-200 rounded-full h-2.5 ">
                      <div
                        className="bg-primary-green h-2.5 rounded-full transform duration-200"
                        style={{ width: `${progress}%` }}
                      ></div>
                    </div>
                    <hr className="my-4" />
                  </div>
                </div>
                <>
                  <div className="row">
                    <div className="col-12">
                      <p className="text-xl text-zinc-700 font-bold mb-4">
                        {questions?.Questions[currentQuestion].Question}
                      </p>
                    </div>
                    <div className="col-12">
                      <ul className="w-full mb-10">
                        {questions?.Questions[currentQuestion].Dets.map(
                          (option) => (
                            <>
                              <li
                                className="mb-3"
                                key={"item-question-" + option.Id}
                              >
                                <input
                                  type="radio"
                                  id={"question-" + option.Id}
                                  name={`question-${questions?.Questions[currentQuestion].Id}`}
                                  value={option.Id}
                                  onChange={() => handleAnswerSelect(option.Id)}
                                  className="hidden peer"
                                />
                                <label
                                  htmlFor={"question-" + option.Id}
                                  className="inline-flex items-center w-full px-5 py-3 text-zinc-500 bg-white border border-gray-200 rounded cursor-pointer peer-checked:border-primary-green peer-checked:text-primaborder-primary-green hover:text-gray-600 hover:bg-gray-100 group"
                                >
                                  <div className="block">
                                    <div className="w-full text-lg font-semibold">
                                      {option.Answer}
                                    </div>
                                  </div>
                                </label>
                              </li>
                            </>
                          )
                        )}
                      </ul>
                    </div>
                  </div>
                </>
              </div>
            </div>
          </div>

          <div className="bg-zinc-100 w-full p-4">
            <div className="container">
              {!submitForm && (
                <div className="row justify-between">
                  <div className="col-auto">
                    <button
                      className="btn btn-primary-green flex items-center"
                      type="button"
                      onClick={handlePreviousQuestion}
                      disabled={currentQuestion === 0}
                    >
                      Anterior
                    </button>
                  </div>
                  <div className="col">
                    <div className="flex h-full w-full items-center">
                      {timeRemaining !== null && (
                        <p className="text-center text-zinc-500 w-full flex items-center justify-center">
                          {" "}
                          <FontAwesomeIcon
                            icon={faClock}
                            className="mr-2"
                          />{" "}
                          {Math.floor(timeRemaining / 60)}:
                          {(timeRemaining % 60).toString().padStart(2, "0")}
                        </p>
                      )}
                    </div>
                  </div>
                  <div className="col-auto">
                    <button
                      className="btn btn-primary-green flex items-center"
                      type="button"
                      onClick={handleNextQuestion}
                      disabled={!isNextButtonEnabled()}
                    >
                      {isLastQuestion && isNextButtonEnabled() ? (
                        <>Terminar</>
                      ) : (
                        <>Siguiente pregunta</>
                      )}
                    </button>
                  </div>
                </div>
              )}
              {submitForm && (
                <div className="row justify-between">
                  <div className="col-auto">
                    <button
                      className="btn btn-primary-green flex items-center"
                      type="button"
                      onClick={() => {
                        setSubmitForm(false);
                      }}
                    >
                      Volver
                    </button>
                  </div>
                  <div className="col-auto">
                    <button
                      className="btn btn-primary-green flex items-center"
                      type="button"
                      onClick={responseEvaluation}
                    >
                      Finalizar
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </>
      )}

      {status === "loading" && (
        <div className="container my-10">
          <div className="row">
            <div className="col">
              <div role="status" className="animate-pulse">
                <div className="h-7 bg-gray-300 rounded w-full mb-4"></div>
                <span className="sr-only">Loading...</span>
              </div>
            </div>
            <div className="col-auto">
              <div role="status" className="animate-pulse">
                <div className="h-5 bg-gray-100 rounded w-full mb-4"></div>
                <span className="sr-only">Loading...</span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div role="status" className="animate-pulse">
                <div className="h-5 bg-gray-200 rounded-full w-full mb-4"></div>
                <span className="sr-only">Loading...</span>
              </div>
              <hr className="my-4" />
            </div>
            <div className="col-12">
              <div role="status" className="animate-pulse">
                <div className="h-7 bg-gray-200 rounded w-full mb-4"></div>
                <span className="sr-only">Loading...</span>
              </div>
            </div>
            <div className="col-12">
              <div role="status" className="animate-pulse">
                <div className="h-14 bg-gray-300 rounded w-full mb-3"></div>
                <div className="h-14 bg-gray-300 rounded w-full mb-3"></div>
                <div className="h-14 bg-gray-300 rounded w-full mb-3"></div>
                <div className="h-14 bg-gray-300 rounded w-full mb-3"></div>
                <span className="sr-only">Loading...</span>
              </div>
            </div>
          </div>
        </div>
      )}

      <Modal isOpen={isModalOpen} onClose={handleToggleModal} size="max-w-3xl">
        <div className="lg:p-8 p-5">
          <div className="row">
            <div className="col-12">
              <h3 className="text-zinc-700 font-semibold text-xl mb-10">
                {resultQuestion?.NameCourse}
              </h3>
            </div>
          </div>
          {!resultQuestion?.UrlCertificate && (
            <div className="row mb-10">
              <div className="col">
                <h2 className=" text-zinc-700 font-semibold text-3xl mb-2">
                  ¡No te rindas!
                </h2>
                <p className="text-lg text-zinc-500">
                  Necesitas una{" "}
                  <span className="text-zinc-700 font-semibold">
                    calificación mínima de 8.0
                  </span>{" "}
                  para aprobar.
                </p>
              </div>
              <div className="col-auto">
                <div className="bg-zinc-100 p-4 rounded-xl flex items-center">
                  <div className="px-7 border-r border-zinc-300">
                    <h3 className="text-zinc-700 text-5xl font-semibold">
                      {resultQuestion?.Calification}
                    </h3>
                    <p>Calificación</p>
                  </div>
                  <div className="px-7">
                    <h3 className="text-zinc-700 text-3xl font-semibold">
                      {resultQuestion?.NumQuetionsTrue}/
                      {resultQuestion?.NumQuetions}
                    </h3>
                    <p>Aciertos</p>
                  </div>
                </div>
              </div>
            </div>
          )}
          {resultQuestion?.UrlCertificate && (
            <div className="row mb-10">
              <div className="col">
                <h2 className=" text-zinc-700 font-semibold text-3xl mb-2">
                  ¡Felicitaciones!
                </h2>
                <p className="text-lg text-zinc-500">
                  Necesitas una{" "}
                  <span className="text-zinc-700 font-semibold">
                    calificación mínima de 8.0
                  </span>{" "}
                  para aprobar.
                </p>
              </div>
              <div className="col-auto">
                <div className="bg-zinc-100 p-4 rounded-xl flex items-center">
                  <div className="px-7 border-r border-zinc-300">
                    <h3 className="text-zinc-700 text-5xl font-semibold">
                      {resultQuestion?.Calification}
                    </h3>
                    <p>Calificación</p>
                  </div>
                  <div className="px-7">
                    <h3 className="text-zinc-700 text-3xl font-semibold">
                      {resultQuestion?.NumQuetionsTrue}/
                      {resultQuestion?.NumQuetions}
                    </h3>
                    <p>Aciertos</p>
                  </div>
                </div>
              </div>
            </div>
          )}
          <div className="row">
            <div className="col-12">
              <button
                type="button"
                className="btn btn-primary-green w-full"
                onClick={handleToggleModal}
              >
                Continuar
              </button>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};
