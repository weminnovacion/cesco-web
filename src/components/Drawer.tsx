//React
import { FC, useContext } from "react";
//Next
import Link from "next/link";
//Context
import { AuthContext } from "@/contexts/AuthContext";
import { CartContext } from "@/contexts/CartContext";
//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faRightToBracket,
  faUserPlus,
  faGraduationCap,
  faBookmark,
  faAddressCard,
  faRightFromBracket,
  faTimes,
  faCartShopping,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames";
import { useInitials } from "@/hooks/useInitials";

export const Drawer: FC<{
  isOpenDrawer: boolean;
  handleToggleDrawer: () => void;
}> = ({ isOpenDrawer, handleToggleDrawer }) => {
  const { isLoggedIn, logout, userInfo } = useContext(AuthContext);
  const { courses, combos } = useContext(CartContext);
  const totalItems = courses.length + combos.length;
  const initials = useInitials(userInfo?.Username ? userInfo?.Username : "");

  return (
    <>
      <aside
        id="sidebar-user"
        className={classNames(
          "fixed top-0 left-0 z-50 w-64 h-screen transition-transform ",
          {
            "-translate-x-0": isOpenDrawer,
            "-translate-x-full": !isOpenDrawer,
          }
        )}
        aria-label="Sidebar"
      >
        <div className="overflow-y-auto py-4 px-3 h-full bg-white border-r border-gray-200">
          <div className="text-center text-gray-500">
            <div className="w-100 flex justify-between items-center pl-2 pr-2 pb-2">
              <a
                href="/"
                className="text-lg text-zinc-500 inline-flex font-bold"
              >
                CESCO
              </a>
              <button
                type="button"
                onClick={handleToggleDrawer}
                className="w-8 h-8 flex justify-center items-center text-gray-500 bg-zinc-100 focus:outline-none focus:ring-4 focus:ring-gray-200 rounded"
              >
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </div>
            {isLoggedIn && (
              <>
                <div className="mx-auto mb-4 w-20 h-20 rounded-full flex justify-center items-center overflow-hidden bg-zinc-100">
                  {userInfo?.Photo && (
                    <img
                      className="w-full h-full bg-cover object-cover bg-center"
                      src={userInfo?.Photo}
                      alt={userInfo?.Username}
                    />
                  )}
                  {!userInfo?.Photo && (
                    <span className="text-5xl font-semibold">{initials}</span>
                  )}
                </div>
                <h3 className="text-xl font-bold tracking-tight text-gray-900">
                  <a href="#">{userInfo?.Username}</a>
                </h3>
                <p className="font-light text-gray-500">{userInfo?.Email}</p>
                <a
                  role="button"
                  onClick={logout}
                  className="inline-flex items-center justify-center w-full py-2.5 px-5 my-5 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded border border-gray-200 hover:bg-gray-100 hover:text-primary-700 focus:z-10 focus:ring-4 focus:ring-gray-200 "
                >
                  <div className="mr-1 w-5 h-5">
                    <FontAwesomeIcon icon={faRightFromBracket} />
                  </div>
                  Cerrar sesión
                </a>
                <ul className="flex justify-center mb-4 space-x-1">
                  <li>
                    <Link
                      href={"/cart"}
                      className="inline-flex text-gray-500 hover:bg-gray-100  focus:outline-none focus:ring-4 focus:ring-gray-200 rounded text-sm relative p-2.5"
                    >
                      <div className="w-6 h-6 text-lg">
                        <FontAwesomeIcon icon={faCartShopping} />
                      </div>
                      <div
                        className={classNames(
                          "absolute items-center justify-center w-5 h-5 text-[0.5rem] font-bold text-white bg-red-600 rounded-full -top-1 right-0 ",
                          {
                            "inline-flex": totalItems,
                            hidden: !totalItems,
                          }
                        )}
                      >
                        {totalItems}
                      </div>
                    </Link>
                  </li>
                  <li>
                    <a
                      href="#"
                      className="inline-flex text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-200 rounded text-sm p-2.5"
                    >
                      <svg
                        className="w-6 h-6"
                        aria-hidden="true"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path d="M10 2a6 6 0 00-6 6v3.586l-.707.707A1 1 0 004 14h12a1 1 0 00.707-1.707L16 11.586V8a6 6 0 00-6-6zM10 18a3 3 0 01-3-3h6a3 3 0 01-3 3z" />
                      </svg>
                    </a>
                  </li>
                  <li>
                    <a
                      href="#"
                      className="inline-flex text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-200 rounded text-sm p-2.5"
                    >
                      <svg
                        className="w-6 h-6"
                        aria-hidden="true"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </a>
                  </li>
                </ul>
              </>
            )}
          </div>
          <ul className="pt-5 mt-5 space-y-2 border-t border-gray-200 ">
            {isLoggedIn && (
              <>
                <li>
                  <Link
                    href={"/profile/" + userInfo?.Username}
                    className="flex items-center p-2 text-base font-normal text-gray-900 rounded hover:bg-gray-100 group"
                  >
                    <div className="w-6 h-6 text-gray-400 transition duration-75 group-hover:text-gray-900">
                      <FontAwesomeIcon icon={faUser} />
                    </div>
                    <span className="ml-3">Ver perfil</span>
                  </Link>
                </li>
              </>
            )}

            {isLoggedIn === false && (
              <>
                <li>
                  <Link
                    href={"/auth/sign-in"}
                    className="flex items-center p-2 text-base font-normal text-gray-900 rounded hover:bg-gray-100 group"
                  >
                    <div className="w-6 h-6 text-gray-400 transition duration-75 group-hover:text-gray-900">
                      <FontAwesomeIcon icon={faRightToBracket} />
                    </div>
                    <span className="ml-3">Entrar</span>
                  </Link>
                </li>

                <li>
                  <Link
                    href={"/auth/sign-up"}
                    className="flex items-center p-2 text-base font-normal text-gray-900 rounded hover:bg-gray-100 group"
                  >
                    <div className="w-6 h-6 text-gray-400 transition duration-75 group-hover:text-gray-900">
                      <FontAwesomeIcon icon={faUserPlus} />
                    </div>
                    <span className="ml-3">Crear cuenta</span>
                  </Link>
                </li>
              </>
            )}
            <li>
              <Link
                href="/courses"
                className="flex items-center p-2 text-base font-normal text-gray-900 rounded hover:bg-gray-100 group"
              >
                <div className="w-6 h-6 text-gray-400 transition duration-75 group-hover:text-gray-900">
                  <FontAwesomeIcon icon={faGraduationCap} />
                </div>
                <span className="ml-3">Cursos</span>
              </Link>
            </li>

            <li>
              <Link
                href="/combos"
                className="flex items-center p-2 text-base font-normal text-gray-900 rounded hover:bg-gray-100 group"
              >
                <div className="w-6 h-6 text-gray-400 transition duration-75 group-hover:text-gray-900">
                  <FontAwesomeIcon icon={faBookmark} />
                </div>
                <span className="ml-3">Combos</span>
              </Link>
            </li>
            <li>
              <Link
                href="/about"
                className="flex items-center p-2 text-base font-normal text-gray-900 rounded hover:bg-gray-100 group"
              >
                <div className="w-6 h-6 text-gray-400 transition duration-75 group-hover:text-gray-900">
                  <FontAwesomeIcon icon={faAddressCard} />
                </div>

                <span className="ml-3">Nosotros</span>
              </Link>
            </li>
          </ul>
        </div>
      </aside>
    </>
  );
};
