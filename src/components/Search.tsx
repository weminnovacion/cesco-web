import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState, ChangeEvent } from "react";

export const SearchComponent = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleToggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  return (
    <>
      <div className="relative w-full">
        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
          <FontAwesomeIcon icon={faSearch} className="icon-search" />
        </div>
        <input
          type="search"
          id="search"
          className="navbar-search"
          placeholder="Buscar cursos"
          onFocus={handleToggleModal}
        />
      </div>
    </>
  );
};
