import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState, FC } from "react";
import { useForm } from "react-hook-form";
import classNames from "classnames";
import { RequestStatus } from "@/model/request-status";

interface RecordPaymentFormProps {
  handleStateForm: (formData: RecordPayment) => void;
  status: RequestStatus;
}
interface CardLengths {
  [key: string]: {
    min: number;
    max: number;
  };
}

export const RecordPaymentForm: FC<RecordPaymentFormProps> = ({
  handleStateForm,
  status,
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<RecordPayment>();
  const [cardType, setCardType] = useState("");

  const handleCardNumberChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const cardNumber = e.target.value;
    let type = "";
    if (/^4/.test(cardNumber)) {
      type = "visa";
    } else if (/^5[1-5]/.test(cardNumber)) {
      type = "mastercard";
    } else if (/^3[47]/.test(cardNumber)) {
      type = "amex";
    } else if (/^6(?:011|5)/.test(cardNumber)) {
      type = "discover";
    } else if (/^(?:3(?:0[0-5]|[68])|5\d\d)\d{11}$/.test(cardNumber)) {
      type = "dinersclub";
    } else if (/^(?:2131|1800|35\d{3})\d{11}$/.test(cardNumber)) {
      type = "jcb";
    }
    setCardType(type);
  };

  const onSubmit = handleSubmit(async (data) => {
    handleStateForm(data);
  });

  const cardLengths: CardLengths = {
    visa: { min: 13, max: 16 },
    mastercard: { min: 16, max: 16 },
    amex: { min: 15, max: 15 },
    discover: { min: 16, max: 16 },
    dinersclub: { min: 14, max: 16 },
    jcb: { min: 16, max: 19 },
  };

  const minCardLength = cardLengths[cardType]?.min || 13;
  const maxCardLength = cardLengths[cardType]?.max || 16;

  return (
    <form onSubmit={onSubmit} className="row">
      <div className="col-12">
        <div className="relative mb-4">
          <label
            htmlFor="CardNumber"
            className="text-zinc-700 block font-semibold mb-2"
          >
            Número de tarjeta
          </label>
          <input
            type="text"
            id="CardNumber"
            placeholder="0000 0000 0000 0000"
            {...register("CardNumber", {
              required: "Este campo es obligatorio",
              pattern: {
                value: /^[0-9]+$/,
                message: "Solo se permiten números en este campo",
              },
              minLength: {
                value: cardLengths[cardType]?.min || 13,
                message: `La longitud mínima para ${cardType?.toUpperCase()} es ${
                  cardLengths[cardType]?.min || 13
                }`,
              },
              maxLength: {
                value: cardLengths[cardType]?.max || 16,
                message: `La longitud máxima para ${cardType?.toUpperCase()} es ${
                  cardLengths[cardType]?.max || 16
                }`,
              },
            })}
            onChange={handleCardNumberChange}
            minLength={minCardLength}
            maxLength={maxCardLength}
            disabled={status === "loading"}
            className={classNames("form-control", {
              invalid: errors.CardNumber,
            })}
          />
          {errors.CardNumber && (
            <span className="text-red-500">{errors.CardNumber.message}</span>
          )}
        </div>
      </div>
      <div className="col-12">
        <div className="row">
          <div className="col">
            <div className="relative mb-4">
              <label
                htmlFor="ExpMonth"
                className="text-zinc-700 block font-semibold mb-2"
              >
                Mes de expiración
              </label>
              <input
                type="text"
                id="ExpMonth"
                placeholder="00"
                disabled={status === "loading"}
                {...register("ExpMonth", {
                  required: "Este campo es obligatorio",
                  pattern: {
                    value: /^[0-9]+$/,
                    message: "Solo se permiten números en este campo",
                  },
                  minLength: {
                    value: 2,
                    message: "El mes de expiración debe tener 2 dígitos",
                  },
                  maxLength: {
                    value: 2,
                    message: "El mes de expiración debe tener 2 dígitos",
                  },
                  validate: (value) => {
                    const currentYear = new Date().getFullYear() % 100; // Obtiene los dos últimos dígitos del año actual
                    const currentMonth = new Date().getMonth() + 1;
                    const enteredYear = parseInt(getValues("ExpYear"), 10);
                    const enteredMonth = parseInt(value, 10);

                    if (enteredYear > currentYear) {
                      return true; // El año es mayor, no importa el mes
                    } else if (enteredYear === currentYear) {
                      return (
                        enteredMonth >= currentMonth ||
                        "La fecha de expiración debe ser mayor o igual a la fecha actual"
                      );
                    } else {
                      return "El año de vencimiento debe ser mayor o igual al año actual";
                    }
                  },
                })}
                className={classNames("form-control", {
                  invalid: errors.ExpMonth,
                })}
              />
              {errors.ExpMonth && (
                <span className="text-red-500">{errors.ExpMonth.message}</span>
              )}
            </div>
          </div>
          <div className="col-auto flex justify-center items-center">
            <span className="text-zinc-700 font-semibold mt-4">/</span>
          </div>
          <div className="col">
            <div className="relative mb-4">
              <label
                htmlFor="ExpYear"
                className="text-zinc-700 block font-semibold mb-2"
              >
                Año de vencimiento
              </label>
              <input
                type="text"
                id="ExpYear"
                placeholder="00"
                disabled={status === "loading"}
                {...register("ExpYear", {
                  required: "Este campo es obligatorio",
                  pattern: {
                    value: /^[0-9]{2}$/,
                    message: "El año de vencimiento debe tener 2 dígitos",
                  },
                  minLength: {
                    value: 2,
                    message: "El año de vencimiento debe tener 2 dígitos",
                  },
                  maxLength: {
                    value: 2,
                    message: "El año de vencimiento debe tener 2 dígitos",
                  },
                  validate: (value) => {
                    const currentYear = new Date().getFullYear() % 100; // Obtiene los dos últimos dígitos del año actual
                    const enteredYear = parseInt(value, 10);
                    return (
                      enteredYear >= currentYear ||
                      "El año de vencimiento debe ser válido"
                    );
                  },
                })}
                className={classNames("form-control", {
                  invalid: errors.ExpYear,
                })}
              />
              {errors.ExpYear && (
                <span className="text-red-500">{errors.ExpYear.message}</span>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 col-lg-4">
        <div className="relative mb-4">
          <label
            htmlFor="Cvc"
            className="text-zinc-700 block font-semibold mb-2"
          >
            CVC
          </label>
          <input
            type="text"
            id="Cvc"
            placeholder="000"
            disabled={status === "loading"}
            {...register("Cvc", {
              required: "Este campo es obligatorio",
              pattern: {
                value: /^[0-9]+$/,
                message: "Solo se permiten números en este campo",
              },
              minLength: {
                value: 3,
                message: "El CVC debe tener 3 dígitos",
              },
              maxLength: {
                value: 3,
                message: "El CVC debe tener 3 dígitos",
              },
            })}
            className={classNames("form-control", { invalid: errors.Cvc })}
          />
          {errors.Cvc && (
            <span className="text-red-500">{errors.Cvc.message}</span>
          )}
        </div>
      </div>
      <div className="col-12">
        <div className="relative mb-4">
          <button
            disabled={status === "loading"}
            className="btn btn-primary-green flex justify-center items-center w-full"
          >
            <div className="w-4 h-4 flex justify-center items-center mr-3">
              <FontAwesomeIcon icon={faCartShopping} />
            </div>
            <span>Completar compra</span>
          </button>
        </div>
      </div>
    </form>
  );
};
