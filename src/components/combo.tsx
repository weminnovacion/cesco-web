//React
import { FC } from "react";

//Context
import { useCurrencyFormatter } from "@/hooks/useCurrencyFormatter";

//Externals
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark, faFilm } from "@fortawesome/free-solid-svg-icons";
import { ComboProps } from "@/model/comboProps.model";
import Link from "next/link";

export const Combo: FC<ComboProps> = ({ children, ...rest }) => {
  const {
    Id,
    Name,
    Description,
    Img,
    NumCourses,
    CourseHourTotal,
    Price,
    PriceTotal,
  } = rest;
  const { currencyWithSuffixFormatter } = useCurrencyFormatter();

  const horas = Math.floor(CourseHourTotal);
  const minutos = Math.floor((CourseHourTotal - horas) * 60);

  return (
    <>
      <div className="card-course border shadow">
        <button className="card-course-header">
          <div className="absolute top-3 left-3 text-white bg-primary-orange/70 text-xs px-3">
            <small>COMBO DE {NumCourses} CURSOS</small>
          </div>
          <img src={Img} alt={Name} />
        </button>
        <div className="card-course-body">
          <div className="flex flex-wrap -mx-3">
            <div className="w-full px-3">
              <h3
                className="text-lg text-zinc-600 font-bold mb-3 capitalize text-container-line-1"
                title={Name}
              >
                <Link
                  href={`/courses/combos/${Id}`}
                  className="a-text-secondary"
                >
                  {Name}
                </Link>
              </h3>
              <p className="text-xs text-zinc-500 text-container-line-3 mb-4">
                {Description}
              </p>
            </div>
          </div>
        </div>
        <div className="card-course-footer">
          <div className="w-full px-0 mb-4">
            <div className="flex flex-wrap -mx-3">
              <div className="flex-grow-1 flex-shrink-0 basis-auto w-auto max-w-full px-3">
                <div className="flex items-center text-xs text-opacity">
                  <div className="mr-1">
                    <FontAwesomeIcon
                      icon={faBookmark}
                      className="mr-1 text-primary-green"
                    />
                  </div>
                  {NumCourses}
                </div>
              </div>
              <div className="flex-grow-1 flex-shrink-0 basis-auto w-auto max-w-full px-3">
                <div className="flex items-center text-xs text-opacity">
                  <div className="mr-1">
                    <FontAwesomeIcon
                      icon={faFilm}
                      className="mr-1 text-primary-green"
                    />
                  </div>
                  {horas}hr {minutos > 0 ? minutos + "mt" : ""}
                </div>
              </div>
            </div>
          </div>
          <div className="text-xs text-zinc-500 mb-4">
            <span className="text-primary-orange text-sm justify-center">
              {PriceTotal && Price ? (
                <>
                  {Math.round(((PriceTotal - Price) / PriceTotal) * 100)}% Dto.
                  <strong className="line-through">
                    {currencyWithSuffixFormatter(PriceTotal)}
                  </strong>
                </>
              ) : (
                ""
              )}
              {!PriceTotal && (
                <>
                  100% Dto.
                  <strong className="line-through">
                    {currencyWithSuffixFormatter(0)}
                  </strong>
                </>
              )}
              <br />
            </span>
          </div>
          {children}
        </div>
      </div>
    </>
  );
};
