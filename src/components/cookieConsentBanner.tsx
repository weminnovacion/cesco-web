import { useState, useEffect } from "react";
import { setConsentCookie, hasConsentCookie } from "../utils/cookies";

export const CookieConsentBanner = () => {
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    setIsVisible(!hasConsentCookie());
  }, []);

  const handleAcceptCookies = () => {
    setConsentCookie();
    setIsVisible(false);
  };

  return isVisible ? (
    <div
      id="banner"
      tabIndex={-1}
      aria-hidden="false"
      className="overflow-y-auto overflow-x-hidden fixed left-1/2 -translate-x-1/2 rounded-t-md bottom-0 z-50 w-full h-modal max-w-5xl"
    >
      <div className="relative w-full h-full md:h-auto">
        <div className="relative bg-white shadow">
          <div className="justify-between items-center p-5 lg:flex">
            <p className="mb-4 text-sm text-gray-500  lg:mb-0">
              Utilizamos cookies propias y de terceros en nuestros sitios web
              para mejorar su experiencia, analizar nuestro tráfico y por
              seguridad y marketing. Seleccione "Aceptar todo" para permitir su
              uso.
            </p>
            <div className="items-center space-y-4 sm:space-y-0 sm:space-x-4 sm:flex lg:pl-10 shrink-0">
              <button
                id="accept-cookies"
                type="button"
                onClick={handleAcceptCookies}
                className="py-2 px-4 w-full text-sm font-medium text-center text-white rounded-lg bg-primary-orange sm:w-auto hover:bg-primary-green focus:ring-4 focus:outline-none focus:ring-primary-green"
              >
                Aceptar todo
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};
