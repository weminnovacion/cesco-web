import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import {
  faFacebook,
  faInstagram,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";

export const ContactInformation = () => {
  return (
    <>
      <aside className="bg-primary-orange/70 py-2 text-white">
        <div className="container">
          <div className="row">
            <div className="col-auto mx-auto lg:mx-0">
              <div className="flex items-center">
                <div className="w-6 h-6 flex justify-center items-center text-xs">
                  <FontAwesomeIcon icon={faPhone} />
                </div>
                <span className="text-xs">+57 324-283-0506</span>
              </div>
            </div>
            <div className="col-auto mx-auto lg:mx-0 lg:border-r">
              <div className="flex items-center ">
                <div className="w-6 h-6 flex justify-center items-center text-xs">
                  <FontAwesomeIcon icon={faEnvelope} />
                </div>
                <span className="text-xs">info@cescoedu.com</span>
              </div>
            </div>
            <div className="col-auto mr-auto ml-auto lg:mr-0 lg:ml-auto lg:border-l">
              <div className="row">
                <div className="col">
                  <a
                    href="https://www.facebook.com/profile.php?id=100090679791262&mibextid=ZbWKwL"
                    target="_blank"
                    className="w-6 h-6 flex justify-center items-center"
                  >
                    <FontAwesomeIcon icon={faFacebook} />
                  </a>
                </div>
                <div className="col">
                  <a
                    href="https://www.instagram.com/cescoeducacion"
                    target="_blank"
                    className="w-6 h-6 flex justify-center items-center"
                  >
                    <FontAwesomeIcon icon={faInstagram} />
                  </a>
                </div>
                <div className="col">
                  <a
                    href="https://www.youtube.com/channel/UCHLdz2BBalec8NwbwLjZ-3g"
                    target="_blank"
                    className="w-6 h-6 flex justify-center items-center"
                  >
                    <FontAwesomeIcon icon={faYoutube} />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </aside>
    </>
  );
};
