import { ChangeEvent, useContext, useEffect, useRef, useState } from "react";
import { AuthContext } from "@/contexts/AuthContext";
import axios from "axios";
import { API_URL } from "../../constants";
import { useInitials } from "@/hooks/useInitials";
import { parseCookies } from "nookies";

export const AvatarUploader: React.FC = () => {
  const { userInfo } = useContext(AuthContext);
  const [isError, setIsError] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);

  const inputFileRef = useRef<HTMLInputElement>(null);
  const [selectedImage, setSelectedImage] = useState<File | null>(null);
  const [previewURL, setPreviewURL] = useState<string | null | undefined>(
    userInfo?.Photo
  );
  const initials = useInitials(
    userInfo?.FirstName ? userInfo?.FirstName + userInfo?.Surname : ""
  );

  const handleImageChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      setSelectedImage(file);
      setPreviewURL(URL.createObjectURL(file));
    }
  };

  useEffect(()=>{
    setPreviewURL(userInfo?.Photo)
  }, [userInfo])

  const handleUpload = async () => {
    if (selectedImage) {
      const formData = new FormData();
      formData.append("IgmProfile", selectedImage);

      const cookies = parseCookies();
      const token = cookies.token;

      try {
        const response = await axios.post(
          API_URL + "/user/uploadFile",
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: token,
            },
          }
        );

        if (response.status == 200) {
          if (response.data?.Status == 500) {
            setIsError(true);
          } else {
            setIsError(false);
            setIsSuccess(true);
            setTimeout(() => {
              setIsSuccess(false);
            }, 10000);
          }
        }
      } catch (error) {
        console.error("Error uploading image:", error);
      }
    }
  };

  return (
    <>
      <div className="col-12">
        <div className="w-full bg-white mb-4 p-8">
          <div className="row mb-5">
            <div className="col-auto mx-auto md:mx-0 mb-4">
              <div className="w-32 h-32  flex justify-center items-center rounded-full bg-zinc-100 border border-zinc-200 text-lg font-semibold overflow-hidden">
                {previewURL ? (
                  <img
                    src={previewURL}
                    alt="Preview"
                    className="w-full h-full bg-cover object-cover bg-center"
                  />
                ) : (
                  <span style={{ fontSize: "48px" }}>{initials}</span>
                )}
              </div>
            </div>
            <div className="col">
              <div className="row">
                <div className="col-12 mb-4">
                  <p className="text-center md:text-left">
                    Imagen de perfil{" "}
                    <span className="text-sm  text-zinc-500">
                      (Recomendado: 800x800px)
                    </span>
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-auto mb-4">
                  <input
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    ref={inputFileRef}
                    style={{ display: "none" }}
                  />
                  <button
                    className="btn btn-white btn-small w-full md:w-auto"
                    type="button"
                    onClick={() => inputFileRef?.current?.click()}
                  >
                    Seleccionar imagen
                  </button>
                </div>
                <div className="col-12 col-md-auto mb-4">
                  <button
                    className="btn btn-primary-green btn-small w-full md:w-auto"
                    type="button"
                    onClick={handleUpload}
                    disabled={!selectedImage}
                  >
                    Guardar cambios
                  </button>
                </div>
              </div>
              <span role="alert" className="text-red-500 text-xs">
                {isError &&
                  "Ocurrio un error al actualizar la imagen de perfil, intente con otra"}
              </span>
              <span role="alert" className="text-green-500 text-xs">
                {isSuccess &&
                  "La imagen de perfil fue actualizada correctamente"}
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
