import { useEffect, useState, FC } from "react";
import { RequestStatus } from "@/model/request-status";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useForm } from "react-hook-form";
import classNames from "classnames";
import axios from "axios";
import { API_URL } from "../../../constants";

interface PSEPaymentFormProps {
  handleStateForm: (formData: PSEPayment) => void;
  status: RequestStatus;
  setStatus: (newStatus: RequestStatus) => void;
}

interface Banks {
  bankCode: string;
  bankName: string;
}

interface TypePerson {
  Id: string;
  Name: string;
}

export const PSEPaymentForm: FC<PSEPaymentFormProps> = ({
  handleStateForm,
  status,
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<PSEPayment>();

  const [banks, setBanks] = useState<Banks[]>([]);
  const [typePerson, setTypePerson] = useState<TypePerson[]>([]);

  const getBanks = async (): Promise<Banks[]> => {
    try {
      const response = await axios.get(API_URL + "checkout/getBanks");
      const banks: Banks[] = response.data.Data.data;
      return banks;
    } catch (error) {
      return [];
    }
  };

  const getTypePerson = async (): Promise<TypePerson[]> => {
    try {
      const response = await axios.get(API_URL + "checkout/getTypePerson");
      const typePerson: TypePerson[] = response.data.Data;
      return typePerson;
    } catch (error) {
      return [];
    }
  };

  const fetchData = async () => {
    try {
      const [responseBanks, responseTypePerson] = await Promise.all([
        getBanks(),
        getTypePerson(),
      ]);
      setBanks(responseBanks);
      setTypePerson(responseTypePerson);
    } catch (error) {}
  };

  useEffect(() => {
    fetchData();
  }, []);

  const onSubmit = handleSubmit(async (data) => {
    handleStateForm(data);
  });

  return (
    <>
      <form onSubmit={onSubmit} className="row">
        <div className="col-12 mb-4">
          <div className="relative">
            <label
              htmlFor="search"
              className="mb-2 text-sm font-semibold text-zinc-900 "
            >
              Nombre del banco
            </label>
            <div className="relative">
              <select
                id="fecha-de-expiracion"
                disabled={status === "loading"}
                {...register("BankId", {
                  required: "Este campo es obligatorio",
                })}
                className={classNames("form-control", {
                  invalid: errors.BankId,
                })}
              >
                <option value="">Seleccione...</option>
                {banks.map((bank) => (
                  <option key={bank.bankCode} value={bank.bankCode}>
                    {bank.bankName}
                  </option>
                ))}
              </select>
              {errors.BankId && (
                <span className="text-red-500">{errors.BankId.message}</span>
              )}
            </div>
          </div>
        </div>
        <div className="col-12 mb-4">
          <div className="relative">
            <label
              htmlFor="search"
              className="mb-2 text-sm font-semibold text-zinc-900 "
            >
              Tipo de persona
            </label>
            <div className="relative">
              <select
                id="fecha-de-expiracion"
                disabled={status === "loading"}
                {...register("TypePersonId", {
                  required: "Este campo es obligatorio",
                })}
                className={classNames("form-control", {
                  invalid: errors.TypePersonId,
                })}
              >
                <option value="">Seleccione...</option>
                {typePerson.map((person) => (
                  <option key={person.Id} value={person.Id}>
                    {person.Name}
                  </option>
                ))}
              </select>
              {errors.TypePersonId && (
                <span className="text-red-500">
                  {errors.TypePersonId.message}
                </span>
              )}
            </div>
          </div>
        </div>
        <div className="col-12 mb-4">
          <button
            disabled={status === "loading"}
            className="btn btn-primary-green flex justify-center items-center w-full"
          >
            <div className="w-4 h-4 flex justify-center items-center mr-3">
              <FontAwesomeIcon icon={faCartShopping} />
            </div>
            <span>Completar compra</span>
          </button>
        </div>
      </form>
    </>
  );
};
