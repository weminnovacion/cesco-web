import { useState, useEffect, ReactNode } from "react";

type ButtonState = "default" | "loading" | "disabled" | "success" | "error";

interface ButtonProps {
  children: ReactNode;
  currentState?: ButtonState;
  timerDuration?: number;
  typeButton?: "button" | "submit" | "reset";
  onClick?: () => void;
}

export const Button: React.FC<ButtonProps> = ({
  children,
  currentState = "default",
  timerDuration = 2000,
  typeButton = "button",
  onClick,
}) => {
  const [buttonState, setButtonState] = useState<ButtonState>(currentState);

  useEffect(() => {
    if (buttonState === "error" || buttonState === "success") {
      const timer = setTimeout(() => {
        setButtonState("default");
      }, timerDuration);

      return () => clearTimeout(timer);
    }
  }, [buttonState, timerDuration]);

  const handleClick = () => {
    if (onClick) {
      onClick();
    }
  };

  useEffect(() => {
    setButtonState(currentState);
  }, [currentState]);

  return (
    <button
      type={typeButton}
      className={`py-2 px-4 w-full rounded-sm 
      ${buttonState === "default" && "bg-secondary text-white "} 
      ${buttonState === "success" && "bg-green-500 text-white"} 
      ${buttonState === "error" && "bg-red-500 text-white"} 
      ${buttonState === "loading" && "bg-secondary text-white"} 
      ${
        buttonState === "disabled" || buttonState === "loading"
          ? "cursor-not-allowed opacity-50"
          : "cursor-pointer"
      }`}
      disabled={buttonState === "disabled" || buttonState === "loading"}
      onClick={handleClick}
    >
      {buttonState === "loading" && "Loading..."}
      {buttonState === "success" && "Success!"}
      {buttonState === "error" && "Error!"}
      {buttonState === "default" && children}
    </button>
  );
};
