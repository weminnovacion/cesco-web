import { FC } from "react";
import { TabProps } from "@/model/tabProps.model";

export const Tab: FC<TabProps> = ({ children }) => {
  return <>{children}</>;
};
