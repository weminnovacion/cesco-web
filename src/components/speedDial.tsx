import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const SpeedDial = () => {
  return (
    <div data-dial-init="" className="fixed right-6 bottom-6 group">
      <a
        href="https://wa.me/+5732428305060"
        target="_blank"
        type="button"
        data-dial-toggle="speed-dial-menu-default"
        aria-controls="speed-dial-menu-default"
        aria-expanded="false"
        className="flex items-center justify-center text-white bg-green-500 rounded-full w-20 h-20 text-4xl hover:bg-green-600  focus:ring-4 focus:ring-green-300 focus:outline-none "
      >
        <FontAwesomeIcon icon={faWhatsapp} />
        <span className="sr-only">Open actions menu</span>
      </a>
    </div>
  );
};
