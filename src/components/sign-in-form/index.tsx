//React
import { FC, useContext, useEffect, useState } from "react";

//Next
import Link from "next/link";

//Models
import { RequestStatus } from "@/model/request-status";

//Utils
import { setAuthToken } from "@/utils/api";

//Contexts
import { AuthContext } from "@/contexts/AuthContext";

//Externals
import axios from "axios";
import classNames from "classnames";
import { useForm } from "react-hook-form";
import { API_URL } from "../../../constants";
import { useRouter } from "next/router";

type SignInFormProps = {
  handleStateForm: (status: RequestStatus, message?: string) => void;
};

type SignInFormInputs = {
  username: string;
  password: string;
};

export const SignInForm: FC<SignInFormProps> = ({ handleStateForm }) => {
  const router = useRouter();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<SignInFormInputs>();
  const [error, setError] = useState("");
  const [status, setStatus] = useState<RequestStatus>("init");
  const { login } = useContext(AuthContext);

  const onSubmit = handleSubmit(async (data) => {
    try {
      setStatus("loading");
      const response = await axios.post(API_URL + "oauth/signin", data);
      if (response.status == 200) {
        if (response.data?.Status == 200) {
          const token = response.data.Data.token;
          setAuthToken(token);
          login();
          setStatus("success");
          setAuthToken(token);
          router.push("/");
        } else {
          setStatus("error");
          setTimeout(() => {
            setStatus("init");
          }, 3000);
          setError(response.data?.Message);
        }
      }
    } catch (error) {
      console.error(error);
    }
  });

  useEffect(() => {
    handleStateForm(status);
  }, [status]);

  return (
    <>
      <form onSubmit={onSubmit}>
        <div
          id="alert-border-2"
          className={classNames(
            " p-4 mb-4 text-red-800 border-t-4 border-red-300 bg-red-50 ",
            {
              hidden: !error,
              flex: error,
            }
          )}
          role="alert"
        >
          <svg
            className="flex-shrink-0 w-5 h-5"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
              clipRule="evenodd"
            />
          </svg>
          <div className="ml-3 text-sm font-medium">{error}</div>
          <button
            onClick={() => setError("")}
            type="button"
            className="ml-auto -mx-1.5 -my-1.5 bg-red-50 text-red-500 rounded-lg focus:ring-2 focus:ring-red-400 p-1.5 hover:bg-red-200 inline-flex h-8 w-8"
            data-dismiss-target="#alert-border-2"
            aria-label="Close"
          >
            <span className="sr-only">Dismiss</span>
            <svg
              aria-hidden="true"
              className="w-5 h-5"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>
        </div>
        <div className="row">
          <div className="col-12 mb-5">
            <label
              htmlFor="username"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Usuario
            </label>
            <input
              type="text"
              id="username"
              className={classNames("form-control", {
                invalid: errors.username,
              })}
              placeholder="name@example.com"
              disabled={status === "loading"}
              {...register("username", {
                required: "Este campos es obligatorio",
              })}
            />
            {errors.username && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.username.message}
              </span>
            )}
          </div>
          <div className="col-12 mb-5">
            <label
              htmlFor="password"
              className="block mb-2 text-sm font-medium text-zinc-700 "
            >
              Contraseña
            </label>
            <input
              type="password"
              id="password"
              placeholder="••••••••"
              disabled={status === "loading"}
              className={classNames("form-control", {
                invalid: errors.password,
              })}
              {...register("password", {
                required: "Este campos es obligatorio",
              })}
            />
            {errors.password && (
              <span role="alert" className="text-red-500 text-xs">
                {errors.password.message}
              </span>
            )}
          </div>
          <div className="col-12 mb-5">
            <div className="flex items-center justify-between">
              <div className="flex items-start">
                <div className="flex items-center h-5">
                  <input
                    id="remember"
                    aria-describedby="remember"
                    type="checkbox"
                    className="w-4 h-4 text-primary-green border border-primary-green rounded bg-primary-green/5 focus:ring-3 focus:ring-primary-green/30 "
                  />
                </div>
                <div className="ml-3 text-sm">
                  <label htmlFor="remember" className="text-zinc-500 ">
                    Recuérdame
                  </label>
                </div>
              </div>
              <Link
                href="/auth/password/edit"
                className="text-sm font-medium text-primary-green hover:underline "
              >
                ¿Has olvidado tu contraseña?
              </Link>
            </div>
          </div>
          <div className="col-12 mb-5">
            <button
              disabled={status === "loading"}
              type="submit"
              className="btn btn-primary-green w-full mb-5"
            >
              {status === "loading" ? (
                <span>Loading...</span>
              ) : (
                <span>Iniciar sesión</span>
              )}
            </button>
          </div>
        </div>
      </form>
    </>
  );
};
