import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { setCookie, parseCookies } from 'nookies';
import { API_URL } from '../../constants';

const api: AxiosInstance = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export function setAuthToken(token: string): void {
  api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  setCookie(null, 'token', token, {
    maxAge: 30 * 24 * 60 * 60,
    path: '/',
  });
}

export function getAuthToken(): string | undefined {
  const cookies = parseCookies();
  const token = cookies.token;
  return token;
}

export default api;