import { setCookie, parseCookies } from 'nookies';

export const setConsentCookie = () => {
  setCookie(null, 'consent', 'true', {
    maxAge: 30 * 24 * 60 * 60, // Caducidad de la cookie en segundos (30 días)
    path: '/',
  });
};

export const getConsentCookie = () => {
  const cookies = parseCookies();
  return cookies.consent || null;
};

export const hasConsentCookie = () => {
  return !!getConsentCookie();
};
