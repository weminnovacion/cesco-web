import { FC, ReactNode, createContext, useContext, useState } from "react";

interface BackdropContextType {
  isBackdropOpen: boolean;
  toggleBackdrop: React.Dispatch<React.SetStateAction<boolean>>;
}

export const BackdropContext = createContext<BackdropContextType>({
  isBackdropOpen: false,
  toggleBackdrop: () => {},
});

export const useBackdropContext = () => useContext(BackdropContext);

export const BackdropProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [isBackdropOpen, toggleBackdrop] = useState(false);

  return (
    <BackdropContext.Provider value={{ isBackdropOpen, toggleBackdrop }}>
      {children}
    </BackdropContext.Provider>
  );
};
