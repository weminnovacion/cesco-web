import { createContext, useState, ReactNode, useEffect } from "react";
import { destroyCookie, parseCookies } from "nookies";
import axios from "axios";
import { API_URL } from "../../constants";

// Definir el tipo para el contexto
interface AuthContextType {
  isLoggedIn: boolean;
  login: () => void;
  logout: () => void;
  userInfo: UserInfoResponse | null;
}

// Crear el contexto
export const AuthContext = createContext<AuthContextType>({
  isLoggedIn: false,
  login: () => {},
  logout: () => {},
  userInfo: null,
});

// Definir el tipo para el componente proveedor del contexto
interface AuthProviderProps {
  children: ReactNode;
}

// Crear el componente proveedor del contexto
export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {

  const initialToken = parseCookies();

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [userInfo, setUserInfo] = useState<UserInfoResponse | null>(null);
  const [token, setToken] = useState<string | null>(initialToken.token);

  useEffect(() => {
    const cookies = parseCookies();
    setToken(cookies.token);
    if (token) {
      axios
        .get(API_URL + "user/userInfo", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          const userData = response.data.Data;
          setUserInfo(userData);
          setIsLoggedIn(true);
        })
        .catch((error) => {
          alert("Su sesión ha expirado");
        });
    }else{
      logout()
    }
  }, [token]);

  const login = () => {
    const cookies = parseCookies();
    setToken(cookies.token);
    setIsLoggedIn(true);
  };

  const logout = () => {
    console.log('logout ' );
    
    destroyCookie(null, "token");
    setIsLoggedIn(false);
  };

  return (
    <AuthContext.Provider value={{ isLoggedIn, login, logout, userInfo }}>
      {children}
    </AuthContext.Provider>
  );
};
