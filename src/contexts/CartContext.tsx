// src/context/CartContext.tsx
import { FC, ReactNode, createContext, useEffect, useState } from "react";
import { CartItem } from "@/model/cart.model";
import { CourseProps } from "@/model/courseProps.model";
import { ComboProps } from "@/model/comboProps.model";

interface CartContextType {
  cart: CartItem[];
  courses: CourseProps[];
  combos: ComboProps[];
  totalCourses: number;
  totalCombos: number;
  addToCart: (item: CartItem) => void;
  removeFromCart: (item: CartItem) => void;
  cleanCart: () => void;
}

// Crear el contexto
export const CartContext = createContext<CartContextType>({
  cart: [],
  courses: [],
  combos: [],
  totalCourses: 0,
  totalCombos: 0,
  addToCart: () => {},
  removeFromCart: () => {},
  cleanCart: () => {},
});

interface CartProviderProps {
  children: ReactNode;
}

export const CartProvider: FC<CartProviderProps> = ({ children }) => {
  const [cart, setCart] = useState<CartItem[]>([]);
  const [courses, setCourses] = useState<CourseProps[]>([]);
  const [combos, setCombos] = useState<ComboProps[]>([]);
  const [totalCourses, setTotalCourses] = useState<number>(0);
  const [totalCombos, setTotalCombos] = useState<number>(0);

  useEffect(() => {
    const savedCart = localStorage.getItem("cart");
    if (savedCart) {
      setCart(JSON.parse(savedCart));
      const filteredCourses = cart.filter(
        (item) => item.type === "course"
      ) as CourseProps[];
      const filteredCombos = cart.filter(
        (item) => item.type === "combo"
      ) as ComboProps[];
      setCourses(filteredCourses);
      setCombos(filteredCombos);
    }
  }, []);

  useEffect(() => {
    const filteredCourses = cart.filter(
      (item) => item.type === "course"
    ) as CourseProps[];
    const filteredCombos = cart.filter(
      (item) => item.type === "combo"
    ) as ComboProps[];

    setCourses(filteredCourses);
    setCombos(filteredCombos);
  }, [cart]);

  useEffect(() => {
    const totalCourses = courses.reduce((acc, item) => {
      if (item.hasOwnProperty("PriceWithDiscount")) {
        return acc + item.PriceWithDiscount;
      }
      return acc;
    }, 0);
    setTotalCourses(totalCourses);
  }, [courses]);

  useEffect(() => {
    const totalCombos = combos.reduce((acc, item) => {
      if (item.hasOwnProperty("TotalValueOfTheComboWithTheDiscount")) {
        return acc + item.TotalValueOfTheComboWithTheDiscount;
      }
      return acc;
    }, 0);
    setTotalCombos(totalCombos);
  }, [combos]);

  const addToCart = (item: CartItem) => {
    if (!cart.some((c) => c.Id === item.Id && c.type === item.type)) {
      setCart([...cart, item]);
      localStorage.setItem("cart", JSON.stringify([...cart, item]));
    }
  };

  const removeFromCart = (item: CartItem) => {
    const updatedCart = cart.filter(
      (c) => !(c.Id === item.Id && c.type === item.type)
    );
    setCart(updatedCart);
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };
  
  const cleanCart = () => {
    setCart([]);
    localStorage.setItem("cart", JSON.stringify([]));
  }

  return (
    <CartContext.Provider
      value={{
        cart,
        courses,
        combos,
        totalCourses,
        totalCombos,
        addToCart,
        removeFromCart,
        cleanCart
      }}
    >
      {children}
    </CartContext.Provider>
  );
};
