/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
    './src/app/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        ...colors,
        primary: {
          'green': '#02AE9B',
          'orange': '#CD7E01',
          'red': '#E37461',
        },
        accent: {
          'green': '#B4D4CE',
          'orange': '#F7E4C6',
        }
      },
      maxHeight: {
        '0': '0',
        '96': '24rem',
      },
      transitionProperty: {
        'max-height': 'max-height',
      },
      transform: {
        'rotate-180': 'rotate(180deg)',
      },

    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
